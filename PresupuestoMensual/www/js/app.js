$.ajaxSetup({
  async: true,
          username:'dinero_app_android',
          password:'SFVUTR456',
          type:'GET',
          xhrFields: { withCredentials: true },
});
var urlAPI = 'https://api.reconfiguracionfinanciera.com.mx/rf_app/REST_CI_DINERO_PROD/index.php/';
var app = {
  
  conn: true,
  comparacionURL:             urlAPI + 'comparison/data/',
  loginUrl:          	      urlAPI + 'user/login/',
  isRegistered:          	  urlAPI + 'user/isregistered/',
  usersUrl:          	      urlAPI + 'user/data/',
  categoryUrl:       	      urlAPI + 'category/data/',
  subcategoryUrl:    	      urlAPI + 'subcategory/data/',
  subcategoryAllUrl: 	      urlAPI + 'subcategory/data_all/',
  estimationUrl:     	      urlAPI + 'estimation/data/',
  originsUrl:        	      urlAPI + 'origins/data/',
  alarm:             	      urlAPI + 'alarm/data/',
  alarmArr:         	      urlAPI + 'alarm/data_arr/',
  alarmEdit:         	      urlAPI + 'alarm/data_edit/',
  alarmDel:         	      urlAPI + 'alarm/data_del/',
  userStep:          	      urlAPI + 'user/step/',
  userResPassUrl:      	      urlAPI + 'user/recover/',
  userChangeUrl:      	      urlAPI + 'user/pass/',
  userLimitUrl:      	      urlAPI + 'user/limit/',
  ingresosUrl:       	      urlAPI + 'ingress/data/',
  ingresosArrUrl:    	      urlAPI + 'ingress/data_arr/',
  ingresosEditUrl:    	      urlAPI + 'ingress/data_edit/',
  ingresosDelUrl:    	      urlAPI + 'ingress/data_del/',
  totalingresosUrl : 	      urlAPI + 'ingress/total_mes/',
  graficaingresosUrl: 	      urlAPI + 'ingress/graph/',
  graficadaysingresosUrl:     urlAPI + 'ingress/graph_days/',
  graficagastosUrl:  	      urlAPI + 'expenses/graph/',
  graficadaysgastosUrl:	      urlAPI + 'expenses/graph_days_new/',
  graficadaysgastosOrigenUrl: urlAPI + 'expenses/graph_days_origins/',
  gastosUrl:         	      urlAPI + 'expenses/data/',
  gastosArrUrl:               urlAPI + 'expenses/data_arr/',
  gastosEditUrl:    	      urlAPI + 'expenses/data_edit/',
  gastosDelUrl:    	          urlAPI + 'expenses/data_del/',
  totalgastosUrl:    	      urlAPI + 'expenses/total_mes/',
  estimationtableUrl: 	      urlAPI + 'estimation/table/',
  graficaPresupuestoUrl:      urlAPI + 'estimation/graph/',
  graficaPresupuestoAnualUrl: urlAPI + 'estimation/graph_anual/',
  totalPresupuestoUrl:        urlAPI + 'estimation/total/',
  totalAhorradoUrl:           urlAPI + 'estimation/ahorrado/',
  ahorrastePresupuestoUrl:    urlAPI + 'estimation/ahorraste/',
  historialURL:               urlAPI + 'historic/data/',
  reportsMonthURL:            urlAPI + 'reports/table/',
  reportsExcelURL: urlAPI + 'reports/excel_report/',
  username:'dinero_app_android',
  password:'SFVUTR456',


  user: {

  		logout: function() {
  			localStorage.removeItem('userId');
	        localStorage.removeItem('userProceso');
	        location.href = 'index.html';
  		},//logout
  		is_registered_with_facebook:function(id,email,name){
  			$.ajax({
	          url: app.isRegistered,
	          data: {
	            email: email
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	        	if(message.user && Array.isArray(message.user) && message.user.length > 0 ){
	        		//Upate user with FB_ID
					var userData = message.user[0];
					localStorage.setItem('userId', userData.id);
					localStorage.setItem('userProceso', userData.proceso);
					localStorage.setItem('localUser', '{"id":'+userData.id+',"email":"'+userData.email+'"}');

					if (userData.proceso==0)
						location.href = '01APP-PASO1.html';
					if (userData.proceso==1)
						location.href = '01APP-PASO2.html';
					if (userData.proceso==2)
						location.href = '01APP-PASO3.html';
					if (userData.proceso==3)
						location.href = '01APP-HOME.html';

	        	}
	        	else{
	        		//Create User wit FB
	        		var userEmail = email;
			        var username  = name;
			        var userPwd   = md5(email);
			        var userPwdc  = md5(email);
			        var aviso 	  = 1;

				    $.ajax({
					     url: app.usersUrl,
					     method: 'PUT',
					     data: {
			            		email   : userEmail,
			            		nombres : username,
			            		pass    : userPwd,
			            		passc   : userPwdc,
			            		aviso   : aviso
			          		},
			          beforeSend: function(jqXHR, settings) {
			          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
			          }
					     }).done(function(message) {
					     var m = $.parseJSON(JSON.stringify(message));
					     $('body').fadeIn('slow');
					     if(m.status === true) {
					       $(".load").fadeOut("slow");
					       $(".alerta").fadeIn("slow");
			         	   $("#mensaje").html("Bienvenido, usuario creado correctamete");
					  	   $("#cerrar").attr("onclick","location.href = 'index.html';");
					     };
					     if(m.status === false) {
					     	$(".load").fadeOut("slow");
					        $(".alerta").fadeIn("slow");
			         		$("#mensaje").html(m.error);
					     }
					     $('.registerUser').animate({
					      top: '100%',
					      },300);
					}).fail(function(message) {
					    console.log(JSON.stringify(message));
					    $('body').fadeIn('slow');
					});
	        	}
	        });
  		},
	 	login: function() {

				$("html, body").animate({ scrollTop: 0 }, 0);
	      if (!app.conn) {
	        alert('You are not connected to the Internet');
	      } else {
	        var user = $('#login > #usuario').val();
	        var userPwd   = md5($('#login > #password').val());
	        //var aviso = 0;

	        /*if (document.getElementById('aviso').checked) {
  				aviso = 1;
			}*/

	        $.ajax({

	          url: app.loginUrl,
	          data: {
	            user: user,
	            pass : userPwd,
	            //aviso : aviso,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	         	$('.load').fadeOut('slow');
	         	$(".alerta").fadeIn("slow");
	         	$("#mensaje").html(m.error);
	         } else if(m.status === true) {
	           var userData = m.data[0];

	           localStorage.setItem('userId', userData.id);
	           localStorage.setItem('userProceso', userData.proceso);
	           localStorage.setItem('localUser', '{"id":'+userData.id+',"email":"'+userData.email+'"}');

	           if (userData.proceso==0)
	          	 	location.href = '01APP-PASO1.html';
	       	   if (userData.proceso==1)
	           		location.href = '01APP-PASO2.html';
	           if (userData.proceso==2)
	           		location.href = '01APP-PASO3.html';
	           if (userData.proceso==3)
	           		location.href = '01APP-HOME.html';
	         };
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert(m);
	       });
	     }
	   },//login

	    add: function() {

		    var userEmail = $('#addUserForm > #email').val();
	        var username  = $('#addUserForm > #nombres').val();
	        var userPwd   = md5($('#addUserForm > #pass').val());
	        var userPwdc  = md5($('#addUserForm > #passc').val());
	        var aviso = 0;

	        if (document.getElementById('aviso').checked) {
  				aviso = 1;
			}

		    $.ajax({
			     url: app.usersUrl,
			     method: 'PUT',
			     data: {
	            		email   : userEmail,
	            		nombres : username,
	            		pass    : userPwd,
	            		passc   : userPwdc,
	            		aviso   : aviso
	          		},
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
			     }).done(function(message) {
			     var m = $.parseJSON(JSON.stringify(message));
			     if(m.status === true) {
			       $(".load").fadeOut("slow");
			       $(".alerta").fadeIn("slow");
	         	   //$("#mensaje").html("La confirmacion ha sido enviada a su correo");
	         	   $("#mensaje").html("Bienvenido, usuario creado correctamete");
			  	   $("#cerrar").attr("onclick","location.href = 'index.html';");
			     };
			     if(m.status === false) {
			     	$(".load").fadeOut("slow");
			        $(".alerta").fadeIn("slow");
	         		$("#mensaje").html(m.error);
			     }
			     $('.registerUser').animate({
			      top: '100%',
			      },300);
			     }).fail(function(message) {
			       console.log(JSON.stringify(message));
			     });

		},// add

		update_pass: function(){

			var uid        = localStorage.getItem('userId');

	        var userPwd    = md5($('#editPassForm > #pass').val());
	        var userPwdc   = md5($('#editPassForm > #passc').val());

		    $.ajax({
			     url: app.userChangeUrl,
			     method: 'POST',
			     data: {
			      	    id      : uid,
	            		pass    : userPwd,
	            		passc   : userPwdc,
	          		},
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
			     }).done(function(message) {
				     var m = $.parseJSON(JSON.stringify(message));
				     if(m.status === true) {
				     	$(".load").fadeOut("slow");
				     	$(".alerta").fadeIn("slow");
	            		$("#mensaje").html("Su contraseña ha sido actualizada");
				     };
				     if(m.status === false) {
				     	$(".load").fadeOut("slow");
				        $(".alerta").fadeIn("slow");
	            		$("#mensaje").html(m.error);
				       //alert("We couldn't save your user's data at this moment, please try again later");
				     }

			     }).fail(function(message) {
			       console.log(JSON.stringify(message));
			     });

		},//update

		paso:function(nStep){
			 var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.userStep,
	          method: 'POST',
	          data: {
	            idUser : uid,
	            nStep  : nStep,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           	alert(m.error);
	         } else if(m.status === true) {

	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//paso

		res_pass:function(){

			$.ajax({
	          url: app.userResPassUrl,
	          data: {
	            email : $("#email").val(),
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           	 $(".alerta").fadeIn("slow");
	            $("#mensaje").html(m.error);
	         } else if(m.status === true) {
	         	$("#cerrar").attr("onclick","location.href='index.html'");
	         	$(".alerta").fadeIn("slow");
	            $("#mensaje").html("La nueva contraseña ha sido enviada a su correo");
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//res_pass

		set_limit:function(){

			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.userLimitUrl,
	          method:"POST",
	          data: {
	            idUser : uid,
	            limit : $("#div_limit").html(),
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           	console.log(m.error);
	           	location.reload();
	         } else if(m.status === true) {
	         	location.reload();
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//set_limit

		get_limit:function(){

			var uid  = localStorage.getItem('userId');

			if(app.offline.check_n_get() === 1){
					$.ajax({
			          url: app.userLimitUrl,
			          data: {
			            idUser : uid,
			          },
			          beforeSend: function(jqXHR, settings) {
			          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
			          }
			        }).done(function(message) {
			         var m = $.parseJSON(JSON.stringify(message));

			         if(m.status === false) {
			           	console.log(m.error);
			         } else if(m.status === true) {

			         	localStorage.setItem('TotalLimit',JSON.stringify(m.data.limite));
			            $("#div_limit").html(m.data.limite);
			            $("#limit").val(m.data.limite);

			         }
			       }).fail(function(message) {
			         var m = JSON.stringify((message));
			        // alert("We couldn't complete your request at this time, please try again later.");
			       });
	   		} else {
	   			var totalLimit = localStorage.getItem('TotalLimit');

	   			if(totalLimit){
	   				$("#div_limit").html(m.data.limite);
			        $("#limit").val(m.data.limite);
			    } else {
			    	$("#div_limit").html(0);
			        $("#limit").val(0);
			    }

			    $('#saveLimit').hide();
	   		}

		},//get_limit

	},//USER

	categoria: {

		list_check:function(){

			if(app.offline.check_n_get() === 1)
          		app.categoria.list();
        	else
          		app.categoria.list_offline();

		},//list_check

		list: function(event) {

		    var uid = localStorage.getItem('userId');
		    var icon = '';
		    $.ajax({
	          url: app.categoryUrl,
	          data: {
	            id: uid
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	            alert('error');
	         } else if(m.status === true) {
	         	//console.log(m.data);
	         	localStorage.setItem('categoryListOffline',JSON.stringify(m.data));
	         	$.each(m.data, function(i, val) {

	         		var categorias  = '<div id="'+val.nombre+'" class="swipe rt">';
	         		    categorias += 	'<div class="swipe-wrap">';
	         		    categorias += 		'<div>';

	         		    var subcategoria = '';
	    					$.each(val.subcats, function(i, valsub) {
	    						subcategoria += valsub.nombre;
	    						subcategoria += ' ';
	    					});
	         		    subcategoria = subcategoria.replace(" ", ", ");

	         		    						icon = (val.icono == null) ? 'default': val.icono;
	         		    categorias += 			'<div class="reder" onclick=" localStorage.setItem(\'presupuesto_categoryId\','+val.id+'); localStorage.setItem(\'presupuesto_categoryName\',\''+val.nombre+'\'); localStorage.setItem(\'presupuesto_categoryIcon\',\''+icon+'\'); location.href=\'04APP-PRESUPUESTO.html\';">';
	         		    categorias += 				'<img src="images/cat_'+icon+'.png" alt="">	';
	         		    categorias += 				'<div><p class="mot">'+val.nombre+'</p>';
	         		    categorias +=					  '<p>'+subcategoria+'....</p>';
	         		    categorias +=					  '</div>';
	         		    categorias += 			'</div>';
	         		    categorias += 		'</div>';
	         		    			if(val.usuarios_id != null){
	         		    categorias +=		'<div class="desac">';
						categorias +=			'<button class="eliminar d-casa" onclick="$(\'#confirm_delete_cat\').attr(\'onclick\',\'app.categoria.delete('+val.id+')\'); $(\'.alerta\').fadeIn(\'slow\');"><a href="#" style="color:#fff;"> Eliminar</a></button>';
						categorias +=		'</div>';
										}


	         		    categorias += 	'</div>';
	         		    categorias += '</div>';
	         		    $(categorias).insertBefore('div#presupuesto_categorias ul.fr');
	         	});
				$('#presupuesto_categorias').fadeIn(2000);
	         	$('.swipe').Swipe();
	         	$('.loading').remove();
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });


		},//list

		list_offline: function(event) {
			var categories = localStorage.getItem('categoryListOffline');
			    categories = $.parseJSON(categories);
			    
			if(categories){
				var categorias = '';
	         	$.each(categories, function(i, val) {
	         		
	         		    categorias += '<div id="'+val.nombre+'" class="swipe rt">';
	         		    categorias += 	'<div class="swipe-wrap">';
	         		    categorias += 		'<div>';

	         		    var subcategoria = '';
	    					$.each(val.subcats, function(i, valsub) {
	    						subcategoria += valsub.nombre;
	    						subcategoria += ' ';
	    					});
	         		    subcategoria = subcategoria.replace(" ", ", ");

	         		    						icon = (val.icono == null) ? 'default': val.icono;
	         		    categorias += 			'<div class="reder" onclick=" localStorage.setItem(\'presupuesto_categoryId\','+val.id+'); localStorage.setItem(\'presupuesto_categoryName\',\''+val.nombre+'\'); localStorage.setItem(\'presupuesto_categoryIcon\',\''+icon+'\'); location.href=\'04APP-PRESUPUESTO.html\';">';
	         		    categorias += 				'<img src="images/cat_'+icon+'.png" alt="">	';
	         		    categorias += 				'<div><p class="mot">'+val.nombre+'</p>';
	         		    categorias +=					  '<p>'+subcategoria+'....</p>';
	         		    categorias +=					  '</div>';
	         		    categorias += 			'</div>';
	         		    categorias += 		'</div>';
	         		    			if(val.usuarios_id != null){
	         		    //categorias +=		'<div class="desac">';
						//categorias +=			'<button class="eliminar d-casa" onclick="$(\'#confirm_delete_cat\').attr(\'onclick\',\'app.categoria.delete('+val.id+')\'); $(\'.alerta\').fadeIn(\'slow\');"><a href="#" style="color:#fff;"> Eliminar</a></button>';
						//categorias +=		'</div>';
										}


	         		    categorias += 	'</div>';
	         		    categorias += '</div>';
	         		    
	         	});
	         	
         	} else {
         		categorias = "<div>No hay datos guardados</div>"
         	}

         	$(categorias).insertBefore('div#presupuesto_categorias ul.fr');
			$('#presupuesto_categorias').fadeIn(2000);
         	$('.swipe').Swipe();
         	$('.loading').remove();
         	$('#agregar_categoria').hide();

		},//list_offline

		list_select: function(object_dom) {

		    var uid = localStorage.getItem('userId');

		    $.ajax({
	          url: app.categoryUrl,
	          data: {
	            id: uid
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           //alert('Your user/password credentials do not match our records, please try again');
	         } else if(m.status === true) {

	         	var categorias  = '<option value=""></option>';
	         	$.each(m.data, function(i, val) {
	         		 categorias += '<option value="'+val.id+'">';
	         		    categorias += 	val.nombre;
	         		    categorias += '</option>';


	         	});
	         	$('#'+object_dom).html(categorias);
	         	$('.loading').addClass('hide');
	         	$('#'+object_dom).removeClass('hide');
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	       //  alert("We couldn't complete your request at this time, please try again later.");
	       });


		},//list_select

    list_select_offline: function(object_dom) {

		    var uid = localStorage.getItem('userId');
        var list_cat = JSON.parse(localStorage.getItem("categories_list"));
        var categorias = '<option value=""></option>';

        $.each(list_cat, function(i, val) {
          categorias += '<option value="'+val.id+'">';
          categorias += 	val.nombre;
          categorias += '</option>';
        });

        $('#'+object_dom).html(categorias);
        $('.loading').addClass('hide');
        $('#'+object_dom).removeClass('hide');

		},//list_select_offline

    get_list: function() {

      var uid = localStorage.getItem('userId');

      $.ajax({
          url: app.categoryUrl,
          data: {
            id: uid
          },
          beforeSend: function(jqXHR, settings) {
            jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
          }
        }).done(function(message) {
         var m = $.parseJSON(JSON.stringify(message));

         if(m.status === false) {
           //alert('Your user/password credentials do not match our records, please try again');
         } else if(m.status === true) {

          var categorias = [];
          $.each(m.data, function(i, val) {
             categorias.push({
               'id': val.id,
               'nombre': val.nombre
             });
          });
          localStorage.setItem("categories_list", JSON.stringify(categorias));

         }
       }).fail(function(message) {
         var m = JSON.stringify((message));
       //  alert("We couldn't complete your request at this time, please try again later.");
       });

    }, // get_list

		add: function(nameCat){

			var uid = localStorage.getItem('userId');

				$.ajax({
		          url: app.categoryUrl,
		          method: 'PUT',
		          data: {
		            idUser  : uid,
		            nameCat : nameCat,
		          },
		          beforeSend: function(jqXHR, settings) {
		          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
		          }
		        }).done(function(message) {
		         var m = $.parseJSON(JSON.stringify(message));

		         if(m.status === false) {
		         	//alert(m.error);
		         	$('.load').fadeOut('slow');
		           	$(".alerta").fadeIn("slow");
		            $("#mensaje").html(m.error);
		         } else if(m.status === true) {
		         	location.reload();
		         }
		       }).fail(function(message) {
		         var m = JSON.stringify((message));
		        // alert("We couldn't complete your request at this time, please try again later.");
		       });	

		},//add

		delete: function(idCat){

		    $.ajax({
	          url: app.categoryUrl,
	          method: 'DELETE',
	          data: {
	            idCat : idCat,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	         	console.log(m.error);
	           	//$(".alerta").fadeIn("slow");
	            //$("#mensaje").html(m.error);
	         } else if(m.status === true) {
	         	location.reload();
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//delete


	},//CATEGORIAS

	subcategoria: {

		list_check:function(){

			if(app.offline.check_n_get() === 1)
          		app.subcategoria.list();
        	else
          		app.subcategoria.list_offline();

		},//list_check


		list: function(event) {

		    var uid   = localStorage.getItem('userId');
		    var idCat = localStorage.getItem('presupuesto_categoryId');

		    $.ajax({
	          url: app.subcategoryUrl,
	          data: {
	            idUser: uid,
	            idCat : idCat,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	         	$('.load').fadeOut('slow');
	           //alert('Your user/password credentials do not match our records, please try again');
	         } else if(m.status === true) {
	         	//console.log(m.data.total_cat);
	         	localStorage.setItem('SubcategoryListOffline'+localStorage.getItem('presupuesto_categoryName'),JSON.stringify(m.data));
	         	localStorage.setItem('SubcategoryListOfflineTotal'+localStorage.getItem('presupuesto_categoryName'),JSON.stringify(m.total));

				$('#total_cat').html(parseFloat(m.total).formatMoney(2,'.',','));
				$('#name_cat').html(localStorage.getItem('presupuesto_categoryName'));

	         	$.each(m.data, function(i, val) {

	         		var monto = (typeof val.monto[0] !== 'undefined') ? val.monto[0].monto : '0.00' ;
	         		    monto = parseFloat(monto).formatMoney(2,'.',',');

	         		var subcategorias  = '<div class="b-twin" id="bt-4">';
	         		    subcategorias += 	'<div class="selecciones" onclick="localStorage.setItem(\'presupuesto_subcategoryId\','+val.id+'); localStorage.setItem(\'presupuesto_subCategoryName\',\''+val.nombre+'\');  location.href=\'04APP-PRESUPUESTO-SUB.html\';">'+val.nombre+': '+monto+' </div>';
	         		    	if(val.usuarios_id != null){
	         		    subcategorias += 	'<button class="cerrar-i alert-close" id="ct-4" onclick="confirm_delete_subcat('+val.id+')"></button>';
	         				}

	         		    subcategorias += '</div>';

	         		    $(subcategorias).insertBefore('ul.frsub');
	         	});

	         	$('.loading').remove();
	         	$('#presupuesto_subcategorias').fadeIn('slow');
	         	$('.loading').remove();

	         }
	       	}).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

	    },//list

	    list_offline: function(event) {

		    subCat      = localStorage.getItem('SubcategoryListOffline'+localStorage.getItem('presupuesto_categoryName'));
		    subCat      = $.parseJSON(subCat);
         	subCatTotla = localStorage.getItem('SubcategoryListOfflineTotal'+localStorage.getItem('presupuesto_categoryName'));

			$('#total_cat').html(parseFloat(subCatTotla).formatMoney(2,'.',','));
			$('#name_cat').html(localStorage.getItem('presupuesto_categoryName'));

			var subcategorias = '';

			if(subCat){
	         	$.each(subCat, function(i, val) {

	         		var monto = (typeof val.monto[0] !== 'undefined') ? val.monto[0].monto : '0.00' ;
	         		    monto = parseFloat(monto).formatMoney(2,'.',',');

	         		    subcategorias += '<div class="b-twin" id="bt-4">';
	         		    subcategorias += 	'<div class="selecciones" onclick="">'+val.nombre+': '+monto+' </div>';
	         		    //subcategorias += 	'<div class="selecciones" onclick="localStorage.setItem(\'presupuesto_subcategoryId\','+val.id+'); localStorage.setItem(\'presupuesto_subCategoryName\',\''+val.nombre+'\');  location.href=\'04APP-PRESUPUESTO-SUB.html\';">'+val.nombre+': '+monto+' </div>';
	         		    	if(val.usuarios_id != null){
	         		    //subcategorias += 	'<button class="cerrar-i alert-close" id="ct-4" onclick="confirm_delete_subcat('+val.id+')"></button>';
	         				}

	         		    subcategorias += '</div>';

	         		    
	         	});
         	} else {
         		subcategorias += '<div>No hay datos guardados</div>';
         	}

         	$(subcategorias).insertBefore('ul.frsub');

         	$('.loading').remove();
         	$('#presupuesto_subcategorias').fadeIn('slow');
         	$('.loading').remove();
         	$('#agregar_subcat').hide();


	    },//list_offline

      list_select: function(object_dom, idCat) {

        $('#'+object_dom).addClass('hide');
        $('#loadingSub').removeClass('hide');

        if(app.offline.check_n_get() === 1)
          app.subcategoria.list_select_online(object_dom, idCat);
        else
          app.subcategoria.list_select_offline(object_dom, idCat);

      }, // list_select

	    list_select_online: function(object_dom,idCat) {

		    var uid   = localStorage.getItem('userId');

		    $.ajax({
	          url: app.subcategoryUrl,
	          data: {
	            idUser: uid,
	            idCat : idCat,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	         	$('.load').fadeOut('slow');
	           //alert('Your user/password credentials do not match our records, please try again');
	         } else if(m.status === true) {
	         	//console.log(m.data.total_cat);

	         	var subcategorias  = '<option value=""></option>';
	         	$.each(m.data, function(i, val) {
	         		 subcategorias += '<option value="'+val.id+'">';
	         		 subcategorias += 	val.nombre;
	         		 subcategorias += '</option>';
	         	});
	         	$('#select_subcat').html(subcategorias);
	         	$('#loadingSub').addClass('hide');
	         	$('#select_subcat').removeClass('hide');

	         }
	       	}).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });


		},//list_select_online

    list_select_offline: function(object_dom,idCat) {

      var uid   = localStorage.getItem('userId');
      var subCat_list = JSON.parse(localStorage.getItem("subcategories_list"));
      var subCat = $.grep(subCat_list, function(elem) { return elem.categorias_id === idCat; });

      var subcategorias  = '<option value=""></option>';
      $.each(subCat, function(i, val) {
         subcategorias += '<option value="'+val.id+'">';
         subcategorias += 	val.nombre;
         subcategorias += '</option>';
      });
      $('#'+object_dom).html(subcategorias);
      $('#loadingSub').addClass('hide');
      $('#'+object_dom).removeClass('hide');

    },//list_select_offline

		add: function(nameSubCat){

			var uid   = localStorage.getItem('userId');
			var idCat = localStorage.getItem('presupuesto_categoryId');

		    $.ajax({
	          url: app.subcategoryUrl,
	          method: 'PUT',
	          data: {
	            idUser     : uid,
	            idCat      : idCat,
	            nameSubCat : nameSubCat,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	         	$('.load').fadeOut('slow');
	           	$(".alerta").fadeIn("slow");
	            $("#mensaje").html(m.error);
	         } else if(m.status === true) {
	         	location.reload();
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//add

		delete: function(idSubCat){

		    $.ajax({
	          url: app.subcategoryUrl,
	          method: 'DELETE',
	          data: {
	            idSubCat : idSubCat,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	         	console.log(m.error);
	           	//$(".alerta").fadeIn("slow");
	            //$("#mensaje").html(m.error);
	         } else if(m.status === true) {
	         	location.reload();
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//delete

    get_list: function() {

      var uid = localStorage.getItem('userId');
      var subcategorias = [];

      $.ajax({
          url: app.subcategoryAllUrl,
          method: 'GET',
          data: {
            idUser: uid
          },
          beforeSend: function(jqXHR, settings) {
            jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
          }
        }).done(function(message) {
         var m = $.parseJSON(JSON.stringify(message));

         if(m.status === false) {
           //alert('Your user/password credentials do not match our records, please try again');
         } else if(m.status === true) {

          var subcat = [];
          $.each(m.data, function(i, val) {
             subcat.push({
               'id':            val.id,
               'categorias_id': val.categorias_id,
               'nombre':        val.nombre,
             });
          });

          localStorage.setItem("subcategories_list", JSON.stringify(subcat));

         }
       }).fail(function(message) {
         var m = JSON.stringify((message));
       //  alert("We couldn't complete your request at this time, please try again later.");
       });

    }, // get_list

	}, // SUBCATEGORIA

	presupuesto: {
		add: function(monto){

			var uid      = localStorage.getItem('userId');
			var idCat    = localStorage.getItem('presupuesto_categoryId');
			var idSubCat = localStorage.getItem('presupuesto_subcategoryId');

			$.ajax({
	          url: app.estimationUrl,
	          method: 'PUT',
	          data: {
	            idUser   : uid,
	            idCat    : idCat,
	            idSubCat : idSubCat,
	            monto    : monto,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	         	$('.load').fadeOut('slow');
	            $(".alerta").fadeIn("slow");
	            $("#mensaje").html(m.error);
	         } else if(m.status === true) {
	         	location.href = '04APP-PRESUPUESTO.html';
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });


		},//add

		table: function(){

			var uid      = localStorage.getItem('userId');

			$.ajax({
	          url: app.estimationtableUrl,
	          method: 'get',
	          data: {
	            idUser   : uid,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));
	         var icon = '';
	         if(m.status === false) {
	           	alert(m.error);
	         } else if(m.status === true) {

	         	var table = '';
	         	var gasto = 0;
	         	var presupuesto = 0
	         	var ahorro = 0;
	         	var class_ahorro = '';
	         	var total_ahorro = 0;
	         	var plus = '';

	         	$.each(m.data, function(i, val) {

	         		table += '<tr>';
	         		table += 	'<td>';
	         		table += 		'<div class="m-gasto">';
	         							icon = (val.icono == null) ? 'default': val.icono;
	         		table += 			'<img src="images/list_'+icon+'.png" alt="">';
	         		table +=            '<p>'+val.nombre+'</p>';
	         		table += 		'</div>';
	         		table += 	'</td>';
	         			presupuesto  = (val.presupuesto)? val.presupuesto : 0;
	         		table += 	'<td>'+parseFloat(presupuesto).formatMoney(2,'.',',')+'</td>';
						gasto  = (val.gasto)? val.gasto : 0;
	         		table += 	'<td>'+parseFloat(gasto).formatMoney(2,'.',',')+'</td>';
	         			ahorro = presupuesto - gasto;
	         			class_ahorro = (ahorro < 0)? "active-td" : "";
	         			plus = (ahorro > 0)? "+" : "";
	         		table += 	'<td class="'+class_ahorro+'">'+plus+parseFloat(ahorro).formatMoney(2,'.',',')+'</td>';
	         		table += '</tr>';

	         	});

	         	table += '<tr class="totales">';
	         	table += 	'<td>';
	         	table += 		'<div class="m-gasto">';
	         	table += 			'<img src="images/icon59.png" alt="">';
	         	table += 			'<p>Totales</p>	';
	         	table += 		'</div>';
	         	table += 	'</td>';
	         	table += 	'<td>'+parseFloat(m.total_pre).formatMoney(2,'.',',');+'</td>';
	         	table += 	'<td>'+parseFloat(m.total_gasto).formatMoney(2,'.',',');+'</td>';
	         	 	total_ahorro = m.total_pre - m.total_gasto;
	         	 	class_ahorro = (total_ahorro < 0)? "active-td" : "";
	         		plus         = (total_ahorro > 0)? "+" : "";
	         	table += 	'<td class="'+class_ahorro+'">'+plus+parseFloat(total_ahorro).formatMoney(2,'.',',');+'</td>';
	         	table += '</tr>';

	         	 $('#tabla_presupuesto').html(table);
	         	 $('.loading').remove();

	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));

	      //   alert("We couldn't complete your request at this time, please try again later.");
	       });


		},//table

		grafica:function(){

			var uid  = localStorage.getItem('userId');
			var anio = localStorage.getItem('gr-anio');
			var mes  = localStorage.getItem('gr-mes');

			$.ajax({
	          url: app.graficaPresupuestoUrl,
	          method: 'GET',
	          data: {
	            idUser : uid,
	            anio   : anio,
	            mes    : mes,
		        },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         	var m = $.parseJSON(JSON.stringify(message));

		         if(m.status === false) {
		           	console.log(m.error);
		         } else if(m.status === true) {
		         	
		         	if(m.gasto.length){

		         		localStorage.setItem('graficaPresupuesto'+anio+mes,JSON.stringify(m));

		         		$('#container').highcharts({
			                chart: {
			                    type: 'column'
			                },
			                 title: null,
			                 navigation: {
			                    buttonOptions: {
			                        enabled: false
			                    }
			                },
			                credits: {
			                     enabled: false
			                },
			                xAxis: {
			                    categories: m.categorias
			                },
			              yAxis: {
			                        min:0,
			                        title:null,
			                        labels: 
			                                { 
			                                  formatter: function() 
			                                  {
			                                     return this.value.formatMoney(0,'.',',');
			                                  }
			                                }
			                    },
			                tooltip: {
			                    shared: true
			                },
			                plotOptions: {
			                    column: {
			                        grouping: false,
			                        shadow: false,
			                        borderWidth: 0
			                    }
			                },
			                series: [
			                {
			                    name: 'Presupuesto',
			                    color: 'rgba(182,225,164,.9)',
			                    data: m.presupuesto,
			                    pointPadding: 0.3
			                }, {
			                    name: 'Total gastado',
			                    color: 'rgba(103,188,69,1)',
			                    data: m.gasto,
			                    pointPadding: 0.4
			                },{
			                    name: 'Saldo',
			                    type: 'spline',
			                    color: 'transparent',
			                    data: m.saldo
			                }]
			            });

		         	} else {
		         		$('#container').html('<div>No hay datos guardados</div>');
		         	}

		         	$('.loading').remove();
		         }

		       }).fail(function(message) {
		         var m = JSON.stringify((message));
		        // alert("We couldn't complete your request at this time, please try again later.");
		       });

		},//grafica

		grafica_offline:function(){

			var uid  = localStorage.getItem('userId');
			var anio = localStorage.getItem('gr-anio');
			var mes  = localStorage.getItem('gr-mes');

			var graficaPresupuesto = localStorage.getItem('graficaPresupuesto'+anio+mes);
			    graficaPresupuesto = $.parseJSON(graficaPresupuesto);

			if(graficaPresupuesto){

				$('#container').highcharts({
			                chart: {
			                    type: 'column'
			                },
			                 title: null,
			                 navigation: {
			                    buttonOptions: {
			                        enabled: false
			                    }
			                },
			                credits: {
			                     enabled: false
			                },
			                xAxis: {
			                    categories: graficaPresupuesto.categorias
			                },
			              yAxis: {
			                        min:0,
			                        title:null,
			                        labels: 
			                                { 
			                                  formatter: function() 
			                                  {
			                                     return this.value.formatMoney(0,'.',',');
			                                  }
			                                }
			                    },
			                tooltip: {
			                    shared: true
			                },
			                plotOptions: {
			                    column: {
			                        grouping: false,
			                        shadow: false,
			                        borderWidth: 0
			                    }
			                },
			                series: [
			                {
			                    name: 'Presupuesto',
			                    color: 'rgba(182,225,164,.9)',
			                    data: graficaPresupuesto.presupuesto,
			                    pointPadding: 0.3
			                }, {
			                    name: 'Total gastado',
			                    color: 'rgba(103,188,69,1)',
			                    data: graficaPresupuesto.gasto,
			                    pointPadding: 0.4
			                },{
			                    name: 'Saldo',
			                    type: 'spline',
			                    color: 'transparent',
			                    data: graficaPresupuesto.saldo
			                }]
			            });

			}else{
				$('#container').html('<div>No hay datos guardados</div>');
			}

			$('.loading').remove();

		},//grafica

		grafica_check:function(){

			if(app.offline.check_n_get() === 1)
				app.presupuesto.grafica();
		    else
		    	app.presupuesto.grafica_offline();
		        
		    
		},//grafica_check

		grafica_anual_check:function(){

			if(app.offline.check_n_get() === 1)
				app.presupuesto.grafica_anual();
		    else
		    	app.presupuesto.grafica_anual_offline();

		},//grafica_check


		grafica_anual:function(){

			var uid  = localStorage.getItem('userId');
			var anio = localStorage.getItem('gr-anio');

			$.ajax({
	          url: app.graficaPresupuestoAnualUrl,
	          method: 'GET',
	          data: {
	            idUser : uid,
	            anio   : anio,
		        },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         	var m = $.parseJSON(JSON.stringify(message));

		         if(m.status === false) {
		           	console.log(m.error);
		         } else if(m.status === true) {

		         	if(m.gasto.length){

		         		localStorage.setItem('graficaPresupuesto'+anio,JSON.stringify(m));

			         	$('#container').highcharts({
				                chart: {
				                    type: 'column'
				                },
				                 title: null,
				                 navigation: {
				                    buttonOptions: {
				                        enabled: false
				                    }
				                },
				                credits: {
				                     enabled: false
				                },
				                xAxis: {
				                    categories: ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]
				                },
				                yAxis: {
				                        min:0,
				                        title:null,
				                        labels: 
				                                { 
				                                  formatter: function() 
				                                  {
				                                     return this.value.formatMoney(0,'.',',');
				                                  }
				                                }
				                    },
				                tooltip: {
				                    shared: true
				                },
				                plotOptions: {
				                    column: {
				                        grouping: false,
				                        shadow: false,
				                        borderWidth: 0
				                    }
				                },
				                series: [
				                {
				                    name: 'Presupuesto',
				                    type: 'spline',
				                    color: 'rgba(182,225,164,.9)',
				                    data: m.presupuesto,
				                    pointPadding: 0.3
				                }, {
				                    name: 'Total gastado',
				                    color: 'rgba(103,188,69,1)',
				                    data: m.gasto,
				                    pointPadding: 0.4
				                },{
				                    name: 'Saldo',
				                    type: 'spline',
				                    data: m.saldo,
				                    color: 'transparent',
				                }]
				        });

		         	} else {
		         		$('#container').html('<div>No hay datos guardados</div>');
		         	}

		         	$('.loading').remove();
		         }

		       }).fail(function(message) {
		         var m = JSON.stringify((message));
		        // alert("We couldn't complete your request at this time, please try again later.");
		       });

		},//grafica

		grafica_anual_offline:function(){

			var uid  = localStorage.getItem('userId');
			var anio = localStorage.getItem('gr-anio');

			var graficaPresupuesto = localStorage.getItem('graficaPresupuesto'+anio);
			    graficaPresupuesto = $.parseJSON(graficaPresupuesto);

			if(graficaPresupuesto){

				$('#container').highcharts({
				                chart: {
				                    type: 'column'
				                },
				                 title: null,
				                 navigation: {
				                    buttonOptions: {
				                        enabled: false
				                    }
				                },
				                credits: {
				                     enabled: false
				                },
				                xAxis: {
				                    categories: ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]
				                },
				                yAxis: {
				                        min:0,
				                        title:null,
				                        labels: 
				                                { 
				                                  formatter: function() 
				                                  {
				                                     return this.value.formatMoney(0,'.',',');
				                                  }
				                                }
				                    },
				                tooltip: {
				                    shared: true
				                },
				                plotOptions: {
				                    column: {
				                        grouping: false,
				                        shadow: false,
				                        borderWidth: 0
				                    }
				                },
				                series: [
				                {
				                    name: 'Presupuesto',
				                    type: 'spline',
				                    color: 'rgba(182,225,164,.9)',
				                    data: graficaPresupuesto.presupuesto,
				                    pointPadding: 0.3
				                }, {
				                    name: 'Total gastado',
				                    color: 'rgba(103,188,69,1)',
				                    data: graficaPresupuesto.gasto,
				                    pointPadding: 0.4
				                },{
				                    name: 'Saldo',
				                    type: 'spline',
				                    data: graficaPresupuesto.saldo,
				                    color: 'transparent',
				                }]
				        });

			} else {
		    	$('#container').html('<div>No hay datos guardados</div>');
         	}

         	$('.loading').remove();

		},//grafica

		ahorraste: function(){
			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.ahorrastePresupuestoUrl,
	          method: 'GET',
	          data: {
	            idUser : uid,
	            anio   : 0,
	            mes    : 0,
		        },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         	var m = $.parseJSON(JSON.stringify(message));

		         if(m.status === false) {
		           	console.log(m.error);
		         } else if(m.status === true) {
		         	$("#monto").html(parseFloat(m.total).formatMoney(2,'.',','));
		         	$('.loading').remove();
		         }

		       }).fail(function(message) {
		         var m = JSON.stringify((message));
		        // alert("We couldn't complete your request at this time, please try again later.");
		       });

		},//ahorraste

		total:function(){

			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.totalPresupuestoUrl,
	          data: {
	            idUser  : uid,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           	//alert(m.error);
	         } else if(m.status === true) {

	         	if(m.data[0].total !== null){

	         		if(m.data[0].total.length > 9)
	         			$("#total_presupuesto").css('font-size', '0.9em');
	         	}

	         	$("#total_presupuesto").html(parseFloat(m.data[0].total).formatMoney(2,'.',','));
            localStorage.setItem("total_presupuesto", parseFloat(m.data[0].total).formatMoney(2,'.',''));


	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//total_presupuesto

    total_offline: function() {
      var uid  = localStorage.getItem('userId');

      var total_presupuesto = localStorage.getItem('total_presupuesto');

      if( !total_presupuesto )
        $("#total_presupuesto").html("0.00");
      else
        $("#total_presupuesto").html(parseFloat(total_presupuesto).formatMoney(2, '.', ','));

    }, // total_mes_offline

		total_ahorrado:function(){

			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.totalAhorradoUrl,
	          data: {
	            idUser  : uid,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           	//alert(m.error);
	         } else if(m.status === true) {

	         	if(m.data[0].total !== null){

	         		if(m.data[0].total.length > 9)
	         			$("#total_ahorrado").css('font-size', '0.9em');
	         	}

	         	$("#total_ahorrado").html(parseFloat(m.data[0].total).formatMoney(2,'.',','));
            localStorage.setItem("total_ahorrado", parseFloat(m.data[0].total).formatMoney(2,'.',''));

	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//total_ahorrado

    total_ahorrado_offline: function() {
      var uid  = localStorage.getItem('userId');

      var total_ahorrado = localStorage.getItem('total_ahorrado');

      if( !total_ahorrado )
        $("#total_ahorrado").html("0.00");
      else
        $("#total_ahorrado").html(parseFloat(total_ahorrado).formatMoney(2, '.', ','));

    }, // total_mes_offline

		add_default_notification:function(){
			var d     = new Date();
			var day   = d.getDate();
			var month =	d.getMonth();
			var year  = d.getFullYear();

			var date_  = new Date(year,month,day,20,0,0,0).getTime();
			app.notificaciones.add(0,date_,"¿Ya registraste tus gastos del día de hoy?","day","Recordatorio creado por el sistema");
			console.log('create default notification');

		},//add_default_notification

	},//PRESUPUESTO

	alarmas:{

		add:function(){

			var uid         = localStorage.getItem('userId');
			var date_       = $( "input[name$='date']" ).val();
			var date        = $( "input[name$='date']" ).val();
			    date        = date.split("-");
       		var time_       = $( "input[name$='time']" ).val();
       		var time        = $( "input[name$='time']" ).val();
       	   		time        = time.split(":");
       		var milisec     = new Date(date[0], date[1]-1, date[2], time[0], time[1], 00, 00).getTime();
       		var nombre      = $( "input[name$='nombre']" ).val();
       		var repetir     = $( "#repetir" ).val();
       		var descripcion = $( "textarea[name$='descripcion']" ).val();

       		var idAlarm = app.alarm_storage.generateId(uid);

	      	if( app.offline.check_n_get() === 1 ) {

	      		var verify = 0;
	          if(nombre === "" || nombre === null)
	            verify = 1;
	          if(!date_.match(/^\d{4}-\d{2}-\d{2}?/g))
	            verify = 2;
	          if(!time_.match(/^\d{2}:\d{2}?/g))
            	verify = 3;
              if(descripcion === "" || descripcion === null)
              	verify = 4;

            	switch( verify ) {
		            case 0:
		           		app.notificaciones.add(idAlarm, milisec, nombre, repetir, descripcion);
		           		$.ajax({
					          url: app.alarm,
					          method: 'PUT',
					          data: {
				                id          : idAlarm,
					            idUser      : uid,
					            nombre      : $( "input[name$='nombre']" ).val(),
					            descripcion : $( "textarea[name$='descripcion']" ).val(),
					            date        : $( "input[name$='date']" ).val(),
					            time        : $( "input[name$='time']" ).val(),
					            repetir     : $( "#repetir" ).val()
					          },
					          beforeSend: function(jqXHR, settings) {
					          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
					          }
					        }).done(function(message) {

					         var m = $.parseJSON(JSON.stringify(message));

					         if(m.status === false) {
					         	$('.load').fadeOut('slow');
					           	$(".alerta").fadeIn("slow");
					            $("#mensaje").html(m.error);

					         } else if(m.status === true) {

					         	if(localStorage.getItem('userProceso') == 3){
					         		location.href = '01APP-RECORDATORIOS.html';
					         	} else {
					         		location.href = '01APP-HOME.html';
					         	}

					         }
					        }).fail(function(message) {
					         var m = JSON.stringify((message));
					        // alert("We couldn't complete your request at this time, please try again later.");
					        });
		              break;
		            case 1:
		              $('.load').fadeOut('slow');
		              $(".alerta").fadeIn("slow");
		              $("#mensaje").html('<span>Favor de rellenar campo: Nombre</span>');
		              break;
		            case 2:
		              $('.load').fadeOut('slow');
		              $(".alerta").fadeIn("slow");
		              $("#mensaje").html('<span>Favor de rellenar campo: Fecha</span>');
		              break;
		            case 3:
		              $('.load').fadeOut('slow');
		              $(".alerta").fadeIn("slow");
		              $("#mensaje").html('<span>Favor de rellenar campo: Hora</span>');
		              break;
		            case 4:
		              $('.load').fadeOut('slow');
		              $(".alerta").fadeIn("slow");
		              $("#mensaje").html('<span>Favor de rellenar campo: Descripción</span>');
		              break;
		            default:
		              $('.load').fadeOut('slow');
		              $(".alerta").fadeIn("slow");
		              $("#mensaje").html('<span>Error de validación, consulte a soporte.</span>');
		              return;
		              break;
         		}

			    
	        } else {

	          // CREAR OBJETO DE DATOS
	          var data = {
	            id          : idAlarm,
	            idUser      : uid,
	            nombre      : $( "input[name$='nombre']" ).val(),
	            descripcion : $( "textarea[name$='descripcion']" ).val(),
	            date        : $( "input[name$='date']" ).val(),
	            time        : $( "input[name$='time']" ).val(),
	            repetir     : $( "#repetir" ).val(),
	            estatus     : 1,
	          }

	          // VERIFICAR DATOS
	          var verify = 0;
	          if(data.nombre === "" || data.nombre === null)
	            verify = 1;
	          if(!data.date.match(/^\d{4}-\d{2}-\d{2}?/g))
	            verify = 2;
	          if(!data.time.match(/^\d{2}:\d{2}?/g))
            	verify = 3;
              if(descripcion === "" || descripcion === null)
              	verify = 4;

	          // MOSTRAR MENSAJE SI LA VERIFICACIÓN NO PASO
	          $('.load').fadeOut('slow');
	          	switch( verify ) {
		            case 0:
		              // AGREGAR OBJETO A LOS DATOS
		              id_temp = app.alarm_storage.add(data);

		              // CREAR ALARMA
		              app.notificaciones.add(id_temp, milisec, data.nombre, data.repetir, data.descripcion);

		              // REDIRECCIONAR
		              location.href = "01APP-RECORDATORIOS.html";
		              break;
		            case 1:
		              $(".alerta").fadeIn("slow");
		              $("#mensaje").html('<span>Favor de rellenar campo: Nombre</span>');
		              break;
		            case 2:
		              $(".alerta").fadeIn("slow");
		              $("#mensaje").html('<span>Favor de rellenar campo: Fecha</span>');
		              break;
		            case 3:
		              $(".alerta").fadeIn("slow");
		              $("#mensaje").html('<span>Favor de rellenar campo: Hora</span>');
		              break;
		            case 4:
		              $('.load').fadeOut('slow');
		              $(".alerta").fadeIn("slow");
		              $("#mensaje").html('<span>Favor de rellenar campo: Descripción</span>');
		              break; 
		            default:
		              $(".alerta").fadeIn("slow");
		              $("#mensaje").html('<span>Error de validación, consulte a soporte.</span>');
		              break;
         		}

        	}

		},//add

		update_all:function(idAlarma){

			 var uid  = localStorage.getItem('userId');
			 var date = $( "input[name$='date']" ).val();
			     date = date.split("-");
	         var time = $( "input[name$='time']" ).val();
	         	 time = time.split(":");
	         var milisec = new Date(date[0], date[1]-1, date[2], time[0], time[1], 00, 00).getTime();
	         var nombre = $( "input[name$='nombre']" ).val();
	         var repetir = $( "#repetir" ).val();

        if( app.offline.check_n_get() === 1 ) {
			    $.ajax({
	          url: app.alarm,
	          method: 'POST',
	          data: {
	            idAlarma    : idAlarma,
	            nombre      : $( "input[name$='nombre']" ).val(),
	            date        : $( "input[name$='date']" ).val(),
	            time        : $( "input[name$='time']" ).val(),
              descripcion : $( "textarea[name$='descripcion']" ).val(),
	            repetir     : $( "#repetir" ).val(),
	            estatus     : 1,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	         	$('.load').fadeOut('slow');
	           	$(".alerta").fadeIn("slow");
	            $("#mensaje").html(m.error);
	         } else if(m.status === true) {

            var data = {
 	            id          : idAlarma,
 	            nombre      : $( "input[name$='nombre']" ).val(),
 	            date        : $( "input[name$='date']" ).val(),
 	            time        : $( "input[name$='time']" ).val(),
               descripcion : $( "textarea[name$='descripcion']" ).val(),
 	            repetir     : $( "#repetir" ).val(),
 	            estatus     : 1,
 	          }

	         	app.notificaciones.update_all(data, milisec);

	         	location.href = '01APP-RECORDATORIOS.html';

	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

       } else {

         // CREAR OBJETO DE DATOS
         var data = {
           idAlarma    : idAlarma,
           nombre      : $( "input[name$='nombre']" ).val(),
           descripcion : $( "textarea[name$='descripcion']" ).val(),
           date        : $( "input[name$='date']" ).val(),
           time        : $( "input[name$='time']" ).val(),
           repetir     : $( "#repetir" ).val(),
           estatus     : 1,
         }

         // VERIFICAR DATOS
         var verify = 0;
         if(data.nombre === "" || data.nombre === null)
           verify = 1;
         if(!data.date.match(/^\d{4}-\d{2}-\d{2}?/g))
           verify = 2;
         if(!data.time.match(/^\d{2}:\d{2}?/g))
           verify = 3;

         // MOSTRAR MENSAJE SI LA VERIFICACIÓN NO PASO
         $('.load').fadeOut('slow');
         switch( verify ) {
           case 0:
             // AGREGAR OBJETO A LOS DATOS
             id_temp = app.alarm_storage.edit(idAlarma, data);

             // CREAR ALARMA
             app.notificaciones.update_offline(idAlarma);

             // REDIRECCIONAR
             location.href = "01APP-RECORDATORIOS.html";
             break;
           case 1:
             $(".alerta").fadeIn("slow");
             $("#mensaje").html('<span>Favor de rellenar campo: Nombre</span>');
             break;
           case 2:
             $(".alerta").fadeIn("slow");
             $("#mensaje").html('<span>Favor de rellenar campo: Fecha</span>');
             break;
           case 3:
             $(".alerta").fadeIn("slow");
             $("#mensaje").html('<span>Favor de rellenar campo: Hora</span>');
             break;
           default:
             $(".alerta").fadeIn("slow");
             $("#mensaje").html('<span>Error de validación, consulte a soporte.</span>');
             break;
         }

       }

		},//update_all

		update:function(idAlarma, estatus){

			 var uid  = localStorage.getItem('userId');

       if(app.offline.check_n_get() === 1) {
			    $.ajax({
	          url: app.alarm,
	          method: 'POST',
	          data: {
	            idUser    : uid,
	            idAlarma  : idAlarma,
	            estatus   : estatus,

	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           	alert(m.error);
	         } else if(m.status === true) {

	         	if(estatus == 1)
	         		app.notificaciones.update(idAlarma);
	         	else
	         		app.notificaciones.cancel(idAlarma);

	         	location.reload();

	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });
       } else {
         $('.offline').css("display","block");
       }

		},//update

		delete:function(idAlarma){

      if(app.offline.check_n_get() === 1) {
		    $.ajax({
          url: app.alarm,
          method: 'DELETE',
          data: {
            idAlarma  : idAlarma,
          },
          beforeSend: function(jqXHR, settings) {
          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
          }
        }).done(function(message) {
         var m = $.parseJSON(JSON.stringify(message));

         if(m.status === false) {
           	alert(m.error);
         } else if(m.status === true) {
           app.notificaciones.cancel(idAlarma);
         	location.reload();

         }
        }).fail(function(message) {
         var m = JSON.stringify((message));
        // alert("We couldn't complete your request at this time, please try again later.");
        });
      } else {

        $('.offline').css("display","block");

      }

		},//delete

    list:function(){
      if(app.offline.check_n_get() === 1)
        app.alarmas.list_online();
      else
        app.alarmas.list_offline();
    }, //list

		list_online:function(){

			var uid       = localStorage.getItem('userId');

			$.ajax({
	          url: app.alarm,
	          data: {
	          	idUser      : uid,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	         	$('.loading').remove();
	           	console.log(m.error);
	         } else if(m.status === true) {

	         	var list_alarmas = '';

	         	$.each(m.data, function(i, val) {

	         		list_alarmas += '<div class="swipe rt">';
	         		list_alarmas += 	'<div class="swipe-wrap">';
	         		list_alarmas += 		'<div>';
	         		list_alarmas += 			'<div class="reder">	';
	         											var alarmaIcon = (val.estatus == 1) ? 'alarma' : 'alarma_gris';
	         		list_alarmas += 				'<img src="images/'+alarmaIcon+'.png" alt="">';
	         		list_alarmas += 				'<div>';
	         											var style = (val.estatus == 1) ? '' : 'style="color:#c5c5c5"';
	         		if(val.estatus == 1)
	         			list_alarmas += 				'<p class="mot" '+style+' onclick="localStorage.setItem(\'alarmaId\','+val.id+'); localStorage.setItem(\'alarmaName\',\''+val.nombre+'\'); location.href=\'04APP-RECORDATORIOS-EDIT.html\';">'+val.nombre+'</p>';

	         		else
	         			list_alarmas += 		       '<p class="mot" '+style+'>'+val.nombre+'</p>';


	         		list_alarmas += 					'<p>'+val.descripcion+'</p>';
	         		list_alarmas += 				'</div>';
	         		list_alarmas += 			'</div>';
	         		list_alarmas += 		'</div>';
	         		list_alarmas += 		'<div class="desac">';
	         		if(val.estatus == 1)
	         			list_alarmas += 			'<button class="desactivar" onclick=" $(\'.load\').fadeIn(\'slow\');  app.alarmas.update('+val.id+',0);">Desactivar</button>';

	         		else
	         			list_alarmas += 			'<button class="desactivar" onclick="app.alarmas.update('+val.id+',1);">Activar</button>';

	         		list_alarmas +=				'<button class="eliminar" onclick=" $(\'.alerta\').fadeIn(\'slow\'); $(\'#confirm_delete_alarm\').attr(\'onclick\',\'app.alarmas.delete('+val.id+')\');">Eliminar</button>';
	         		list_alarmas += 		'</div>';
	         		list_alarmas +=		 '</div>';
	         		list_alarmas += '</div>';

	         	});

	         	$('#div_list_alarms').html(list_alarmas);
	         	$('.swipe').Swipe();
	         	$('.loading').remove();

	         }
	       }).fail(function(message) {
	       	 $('.loading').remove();
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

	      /* cordova.plugins.notification.local.getIds(function (ids) {
                    console.log('IDs: ' + ids.join(' ,'));
                });*/

		},//list_online

    list_offline: function() {

      // VERIFICAR SI EXISTE ALARM_LIST
      if(!localStorage.getItem("alarm_list"))
        var alm_list = [];
      else
        var alm_list = JSON.parse(localStorage.getItem("alarm_list"));

      // MOSTRAR REGISTROS CREADOS OFFLINE
      // // COMPROBAR SI EXISTE INS_ALARM
      // if( !localStorage.getItem("ins_alarm") )
      //   var ins_alm = "";
      // else
      //   var ins_alm = localStorage.getItem("ins_alarm");
      //
      // // COMBINAR ARREGLOS
      // if( ins_alm !== "" ) {
      //   ins_alm = JSON.parse(ins_alm);
      //   alm_list = alm_list.concat(ins_alm);
      // }

      var list_alarmas = "";

      $.each(alm_list, function(i, val) {
        list_alarmas += '<div class="swipe rt">';
        list_alarmas += 	'<div class="swipe-wrap">';
        list_alarmas += 		'<div>';
        list_alarmas += 			'<div class="reder">	';
                          var alarmaIcon = (val.estatus == 1) ? 'alarma' : 'alarma_gris';
        list_alarmas += 				'<img src="images/'+alarmaIcon+'.png" alt="">';
        list_alarmas += 				'<div>';
                          var style = (val.estatus == 1) ? '' : 'style="color:#c5c5c5"';
        if(val.estatus == 1)
          list_alarmas += 				'<p class="mot" '+style+' onclick="localStorage.setItem(\'alarmaId\','+val.id+'); localStorage.setItem(\'alarmaName\',\''+val.nombre+'\'); location.href=\'04APP-RECORDATORIOS-EDIT.html\';">'+val.nombre+'</p>';

        else
          list_alarmas += 		       '<p class="mot" '+style+'>'+val.nombre+'</p>';


        list_alarmas += 					'<p>'+val.descripcion+'</p>';
        list_alarmas += 				'</div>';
        list_alarmas += 			'</div>';
        list_alarmas += 		'</div>';
        list_alarmas += 		'<div class="desac">';
        if(val.estatus == 1)
          list_alarmas += 			'<button class="desactivar" onclick=" $(\'.load\').fadeIn(\'slow\');  app.alarmas.update('+val.id+',0);">Desactivar</button>';

        else
          list_alarmas += 			'<button class="desactivar" onclick="app.alarmas.update('+val.id_tmp+',1);">Activar</button>';

        list_alarmas +=				'<button class="eliminar" onclick=" $(\'.alerta\').fadeIn(\'slow\'); $(\'#confirm_delete_alarm\').attr(\'onclick\',\'app.alarmas.delete('+val.id+')\');">Eliminar</button>';
        list_alarmas += 		'</div>';
        list_alarmas +=		 '</div>';
        list_alarmas += '</div>';
      });

      $('#div_list_alarms').html(list_alarmas);
      $('.loading').remove();

    }, //list_offline

    get_list: function() {

      var uid = localStorage.getItem('userId');

      $.ajax({
          url: app.alarm,
          method: 'GET',
          data: {
            idUser: uid
          },
          beforeSend: function(jqXHR, settings) {
            jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
          }
        }).done(function(message) {
         var m = $.parseJSON(JSON.stringify(message));

         if(m.status === false) {
            console.log('No se encontraron datos.');
            $('.loading').remove();
         } else if(m.status === true) {
           var alarmas = [];
           $.each(m.data, function(i, val) {
             var fecha_inicio = val.fecha_inicio.split(" "),
                 fecha = fecha_inicio[0],
                 hora = fecha_inicio[1];
             alarmas.push({
               'id': val.id,
               'nombre': val.nombre,
               'descripcion': val.descripcion,
               'fecha': fecha,
               'hora': hora,
               'repetir': val.recurrente,
               'estatus': val.estatus,
             });
           });
           localStorage.setItem("alarm_list", JSON.stringify(alarmas));
         }
       }).fail(function(message) {
         var m = JSON.stringify((message));
        // alert("We couldn't complete your request at this time, please try again later.");
       });

    }, //get_list

		get:function(idAlarma, callback){

			var me = this;
			me.callback = callback;

			var uid       = localStorage.getItem('userId');

			$.ajax({
	          url: app.alarm,
	          data: {
	          	idUser      : uid,
	            idAlarma    : idAlarma,
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           	console.log(m.error);
	         } else if(m.status === true) {
	         	me.callback(m.data);
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//get

	},//ALARMAS

	notificaciones:{

		add:function(id,fecha,nombre,repetir, descripcion){

			  var date = new Date(fecha);

            cordova.plugins.notification.local.schedule({
					    id: id,
					    title: "Presupuesto Mensual",
					    text: nombre,
					    at: date,
					    every:repetir,
					    data: { "descripcion": descripcion }
					});

		},//add

		cancel:function(id){

			  cordova.plugins.notification.local.cancel(id);

		},//cancel

		update:function(id){

			app.alarmas.get(id,function(data){

 				 cordova.plugins.notification.local.schedule({
					    id: id,
					    title: "Presupuesto Mensual",
					    text: data.nombre,
					   // at: date,
					    every:data.recurrente
					});

				/*cordova.plugins.notification.local.update({
                    id: id,
                    text: data.nombre,
                    at: date,
                    every:data.repetir,
                    json: { updated: true }
                });*/

			});

		},//update

    update_offline:function(idAlarm) {

      var alarm_list = JSON.parse(localStorage.getItem("alarm_list"));

      var record_index = 0;
      for(i = 0; i < alarm_list.length; i++) {
        if( alarm_list[i].id == idAlarm ) {
          record_index = i;
          record = alarm_list[i];
          break;
        }
      }

      var date        = alarm_list[ record_index ].fecha;
          date        = date.split("-");
      var time        = alarm_list[ record_index ].hora;
          time        = time.split(":");
      var milisec     = new Date(date[0], date[1]-1, date[2], time[0], time[1], 00, 00).getTime();

      cordova.plugins.notification.local.update({
        id: idAlarm,
        title: "Presupuesto Mensual",
        text: alarm_list[ record_index ].nombre,
        at: milisec,
        every: alarm_list[ record_index ].repetir,
        data: { updated: true }
      });

  		/*cordova.plugins.notification.local.update({
        id: id,
        text: data.nombre,
        at: date,
        every:data.repetir,
        json: { updated: true }
      });*/

		},//update

		update_all:function(data, fecha){

			var date = new Date(fecha);

			// app.alarmas.get(data.id,function(data){
      var exist = 0;

			// cordova.plugins.notification.local.update({
      //             id: id,
      //             text: data.nombre,
      //             at: date,
      //             every:data.recurrente
      //         });
      cordova.plugins.notification.local.isPresent(data.id, function(present) {
        if(present) exist = 1;
        else exist = 0;
      });

      if( exist === 1 ) {
          cordova.plugins.notification.local.update({
              id: data.id,
              text: data.nombre,
              at: date,
              every:data.recurrente
          });
      } else {
        cordova.plugins.notification.local.schedule({
          id: data.id,
          title: 'Presupuesto Mensual',
          text: data.nombre,
          at: date,
          every: data.recurrente,
        });
      }

		},//update_all

		list:function(){
			cordova.plugins.notification.local.getIds(function (ids){
			    console.log(ids);
			});
		},//list

		listDef:function(){
			cordova.plugins.notification.local.get(0, function (notification) {
    			console.log(notification);
			});
		},//listAll

	},//NOTIFICACIONES

	ingresos: {

		add:function(){

			var uid  = localStorage.getItem('userId');

      if(app.offline.check_n_get() === 1) {
			  $.ajax({
	        url: app.ingresosUrl,
	        method: 'PUT',
	        data: {
	          idUser  : uid,
	          idOrigen: $('#origen').val(),
	          monto   : $('#monto').html(),
	          descripcion : $( "textarea[name$='descripcion']" ).val(),
	          date    : $( "input[name$='date']" ).val(),
          },
	        beforeSend: function(jqXHR, settings) {
	        	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	        }
	      }).done(function(message) {
          var m = $.parseJSON(JSON.stringify(message));

          if(m.status === false) {
            $('.load').fadeOut('slow');
            $(".alerta").fadeIn("slow");
            $("#mensaje").html(m.error);
          } else if(m.status === true) {
            location.href = '01APP-HOME.html';
          }
        }).fail(function(message) {
          var m = JSON.stringify((message));
          // alert("We couldn't complete your request at this time, please try again later.");
        });
      } else {
        // ALMACENAR LOS DATOS
        data = {
          'idUser'  : uid,
          'idOrigen': $('#origen').val(),
          'monto'   : $('#monto').html().replace(",", ""),
          'date'    : $("input[name$='date']").val(),
        };

        var verify = 0;
        // VERIFICAR DATOS
        if(isNaN(parseInt(data.idOrigen)))
          verify = 1;
        if(isNaN(parseFloat(data.monto)))
          verify = 2;
        if(!data.date.match(/^\d{4}-\d{2}-\d{2}?/g))
          verify = 3;

       // MOSTRAR MENSAJE SI LA VERIFICACIÓN NO PASO
       $('.load').fadeOut('slow');
        switch( verify ) {
          case 0:
            app.ins_storage.add(data);
            break;
          case 1:
            $(".alerta").fadeIn("slow");
            $("#mensaje").html('<span>Favor de rellenar campo: Origen</span>');
            break;
          case 2:
            $(".alerta").fadeIn("slow");
            $("#mensaje").html('<span>Favor de rellenar campo: Monto</span>');
            break;
          case 3:
            $(".alerta").fadeIn("slow");
            $("#mensaje").html('<span>Favor de rellenar campo: Fecha</span>');
            break;
          default:
            $(".alerta").fadeIn("slow");
            $("#mensaje").html('<span>Error de validación, consulte a soporte.</span>');
            break;
        }

      }

		},//add

		edit:function(){

			var uid       = localStorage.getItem('userId');
			var idIngress = localStorage.getItem('ingresoId');

      if( app.offline.check_n_get() === 1 ) {
        $.ajax({
          url: app.ingresosUrl,
          method: 'POST',
          data: {
            idUser     : uid,
            idIngress  : idIngress,
            monto      : $('#monto').html(),
	          },
          beforeSend: function(jqXHR, settings) {
          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
          }
        }).done(function(message) {
         var m = $.parseJSON(JSON.stringify(message));

         if(m.status === false) {
         	$('.load').fadeOut('slow');
            $(".alerta").fadeIn("slow");
            $("#mensaje").html('<span>'+m.error+'</span>');
            $("#imgAlerta").attr('src','images/icon35.png');
            $("#close").attr('onclick','$(".alerta").fadeOut("slow");');
         } else if(m.status === true) {
         	location.href = '01APP-LIST-INGRESOS.html';
         }
       }).fail(function(message) {
         var m = JSON.stringify((message));
         $('.load').fadeOut('slow');
        // alert("We couldn't complete your request at this time, please try again later.");
       });
     } else {

       var ingress_list = JSON.parse(localStorage.getItem("ingress_list"));
       var monto_to_go = $('#monto').html().replace(",", "");
       var ingress_edit_list = JSON.parse(localStorage.getItem("ingress_edit_list"));

       if(isNaN(parseInt(monto_to_go))) {
         $('.load').fadeOut('fast');
         $(".alerta").fadeIn("fast");
         $("#mensaje").html('<span>Favor de rellenar campo: Monto</span>');
         $(".alerta").delay(1500).fadeOut("slow");
       } else {

         var record_index = 0;
         for(i = 0; i < ingress_list.length; i++) {
           if( ingress_list[i].id == idIngress ) {
             record_index = i;
             record = ingress_list[i];
             break;
           }
         }

         ingress_list[ record_index ].monto = monto_to_go;

         // GENERAR LISTA DE CAMBIOS
         if( !ingress_edit_list ) {
           ingress_edit_list = [{
             'id': ingress_list[ record_index ].id,
             'monto': monto_to_go,
           }];
           localStorage.setItem("ingress_edit_list", JSON.stringify(ingress_edit_list));
         } else {
           ingress_edit_list.push({'id': ingress_list[ record_index ].id, 'monto': monto_to_go});
           localStorage["ingress_edit_list"] = JSON.stringify(ingress_edit_list);
         }

         // GUARDAR LISTA DE INGRESOS
         localStorage.removeItem("ingress_list");
         localStorage.setItem("ingress_list", JSON.stringify(ingress_list));

         // REDIRECT
         location.href = "01APP-LIST-INGRESOS.html";

       } // ELSE (IF MONTO IS EMPTY)

     }

		},//edit

    list: function() {
      if(app.offline.check_n_get() === 1)
       app.ingresos.list_online();
      else
        app.ingresos.list_offline();
    },//list

		list_online: function(event) {

		    var uid = localStorage.getItem('userId');

		    $.ajax({
	          url: app.ingresosUrl,
	          data: {
	            id: uid
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	            console.log('no hay datos');
	            $('.loading').remove();
	         } else if(m.status === true) {
             var ingresos = "";
             $.each(m.data, function(i, val) {
 	         		  ingresos  += '<div class="swipe rt">';
 	         		    ingresos += 	'<div class="swipe-wrap">';
 	         		    ingresos += 		'<div>';
 	         		    ingresos += 			'<div class="reder" onclick=" localStorage.setItem(\'ingresoId\','+val.idIngress+'); location.href=\'01APP-EDIT-INGRESOS.html\';">';
 	         		    ingresos += 				'<div><p class="mot">'+val.fecha+' : $ '+parseFloat(val.monto).formatMoney(2,'.',',')+'</p>';
 	         		    ingresos +=					  '<p>'+val.nombre+'</p>';
 	         		    ingresos +=					  '</div>';
 	         		    ingresos += 			'</div>';
 	         		    ingresos += 		'</div>';
 	         		    ingresos +=		'<div class="desac">';
 						ingresos +=			'<button class="eliminar d-casa" onclick="$(\'#confirm_delete_ingress\').attr(\'onclick\',\'app.ingresos.delete('+val.idIngress+')\'); $(\'.alerta\').fadeIn(\'slow\');"><a href="#" style="color:#fff;"> Eliminar</a></button>';
 						ingresos +=		'</div>';
 	         		    ingresos += 	'</div>';
 	         		    ingresos += '</div>';
 	         	});

				$('#list_ingresos').html(ingresos);
				$('#list_ingresos').fadeIn(2000);
        $('.swipe').Swipe();
	         	$('.loading').remove();
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//list_online

    list_offline: function() {

      var ins_list = JSON.parse(localStorage.getItem("ingress_list"));
      var ingresos = "";

      if( !ins_list )
        return false;

      $.each(ins_list, function(i, val) {
        ingresos  += '<div class="swipe rt">';
        ingresos += 	'<div class="swipe-wrap">';
        ingresos += 		'<div>';
        ingresos += 			'<div class="reder" onclick=" localStorage.setItem(\'ingresoId\','+val.id+'); location.href=\'01APP-EDIT-INGRESOS.html\';">';
        ingresos += 				'<div><p class="mot">'+val.fecha+' : $ '+parseFloat(val.monto).formatMoney(2,'.',',')+'</p>';
        ingresos +=					  '<p>'+val.nombre+'</p>';
        ingresos +=					  '</div>';
        ingresos += 			'</div>';
        ingresos += 		'</div>';
        ingresos +=		'<div class="desac">';
        ingresos +=			'<button class="eliminar d-casa" onclick="$(\'#confirm_delete_ingress\').attr(\'onclick\',\'app.ingresos.delete('+val.id+')\'); $(\'.alerta\').fadeIn(\'slow\');"><a href="#" style="color:#fff;"> Eliminar</a></button>';
        ingresos +=		'</div>';
        ingresos += 	'</div>';
        ingresos += '</div>';
      });

      console.log(ingresos);

      $('#list_ingresos').html(ingresos);
      $('#list_ingresos').fadeIn(2000);
      $('.loading').remove();

    }, //list_offline

		get: function(event) {

		    var uid = localStorage.getItem('userId');
		    var idIngress = localStorage.getItem('ingresoId');

		    $.ajax({
	          url: app.ingresosUrl,
	          data: {
	            id: uid,
	            idIngress: idIngress
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	            console.log('no hay datos');
	            $('.loading').remove();
	         } else if(m.status === true) {
	         	    //$('#monto').html(parseFloat(m.data[0].monto).formatMoney(2,'.',','));
	         	    //$('#monto_').val(parseFloat(m.data[0].monto).formatMoney(2,'.',','));

	         	$('.loading').remove();
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });


		},//get

		total_mes:function(){

			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.totalingresosUrl,
	          method: 'GET',
	          data: {
	            idUser  : uid,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           //	alert(m.error);
	         } else if(m.status === true) {

	         	if(m.data[0].monto !== null){

	         		if(m.data[0].monto.length > 9)
	         			$("#total_ingresos_mes").css('font-size', '0.9em');

				}

	         	$("#total_ingresos_mes").html(parseFloat(m.data[0].monto).formatMoney(2,'.',','));
            localStorage.setItem('total_ingresos_mes', parseFloat(m.data[0].monto).formatMoney(2,'.',''));

	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//total_mes

    total_mes_offline: function() {
      var uid  = localStorage.getItem('userId');

      var total_ingresos_mes = localStorage.getItem('total_ingresos_mes');

      if( !total_ingresos_mes )
        $("#total_ingresos_mes").html("0.00");
      else
        $("#total_ingresos_mes").html((parseFloat(total_ingresos_mes) + app.ins_storage.sum_all()).formatMoney(2, '.', ','));

    }, // total_mes_offline

		grafica:function(){

			var uid  = localStorage.getItem('userId');
			var anio  = localStorage.getItem('gr-anio');

			$.ajax({
	          url: app.graficaingresosUrl,
	          method: 'GET',
	          data: {
	            idUser  : uid,
	            anio    : anio,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {

	           	console.log(m.error);
	         } else if(m.status === true) {

	         	if(m.data.length > 1){

	         		localStorage.setItem('graficaIngresos'+anio,JSON.stringify(m));

		         	$('#container').highcharts({
				         title: null,
				         navigation: {
				            buttonOptions: {
				                enabled: false
				            }
				        },
				        credits: {
				                     enabled: false
				        },
				        xAxis: {
				            categories: ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]
				        },
				         yAxis: {
				            min:0,
				            title:null,
				            labels: 
				                    { 
				                      formatter: function() 
				                      {
				                         return this.value.formatMoney(0,'.',',');
				                      }
				                    }
				        },
				        colors: ['#b9b9b9', '#000000', '#67bc45'],
				        tooltip: {
				                  valuePrefix: '$',
				                  crosshairs: true,
				                  shared: true
				              },
				        plotOptions: {
				            spline: {
				                marker: {
				                    radius: 4,
				                    lineColor: '#666666',
				                    lineWidth: 1
				                }
				            }
				        },
				        series: m.data
				    });
	         	} else {
	         		$('#container').html('<div>No hay datos guardados</div>');
	         	}

	         	$('.loading').remove();
	         }

	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//grafica

		grafica_offline:function(){

			var uid    = localStorage.getItem('userId');
			var grAnio = localStorage.getItem('gr-anio');

			var graficaIngresos = localStorage.getItem('graficaIngresos'+grAnio);
			    graficaIngresos = $.parseJSON(graficaIngresos);

			if(graficaIngresos){

				$('#container').highcharts({
				         title: null,
				         navigation: {
				            buttonOptions: {
				                enabled: false
				            }
				        },
				        credits: {
				                     enabled: false
				        },
				        xAxis: {
				            categories: ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]
				        },
				         yAxis: {
				            min:0,
				            title:null,
				            labels: 
				                    { 
				                      formatter: function() 
				                      {
				                         return this.value.formatMoney(0,'.',',');
				                      }
				                    }
				        },
				        colors: ['#b9b9b9', '#000000', '#67bc45'],
				        tooltip: {
				                  valuePrefix: '$',
				                  crosshairs: true,
				                  shared: true
				              },
				        plotOptions: {
				            spline: {
				                marker: {
				                    radius: 4,
				                    lineColor: '#666666',
				                    lineWidth: 1
				                }
				            }
				        },
				        series: graficaIngresos.data
				    });

			} else {
				$('#container').html('<div>No hay datos guardados</div>');
			}

			$('.loading').remove();

		},//grafica_offline

		grafica_dia_check:function(){

			if(app.offline.check_n_get() === 1)
				app.ingresos.grafica_dia();
		    else
		    	app.ingresos.grafica_dia_offline();
		        
		    
		},//grafica_check

		grafica_dia:function(){

			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.graficadaysingresosUrl,
	          method: 'GET',
	          data: {
	            idUser  : uid
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));


	         if(m.status === false) {

	           	console.log(m.error);
	         } else if(m.status === true) {

	         	localStorage.setItem('graficaIngresosDay',JSON.stringify(m));

	         	$('#container').highcharts({
			        title: null,
			         navigation: {
			            buttonOptions: {
			                enabled: false
			            }
			        },
			        credits: {
			                     enabled: false
			        },
			        xAxis: {
			            categories: m.values
			        },
			        yAxis: {
			            min:0,
			            title:null,
			            labels: 
			                    { 
			                      formatter: function() 
			                      {
			                         return this.value.formatMoney(0,'.',',');
			                      }
			                    }
			        },
			        colors: ['#b9b9b9', '#000000', '#67bc45', '#49ae34'],
			        tooltip: {
			                    valuePrefix: '$',
			                    crosshairs: true,
			                    shared: true
			                },
			        plotOptions: {
			                    spline: {
			                        marker: {
			                            radius: 4,
			                            lineColor: '#666666',
			                            lineWidth: 1
			                        }
			                    }
			                },
			        series: m.data

			    });

	         	$('.loading').remove();
	         	
	         }

	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//grafica_dia

		grafica_dia_offline:function(){

			var uid  = localStorage.getItem('userId');

			var graficaIngresosDay = localStorage.getItem('graficaIngresosDay');
			    graficaIngresosDay = $.parseJSON(graficaIngresosDay);

			if(graficaIngresosDay){

	         	$('#container').highcharts({
			        title: null,
			         navigation: {
			            buttonOptions: {
			                enabled: false
			            }
			        },
			        credits: {
			                     enabled: false
			        },
			        xAxis: {
			            categories: graficaIngresosDay.values
			        },
			        yAxis: {
			            min:0,
			            title:null,
			            labels: 
			                    { 
			                      formatter: function() 
			                      {
			                         return this.value.formatMoney(0,'.',',');
			                      }
			                    }
			        },
			        colors: ['#b9b9b9', '#000000', '#67bc45', '#49ae34'],
			        tooltip: {
			                    valuePrefix: '$',
			                    crosshairs: true,
			                    shared: true
			                },
			        plotOptions: {
			                    spline: {
			                        marker: {
			                            radius: 4,
			                            lineColor: '#666666',
			                            lineWidth: 1
			                        }
			                    }
			                },
			        series: graficaIngresosDay.data

			    });

	        } else {
	         	$('#container').html('<div>No hay datos guardados</div>');
	        }

	        $('.loading').remove(); 

		},//grafica_dia_offline

		grafica_check:function(){

			if(app.offline.check_n_get() === 1)
				app.ingresos.grafica_anio_mes();
		    else
		    	app.ingresos.grafica_anio_mes_offline();
		        
		    
		},//grafica_check

		grafica_anual_check:function(){

			if(app.offline.check_n_get() === 1)
				app.ingresos.grafica();
		    else
		    	app.ingresos.grafica_offline();

		},//grafica_check

		grafica_anio_mes:function(){

			var uid    = localStorage.getItem('userId');
			var grAnio = localStorage.getItem('gr-anio');
			var grMes  = localStorage.getItem('gr-mes');

			$.ajax({
	          url: app.graficadaysingresosUrl,
	          method: 'GET',
	          data: {
	            idUser  : uid,
	            grAnio  : grAnio,
	            grMes   : grMes
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {

	           	console.log(m.error);
	         } else if(m.status === true) {

	         	if(m.data.length > 1){

	         		localStorage.setItem('graficaIngresos'+grAnio+grMes,JSON.stringify(m));

		         	$('#container').highcharts({
		                title: null,
		                 navigation: {
		                    buttonOptions: {
		                        enabled: false
		                    }
		                },
		                credits: {
		                             enabled: false
		                },
		                xAxis: {
		                    categories: m.values
		                },
		                yAxis: {
		                    min:0,
		                    title:null,
		                    labels: 
		                            { 
		                              formatter: function() 
		                              {
		                                 return this.value.formatMoney(0,'.',',');
		                              }
		                            }
		                },
		                colors: ['#b9b9b9', '#000000', '#67bc45', '#49ae34'],
		                tooltip: {
		                    valuePrefix: '$',
		                    crosshairs: true,
		                    shared: true
		                },
		                plotOptions: {
		                    spline: {
		                        marker: {
		                            radius: 4,
		                            lineColor: '#666666',
		                            lineWidth: 1
		                        }
		                    }
		                },
		                series: m.data
	            	});
	         	} else {
	         		$('#container').html('<div>No hay datos guardados</div>');
	         	}

	         	$('.loading').remove();
	         	
	         }

	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//grafica_anio_mes

		grafica_anio_mes_offline:function(){

			var uid    = localStorage.getItem('userId');
			var grAnio = localStorage.getItem('gr-anio');
			var grMes  = localStorage.getItem('gr-mes');

			var graficaIngresos = localStorage.getItem('graficaIngresos'+grAnio+grMes);
			    graficaIngresos = $.parseJSON(graficaIngresos);

			if(graficaIngresos){

				$('#container').highcharts({
		                title: null,
		                 navigation: {
		                    buttonOptions: {
		                        enabled: false
		                    }
		                },
		                credits: {
		                             enabled: false
		                },
		                xAxis: {
		                    categories: graficaIngresos.values
		                },
		                yAxis: {
		                    min:0,
		                    title:null,
		                    labels: 
		                            { 
		                              formatter: function() 
		                              {
		                                 return this.value.formatMoney(0,'.',',');
		                              }
		                            }
		                },
		                colors: ['#b9b9b9', '#000000', '#67bc45', '#49ae34'],
		                tooltip: {
		                    valuePrefix: '$',
		                    crosshairs: true,
		                    shared: true
		                },
		                plotOptions: {
		                    spline: {
		                        marker: {
		                            radius: 4,
		                            lineColor: '#666666',
		                            lineWidth: 1
		                        }
		                    }
		                },
		                series: graficaIngresos.data
	            	});

			} else {
				$('#container').html('<div>No hay datos guardados</div>');
			}

			$('.loading').remove();

		},//grafica_anio_mes_offline

		delete:function(idIngress){

      if(app.offline.check_n_get() === 1) {
		        $.ajax({
		         url: app.ingresosUrl,
		         type:'DELETE',
		         data: {
		            idIngress:idIngress
		         },
		          beforeSend: function(jqXHR, settings) {
		          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
		          }
		       }).done(function(message){
		       		 var m = $.parseJSON(JSON.stringify(message));
		       		 if(m.status === true) {
		          		location.reload();
		      		} else {
		      			console.log(m.error);
		      		}
		       }).fail(function(){
		          console.log('error');
		       });
        } else {

          var ingress_list = JSON.parse(localStorage.getItem("ingress_list"));
          var ingress_delete_list = JSON.parse(localStorage.getItem("ingress_delete_list"));

          // BUSCAR REGISTRO EN INGRESS_LIST
          var record_index = 0;
          for(i = 0; i < ingress_list.length; i++) {
            if( ingress_list[i].id == idIngress ) {
              record_index = i;
              break;
            }
          }

          // BORRAR DEL ARRAY
          ingress_list.splice(record_index, 1);

          // GENERAR LISTA DE CAMBIOS
          if( !ingress_delete_list ) {
            ingress_delete_list = [{
              'id': idIngress,
            }];
            localStorage.setItem("ingress_delete_list", JSON.stringify(ingress_delete_list));
          } else {
            ingress_delete_list.push({'id': idIngress});
            localStorage["ingress_delete_list"] = JSON.stringify(ingress_delete_list);
          }

          // GUARDAR LISTA DE INGRESOS
          localStorage.removeItem("ingress_list");
          localStorage.setItem("ingress_list", JSON.stringify(ingress_list));

          // RELOAD
          location.reload();

        }

    	},//delete

	},//INGRESOS

	gastos:{

		add:function(){

			var uid  = localStorage.getItem('userId');

      if( app.offline.check_n_get() === 1 ) {
        $.ajax({
          url: app.gastosUrl,
          method: 'PUT',
          data: {
            idUser  : uid,
            idOrigen: $('#origen').val(),
            idCat   : $('#select_cat').val(),
            idSubCat: $('#select_subcat').val(),
            monto   : $('#monto').html(),
            date    : $( "input[name$='date']" ).val(),
            descripcion : $( "textarea[name$='descripcion']" ).val(),
          },
          beforeSend: function(jqXHR, settings) {
            jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
          }
        }).done(function(message) {
          var m = $.parseJSON(JSON.stringify(message));

          if(m.status === false) {
            $('.load').fadeOut('slow');
            $(".alerta").fadeIn("slow");
            $("#mensaje").html('<span>'+m.error+'</span>');
            $("#imgAlerta").attr('src','images/icon35.png');
            $("#close").attr('onclick','$(".alerta").fadeOut("slow");');
          } else if(m.status === true) {
            app.comparacion.comparacion();
          }
        }).fail(function(message) {
          var m = JSON.stringify((message));
          $('.load').fadeOut('slow');
          // alert("We couldn't complete your request at this time, please try again later.");
        });
       } else {
         // ALMACENAR LOS DATOS
         data = {
           idUser  : uid,
           idOrigen: $('#origen').val(),
           idCat   : $('#select_cat').val(),
           idSubCat: $('#select_subcat').val(),
           monto   : $('#monto').html().replace(",", ""),
           date    : $( "input[name$='date']" ).val(),
           descripcion : $( "textarea[name$='descripcion']" ).val(),
         };

         var verify = 0;
         // VERIFICAR DATOS
         if(isNaN(parseInt(data.idOrigen)))
           verify = 1;
         if(isNaN(parseInt(data.idSubCat)))
             verify = 3
         if(isNaN(parseInt(data.idCat)))
           verify = 2;
         if(isNaN(parseFloat(data.monto)))
           verify = 4;
         if(!data.date.match(/^\d{4}-\d{2}-\d{2}?/g))
           verify = 5;

        // MOSTRAR MENSAJE SI LA VERIFICACIÓN NO PASO
        $('.load').fadeOut('slow');
         switch( verify ) {
           case 0:
             app.outs_storage.add(data);
             break;
           case 1:
             $(".alerta").fadeIn("slow");
             $("#mensaje").html('<span>Favor de rellenar campo: Origen</span>');
             break;
           case 2:
             $(".alerta").fadeIn("slow");
             $("#mensaje").html('<span>Favor de rellenar campo: Categorías</span>');
             break;
           case 3:
             $(".alerta").fadeIn("slow");
             $("#mensaje").html('<span>Favor de rellenar campo: Subcategorías</span>');
             break;
           case 4:
             $(".alerta").fadeIn("slow");
             $("#mensaje").html('<span>Favor de rellenar campo: Monto</span>');
             break;
           case 5:
             $(".alerta").fadeIn("slow");
             $("#mensaje").html('<span>Favor de rellenar campo: Fecha</span>');
             break;
           default:
             $(".alerta").fadeIn("slow");
             $("#mensaje").html('<span>Error de validación, consulte a soporte.</span>');
             break;
         }

       }

		},//add

		edit:function(){

			var uid  = localStorage.getItem('userId');
			var idExpenses  = localStorage.getItem('gastoId');

      if( app.offline.check_n_get() === 1 ) {
			   $.ajax({
	          url: app.gastosUrl,
	          method: 'POST',
	          data: {
	            idUser     : uid,
	            idExpenses : idExpenses,
	            monto      : $('#monto').html(),
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	         	$('.load').fadeOut('slow');
	            $(".alerta").fadeIn("slow");
	            $("#mensaje").html('<span>'+m.error+'</span>');
	            $("#imgAlerta").attr('src','images/icon35.png');
	            $("#close").attr('onclick','$(".alerta").fadeOut("slow");');
	         } else if(m.status === true) {
	         	location.href = '01APP-LIST-GASTOS.html';
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	         $('.load').fadeOut('slow');
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });
       } else {

         var expenses_list = JSON.parse(localStorage.getItem("expenses_list"));
         var monto_to_go = $('#monto').html();
         var expenses_edit_list = JSON.parse(localStorage.getItem("expenses_edit_list"));

         if(isNaN(parseInt(monto_to_go))) {
           $('.load').fadeOut('fast');
           $(".alerta").fadeIn("fast");
           $("#mensaje").html('<span>Favor de rellenar campo: Monto</span>');
           $(".alerta").delay(1500).fadeOut("slow");
         } else {

           var record_index = 0;
           for(i = 0; i < expenses_list.length; i++) {
             if( expenses_list[i].id == idExpenses ) {
               record_index = i;
               record = expenses_list[i];
               break;
             }
           }

           expenses_list[ record_index ].monto = monto_to_go;

           // GENERAR LISTA DE CAMBIOS
           if( !expenses_edit_list ) {
             expenses_edit_list = [{
               'id': expenses_list[ record_index ].id,
               'monto': monto_to_go.replace(",", ""),
             }];
             localStorage.setItem("expenses_edit_list", JSON.stringify(expenses_edit_list));
           } else {
             expenses_edit_list.push({
               'id': expenses_list[ record_index ].id,
               'monto': monto_to_go.replace(",", ""),
             });
             localStorage["expenses_edit_list"] = JSON.stringify(expenses_edit_list);
           }

           // GUARDAR LISTA DE INGRESOS
           localStorage.removeItem("expenses_list");
           localStorage.setItem("expenses_list", JSON.stringify(expenses_list));

           // REDIRECT
           location.href = "01APP-LIST-GASTOS.html";
         } // EDIT (IF MONTO IS EMPTY)

       }

		},//edit

    list: function() {
      if(app.offline.check_n_get() === 1)
       app.gastos.list_online();
      else
        app.gastos.list_offline();
    },//list

		list_online: function(event) {

		    var uid = localStorage.getItem('userId');

		    $.ajax({
	          url: app.gastosUrl,
	          data: {
	            id: uid
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	            console.log('no hay datos');
	            $('.loading').remove();
	         } else if(m.status === true) {
	         	    var gastos = '';
	         	$.each(m.data, function(i, val) {
	         		    gastos  += '<div class="swipe rt">';
	         		    gastos += 	'<div class="swipe-wrap">';
	         		    gastos += 		'<div>';
	         		    gastos += 			'<div class="reder" onclick=" localStorage.setItem(\'gastoId\','+val.idExpenses+'); location.href=\'01APP-EDIT-GASTOS.html\';">';
	         		    gastos += 				'<div><p class="mot">'+val.fecha+' : $ '+parseFloat(val.monto).formatMoney(2,'.',',')+'</p>';
	         		    gastos +=					  '<p>'+val.origen+' - '+val.categoria+' - '+val.subcategoria+'</p>';
	         		    gastos +=					  '</div>';
	         		    gastos += 			'</div>';
	         		    gastos += 		'</div>';
	         		    gastos +=		'<div class="desac">';
						gastos +=			'<button class="eliminar d-casa" onclick="$(\'#confirm_delete_expenses\').attr(\'onclick\',\'app.gastos.delete('+val.idExpenses+')\'); $(\'.alerta\').fadeIn(\'slow\');"><a href="#" style="color:#fff;"> Eliminar</a></button>';
						gastos +=		'</div>';



	         		    gastos += 	'</div>';
	         		    gastos += '</div>';
	         	});

				$('#list_gastos').html(gastos);
				$('#list_gastos').fadeIn(2000);
	         	$('.swipe').Swipe();
	         	$('.loading').remove();
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });


		},//list_online

    list_offline: function() {

      var outs_list = JSON.parse(localStorage.getItem("expenses_list"));
      var gastos = "";

      if( !outs_list )
        return false;

      $.each(outs_list, function(i, val) {
        gastos  += '<div class="swipe rt">';
        gastos += 	'<div class="swipe-wrap">';
        gastos += 		'<div>';
        gastos += 			'<div class="reder" onclick=" localStorage.setItem(\'gastoId\','+val.id+'); location.href=\'01APP-EDIT-GASTOS.html\';">';
        gastos += 				'<div><p class="mot">'+val.fecha+' : $ '+parseFloat(val.monto).formatMoney(2,'.',',')+'</p>';
        gastos +=					  '<p>'+val.origen+' - '+val.categoria+' - '+val.subcategoria+'</p>';
        gastos +=					  '</div>';
        gastos += 			'</div>';
        gastos += 		'</div>';
        gastos +=	  	'<div class="desac">';
        gastos +=	  		'<button class="eliminar d-casa" onclick="$(\'#confirm_delete_expenses\').attr(\'onclick\',\'app.gastos.delete('+val.id+')\'); $(\'.alerta\').fadeIn(\'slow\');"><a href="#" style="color:#fff;"> Eliminar</a></button>';
        gastos +=	  	'</div>';
        gastos += 	'</div>';
        gastos += '</div>';
      });

      $('#list_gastos').html(gastos);
      $('#list_gastos').fadeIn(2000);
      $('.loading').remove();

    }, //list_offline

		get: function(event) {

		    var uid = localStorage.getItem('userId');
		    var idExpenses = localStorage.getItem('gastoId');

		    $.ajax({
	          url: app.gastosUrl,
	          data: {
	            id: uid,
	            idExpenses: idExpenses
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	            console.log('no hay datos');
	            $('.loading').remove();
	         } else if(m.status === true) {
	         	    //$('#monto').html(parseFloat(m.data[0].monto).formatMoney(2,'.',','));
	         	    //$('#monto_').val(parseFloat(m.data[0].monto).formatMoney(2,'.',','));

	         	$('.loading').remove();
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });


		},//get

		total_mes:function(){

			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.totalgastosUrl,
	          method: 'GET',
	          data: {
	            idUser  : uid,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           	//alert(m.error);
	         } else if(m.status === true) {

	         	if(m.data[0].monto !== null){

	         		if(m.data[0].monto.length > 9)
	         			$("#total_gastos_mes").css('font-size', '0.9em');
	         	}

	         	$('.load').fadeOut('slow')

	         	$("#total_gastos_mes").html(parseFloat(m.data[0].monto).formatMoney(2,'.',','));
            localStorage.setItem("total_gastos_mes", parseFloat(m.data[0].monto).formatMoney(2,'.',''));


	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//total_mes

    total_mes_offline: function() {

      var total_gastos_mes = localStorage.getItem('total_gastos_mes');

      if( !total_gastos_mes )
        $("#total_gastos_mes").html("0.00");
      else
        $("#total_gastos_mes").html((parseFloat(total_gastos_mes) + app.outs_storage.sum_all()).formatMoney(2, '.', ','));

    }, // total_mes_offline

		grafica:function(){

			var uid    = localStorage.getItem('userId');
			var grAnio = localStorage.getItem('gr-anio');

			$.ajax({
	          url: app.graficagastosUrl,
	          method: 'GET',
	          data: {
	            idUser : uid,
	            anio   : grAnio,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           	alert(m.error);
	         } else if(m.status === true) {

	         	if(m.data.length > 1){

	         		localStorage.setItem('graficaGastos'+grAnio,JSON.stringify(m));

		         	$('#container').highcharts({
				        title: null,
				        navigation: {
				            buttonOptions: {
				                enabled: false
				            }
				        },
				        credits: {
				                     enabled: false
				        },
				        xAxis: {
				            categories: ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]
				        },
				         yAxis: {
				            min:0,
				            title:null,
				            labels: 
				                    { 
				                      formatter: function() 
				                      {
				                         return this.value.formatMoney(0,'.',',');
				                      }
				                    }
				        },
				        colors: ['#b9b9b9', '#000000', '#67bc45'],
				        tooltip: {
				                  valuePrefix: '$',
				                  crosshairs: true,
				                  shared: true
				              },
				        /*legend:{
				              enabled:false
				        },*/
				        plotOptions: {
				            spline: {
				                marker: {
				                    radius: 4,
				                    lineColor: '#666666',
				                    lineWidth: 1
				                }
				            }
				        },
				        series: m.data
				    });
	         	}
	         	else {
	         		$('#container').html('<div>No hay datos guardados</div>');
	         	}
	         	$('.loading').remove();
	         	
			 }

	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//grafica

		grafica_offline:function(callback){

			var uid    = localStorage.getItem('userId');
			var grAnio = localStorage.getItem('gr-anio');

			var graficaGastos = localStorage.getItem('graficaGastos'+grAnio);
			    graficaGastos = $.parseJSON(graficaGastos);

			    if(graficaGastos){
			    	$('#container').highcharts({
				        title: null,
				        navigation: {
				            buttonOptions: {
				                enabled: false
				            }
				        },
				        credits: {
				                     enabled: false
				        },
				        xAxis: {
				            categories: ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]
				        },
				         yAxis: {
				            min:0,
				            title:null,
				            labels: 
				                    { 
				                      formatter: function() 
				                      {
				                         return this.value.formatMoney(0,'.',',');
				                      }
				                    }
				        },
				        colors: ['#b9b9b9', '#000000', '#67bc45'],
				        tooltip: {
				                  valuePrefix: '$',
				                  crosshairs: true,
				                  shared: true
				              },
				        /*legend:{
				              enabled:false
				        },*/
				        plotOptions: {
				            spline: {
				                marker: {
				                    radius: 4,
				                    lineColor: '#666666',
				                    lineWidth: 1
				                }
				            }
				        },
				        series: graficaGastos.data
				    });

			    }
			    else{
			    	$('#container').html('<div>No hay datos guardados</div>');
			    }

			    $('.loading').remove();

		},//grafica

		grafica_dia:function(callback){

			var me = this;
			me.callback = callback;

			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.graficadaysgastosUrl,
	          method: 'GET',
	          data: {
	            idUser  : uid,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));


	         if(m.status === false) {
	           	alert(m.error);
	         } else if(m.status === true) {
	         	$('.loading').remove();
	         	me.callback(m);
			 }

	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//grafica_dia

		grafica_dia_origen_check:function(){

			if(app.offline.check_n_get() === 1)
				app.gastos.grafica_dia_origen();
		    else
		    	app.gastos.grafica_dia_origen_offline();
		        
		    
		},//grafica_check


		grafica_dia_origen:function(){

			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.graficadaysgastosOrigenUrl,
	          method: 'GET',
	          data: {
	            idUser  : uid,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));


	         if(m.status === false) {
	           	alert(m.error);
	         } else if(m.status === true) {

	         	localStorage.setItem('graficaGastosDay',JSON.stringify(m));

	         	$('#container').highcharts({
			         title: null,
			         navigation: {
			            buttonOptions: {
			                enabled: false
			            }
			        },
			        credits: {
			                     enabled: false
			        },
			        xAxis: {
			            categories: m.values
			        },
			         yAxis: {
			                        min:0,
			                        title:null,
			                        labels: 
			                                { 
			                                  formatter: function() 
			                                  {
			                                     return this.value.formatMoney(0,'.',',');
			                                  }
			                                }
			                    },
			        colors: ['#b9b9b9', '#000000', '#67bc45'],
			        tooltip: {
			                    valuePrefix: '$',
			                    crosshairs: true,
			                    shared: true
			                },
			        plotOptions: {
			                    spline: {
			                        marker: {
			                            radius: 4,
			                            lineColor: '#666666',
			                            lineWidth: 1
			                        }
			                    }
			                },
			        series: m.data
			    });

	         	$('.loading').remove();
	         }

	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//grafica_dia

		grafica_dia_origen_offline:function(){

			var uid  = localStorage.getItem('userId');

	        var graficaGastosDay = localStorage.getItem('graficaGastosDay');
			    graficaGastosDay = $.parseJSON(graficaGastosDay);

			if(graficaGastosDay){
	         	$('#container').highcharts({
			         title: null,
			         navigation: {
			            buttonOptions: {
			                enabled: false
			            }
			        },
			        credits: {
			                     enabled: false
			        },
			        xAxis: {
			            categories: graficaGastosDay.values
			        },
			         yAxis: {
			                        min:0,
			                        title:null,
			                        labels: 
			                                { 
			                                  formatter: function() 
			                                  {
			                                     return this.value.formatMoney(0,'.',',');
			                                  }
			                                }
			                    },
			        colors: ['#b9b9b9', '#000000', '#67bc45'],
			        tooltip: {
			                    valuePrefix: '$',
			                    crosshairs: true,
			                    shared: true
			                },
			        plotOptions: {
			                    spline: {
			                        marker: {
			                            radius: 4,
			                            lineColor: '#666666',
			                            lineWidth: 1
			                        }
			                    }
			                },
			        series: graficaGastosDay.data
			    });
	         } else {
	         	$('#container').html('<div>No hay datos guardados</div>');
	         }

	         $('.loading').remove();

		},//grafica_dia

		grafica_check:function(){

			if(app.offline.check_n_get() === 1)
				app.gastos.grafica_anio_mes();
		    else
		    	app.gastos.grafica_anio_mes_offline();
		        
		    
		},//grafica_check

		grafica_anual_check:function(){

			if(app.offline.check_n_get() === 1)
				app.gastos.grafica();
		    else
		    	app.gastos.grafica_offline();
		        
		    
		},//grafica_anual_check

		grafica_anio_mes:function(){

			var uid    = localStorage.getItem('userId');
			var grAnio = localStorage.getItem('gr-anio');
			var grMes  = localStorage.getItem('gr-mes');

			$.ajax({
	          url: app.graficadaysgastosUrl,
	          method: 'GET',
	          data: {
	            idUser  : uid,
	            grAnio  : grAnio,
	            grMes   : grMes,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));
	         if(m.status === false) {
	           	alert(m.error);
	         } else if(m.status === true) {
	         	
	         	if(m.data.length > 1){
		         	localStorage.setItem('graficaGastos'+grAnio+grMes,JSON.stringify(m));

		         	$('#container').highcharts({
	                            title: null,
	                             navigation: {
	                                buttonOptions: {
	                                    enabled: false
	                                }
	                            },
	                            credits: {
	                                         enabled: false
	                            },
	                            xAxis: {
	                                categories: m.values
	                            },
	                            yAxis: {
	                                min:0,
	                                title:null,
	                                labels: 
	                                        { 
	                                          formatter: function() 
	                                          {
	                                             return this.value.formatMoney(0,'.',',');
	                                          }
	                                        }
	                            },
	                            colors: ['#b9b9b9', '#000000', '#67bc45', '#49ae34'],
	                            tooltip: {
	                                valuePrefix: '$',
	                                crosshairs: true,
	                                shared: true
	                            },
	                            /*legend:{
	                                enabled:false
	                            },*/
	                            plotOptions: {
	                                spline: {
	                                    marker: {
	                                        radius: 4,
	                                        lineColor: '#666666',
	                                        lineWidth: 1
	                                    }
	                                }
	                            },
	                            series: m.data
	                        });
	         	}else{
	         		$('#container').html('<div>No hay datos guardados</div>');
	         	}

	         	$('.loading').remove();
			 }

	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//grafica_anio_mes

		grafica_anio_mes_offline:function(){
			
			var uid    = localStorage.getItem('userId');
			var grAnio = localStorage.getItem('gr-anio');
			var grMes  = localStorage.getItem('gr-mes');

			var graficaGastos = localStorage.getItem('graficaGastos'+grAnio+grMes);
			    graficaGastos = $.parseJSON(graficaGastos);

			    if(graficaGastos){
				    $('#container').highcharts({
	                            title: null,
	                             navigation: {
	                                buttonOptions: {
	                                    enabled: false
	                                }
	                            },
	                            credits: {
	                                         enabled: false
	                            },
	                            xAxis: {
	                                categories: graficaGastos.values
	                            },
	                            yAxis: {
	                                min:0,
	                                title:null,
	                                labels: 
	                                        { 
	                                          formatter: function() 
	                                          {
	                                             return this.value.formatMoney(0,'.',',');
	                                          }
	                                        }
	                            },
	                            colors: ['#b9b9b9', '#000000', '#67bc45', '#49ae34'],
	                            tooltip: {
	                                valuePrefix: '$',
	                                crosshairs: true,
	                                shared: true
	                            },
	                            /*legend:{
	                                enabled:false
	                            },*/
	                            plotOptions: {
	                                spline: {
	                                    marker: {
	                                        radius: 4,
	                                        lineColor: '#666666',
	                                        lineWidth: 1
	                                    }
	                                }
	                            },
	                            series: graficaGastos.data
	                        });
			}
			else{
				 $('#container').html('<div>No hay datos guardados</div>');
			}

			$('.loading').remove();

		},//grafica_anio_mes_offline

		delete:function(idExpenses){

        if(app.offline.check_n_get() === 1) {
		        $.ajax({
		         url: app.gastosUrl,
		         type:'DELETE',
		         data: {
		            idExpenses:idExpenses
		         },
		          beforeSend: function(jqXHR, settings) {
		          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
		          }
		       }).done(function(message){
		       		 var m = $.parseJSON(JSON.stringify(message));
		       		 if(m.status === true) {
		          		location.reload();
		      		} else {
		      			console.log(m.error);
		      		}
		       }).fail(function(){
		          console.log('error');
		       });
         } else {

           var expenses_list = JSON.parse(localStorage.getItem("expenses_list"));
           var expenses_delete_list = JSON.parse(localStorage.getItem("expenses_delete_list"));

           // BUSCAR REGISTRO EN INGRESS_LIST
           var record_index = 0;
           for(i = 0; i < expenses_list.length; i++) {
             if( expenses_list[i].id == idExpenses ) {
               record_index = i;
               break;
             }
           }

           // BORRAR DEL ARRAY
           expenses_list.splice(record_index, 1);

           // GENERAR LISTA DE CAMBIOS
           if( !expenses_delete_list ) {
             expenses_delete_list = [{
               'id': idExpenses,
             }];
             localStorage.setItem("expenses_delete_list", JSON.stringify(expenses_delete_list));
           } else {
             ingress_delete_list.push({'id': idExpenses});
             localStorage["expenses_delete_list"] = JSON.stringify(expenses_delete_list);
           }

           // GUARDAR LISTA DE GASTOS
           localStorage.removeItem("expenses_list");
           localStorage.setItem("expenses_list", JSON.stringify(expenses_list));

           // RELOAD
           location.reload();

         }

    	},//delete

	},//GASTOS

	reportes:{
		
		table_month_check:function(){

			if(app.offline.check_n_get() === 1)
          		app.reportes.table_month();
        	else
          		app.reportes.table_month_offline();

		},//table_month_check

		table_year_check:function(){

			if(app.offline.check_n_get() === 1)
          		app.reportes.table_year();
        	else
          		app.reportes.table_year_offline();

		},//table_year_check

		table_month:function(){
			var uid    = localStorage.getItem('userId');
			var repAnio = localStorage.getItem('rep-anio');
			var repMes  = localStorage.getItem('rep-mes');

			$.ajax({
	          url: app.reportsMonthURL,
	          method: 'GET',
	          data: {
	            idUser  : uid,
	            repAnio  : repAnio,
	            repMes   : repMes,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));
	         if(m.status === false) {
	           	alert(m.error);
	         } else if(m.status === true) {
	         
	         	var table = ''; 
	         	if(m.data.length){

	         		localStorage.setItem('reportesMeses'+repAnio+repMes,JSON.stringify(m));

		         	$.each(m.data, function(i, val) {
		         		
		         		var classNota = (val.descripcion == "" || val.descripcion == null)?"":"rep-nota";
		         		var catClass  = (val.nom_cat == "-")?'':'red';

		         		table += '<tr class="'+classNota+'-tr" onclick="$(\'#'+classNota+i+'\').toggle( \'slow\' );">';
		         		table += '<td>';
		         		table += val.fecha
		         		table += '</td>';
		         		table += '<td class="'+catClass+'">$';
		         		table += parseFloat(val.monto).formatMoney(2,'.',',');
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.origen
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.nom_cat
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.nom_subcat
		         		table += '</td>';
		         		table += '<td>';
		         		table += (val.descripcion == "" || val.descripcion == null)?"-":"Ver Nota";
		         		table += '</td>';
		         		table += '</tr>';

		         		if (val.descripcion != "" && val.descripcion != null) {

		         			table += '<tr class="notaTR" id="'+classNota+i+'" >';
		         			table += '<td colspan="100%">';
		         			table += '<strong>Nota: </strong>' + val.descripcion;
		         			table += '</td>';
		         			table += '</tr>';

		         		}

		         	});
		         		table += '<tr class="totales">';
			         	table += 	'<td>';
			         	table += 		'<div class="m-gasto">';
			         	table += 			'<p>Balance</p>	';
			         	table += 		'</div>';
			         	table += 	'</td>';
		         		table += '<td>$';
		         		table += parseFloat(m.balance).formatMoney(2,'.',',');
		         		table += '</td>';
		         		table += '</tr>';

	         	} else {

		         	table += '<tr class="totales">';
			         	table += 	'<td colspan="100%">';
			         	table += 		'<div class="m-gasto">';
			         	table += 			'<p>No hay datos para mostrar</p>	';
			         	table += 		'</div>';
			         	table += 	'</td>';
		         		table += '</tr>';

		         	$('.div-ico-email').hide();

		         }

	         	$('#tabla_presupuesto').html(table);
	         	$('.loading').remove();
			 }

	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	       });
	   },//table

	   table_month_offline:function(){

			var uid     = localStorage.getItem('userId');
			var repAnio = localStorage.getItem('rep-anio');
			var repMes  = localStorage.getItem('rep-mes');
			var table   = '';

			var reportesMeses = localStorage.getItem('reportesMeses'+repAnio+repMes);
			    reportesMeses = $.parseJSON(reportesMeses);

			if(reportesMeses) {
	         	 
	         	if(reportesMeses.data.length){

		         	$.each(reportesMeses.data, function(i, val) {

		         		var classNota = (val.descripcion == "" || val.descripcion == null)?"":"rep-nota";
		         		var catClass  = (val.nom_cat == "-")?'':'red';

		         		table += '<tr class="'+classNota+'-tr" onclick="$(\'#'+classNota+i+'\').toggle( \'slow\' );">';
		         		table += '<td>';
		         		table += val.fecha
		         		table += '</td>';
		         		table += '<td class="'+catClass+'">$';
		         		table += parseFloat(val.monto).formatMoney(2,'.',',');
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.origen
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.nom_cat
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.nom_subcat
		         		table += '</td>';
		         		table += '<td>';
		         		table += (val.descripcion == "" || val.descripcion == null)?"-":"Ver Nota";
		         		table += '</td>';
		         		table += '</tr>';

		         		if (val.descripcion != "" && val.descripcion != null) {

		         			table += '<tr class="notaTR" id="'+classNota+i+'" >';
		         			table += '<td colspan="100%">';
		         			table += '<strong>Nota: </strong>' + val.descripcion;
		         			table += '</td>';
		         			table += '</tr>';

		         		}

		         	});
		         		table += '<tr class="totales">';
			         	table += 	'<td>';
			         	table += 		'<div class="m-gasto">';
			         	table += 			'<p>Balance</p>	';
			         	table += 		'</div>';
			         	table += 	'</td>';
		         		table += '<td>$';
		         		table += parseFloat(reportesMeses.balance).formatMoney(2,'.',',');
		         		table += '</td>';
		         		table += '</tr>';

	         	} else {

		         	table += '<tr class="totales">';
		         	table += 	'<td colspan="100%">';
		         	table += 		'<div class="m-gasto">';
		         	table += 			'<p>No hay datos para mostrar</p>	';
		         	table += 		'</div>';
		         	table += 	'</td>';
	         		table += '</tr>';

		         }

	         	
	         	
			 } else {
			 	    table += '<tr class="totales">';
		         	table += 	'<td colspan="100%">';
		         	table += 		'<div class="m-gasto">';
		         	table += 			'<p>No hay datos para mostrar</p>	';
		         	table += 		'</div>';
		         	table += 	'</td>';
	         		table += '</tr>';
			 }

			 $('#tabla_presupuesto').html(table);
			 $('.div-ico-email').hide();
			 $('.loading').remove();

	   },//table

	   table_year:function(){
			var uid    = localStorage.getItem('userId');
			var repAnio = localStorage.getItem('rep-anio');

			$.ajax({
	          url: app.reportsMonthURL,
	          method: 'GET',
	          data: {
	            idUser  : uid,
	            repAnio  : repAnio
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));
	         if(m.status === false) {
	           	alert(m.error);
	         } else if(m.status === true) {
	         
	         	var table = ''; 

	         	if(m.data.length){

	         		localStorage.setItem('reportesAnual'+repAnio,JSON.stringify(m));

		         	$.each(m.data, function(i, val) {

		         		var classNota = (val.descripcion == "" || val.descripcion == null)?"":"rep-nota";
		         		var catClass  = (val.nom_cat == "-")?'':'red';

		         		table += '<tr class="'+classNota+'-tr" onclick="$(\'#'+classNota+i+'\').toggle( \'slow\' );">';
		         		table += '<td>';
		         		table += val.fecha
		         		table += '</td>';
		         		table += '<td class="'+catClass+'">$';
		         		table += parseFloat(val.monto).formatMoney(2,'.',',');
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.origen
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.nom_cat
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.nom_subcat
		         		table += '</td>';
		         		table += '<td>';
		         		table += (val.descripcion == "" || val.descripcion == null)?"-":"Ver Nota";
		         		table += '</td>';
		         		table += '</tr>';

		         		if (val.descripcion != "" && val.descripcion != null) {

		         			table += '<tr class="notaTR" id="'+classNota+i+'" >';
		         			table += '<td colspan="100%">';
		         			table += '<strong>Nota: </strong>' + val.descripcion;
		         			table += '</td>';
		         			table += '</tr>';

		         		}

		       
		         	});
		         		table += '<tr class="totales">';
			         	table += 	'<td>';
			         	table += 		'<div class="m-gasto">';
			         	table += 			'<p>Balance</p>	';
			         	table += 		'</div>';
			         	table += 	'</td>';
		         		table += '<td>$';
		         		table += parseFloat(m.balance).formatMoney(2,'.',',');
		         		table += '</td>';
		         		table += '</tr>';

		         } else {

		         	table += '<tr class="totales">';
			         	table += 	'<td colspan="100%">';
			         	table += 		'<div class="m-gasto">';
			         	table += 			'<p>No hay datos para mostrar</p>	';
			         	table += 		'</div>';
			         	table += 	'</td>';
		         		table += '</tr>';

		         	$('.div-ico-email').hide();	
		         }

	         	$('#tabla_presupuesto').html(table);
	         	$('.loading').remove();
			 }

	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	       });
	   },//table_year

	   table_year_offline:function(){

			var uid     = localStorage.getItem('userId');
			var repAnio = localStorage.getItem('rep-anio');
			var table   = '';
 
			var reportesAnual = localStorage.getItem('reportesAnual'+repAnio);
			    reportesAnual = $.parseJSON(reportesAnual);

			if(reportesAnual){

				if(reportesAnual.data.length){

		         	$.each(reportesAnual.data, function(i, val) {

		         		var classNota = (val.descripcion == "" || val.descripcion == null)?"":"rep-nota";
		         		var catClass  = (val.nom_cat == "-")?'':'red';
		         		
		         		table += '<tr class="'+classNota+'-tr" onclick="$(\'#'+classNota+i+'\').toggle( \'slow\' );">';
		         		table += '<td>';
		         		table += val.fecha
		         		table += '</td>';
		         		table += '<td class="'+catClass+'">$';
		         		table += parseFloat(val.monto).formatMoney(2,'.',',');
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.origen
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.nom_cat
		         		table += '</td>';
		         		table += '<td>';
		         		table += val.nom_subcat
		         		table += '</td>';
		         		table += '<td>';
		         		table += (val.descripcion == "" || val.descripcion == null)?"-":"Ver Nota";
		         		table += '</td>';
		         		table += '</tr>';

		         		if (val.descripcion != "" && val.descripcion != null) {

		         			table += '<tr class="notaTR" id="'+classNota+i+'" >';
		         			table += '<td colspan="100%">';
		         			table += '<strong>Nota: </strong>' + val.descripcion;
		         			table += '</td>';
		         			table += '</tr>';

		         		}


		         	});
		         		table += '<tr class="totales">';
			         	table += 	'<td>';
			         	table += 		'<div class="m-gasto">';
			         	table += 			'<p>Balance</p>	';
			         	table += 		'</div>';
			         	table += 	'</td>';
		         		table += '<td>$';
		         		table += parseFloat(reportesAnual.balance).formatMoney(2,'.',',');
		         		table += '</td>';
		         		table += '</tr>';

		         } else {

		         	    table += '<tr class="totales">';
			         	table += 	'<td colspan="100%">';
			         	table += 		'<div class="m-gasto">';
			         	table += 			'<p>No hay datos para mostrar</p>	';
			         	table += 		'</div>';
			         	table += 	'</td>';
		         		table += '</tr>';

		         }

			} else {

				table += '<tr class="totales">';
	         	table += 	'<td colspan="100%">';
	         	table += 		'<div class="m-gasto">';
	         	table += 			'<p>No hay datos para mostrar</p>	';
	         	table += 		'</div>';
	         	table += 	'</td>';
         		table += '</tr>';

			}

			$('.div-ico-email').hide();	
			$('#tabla_presupuesto').html(table);
	        $('.loading').remove();

	   },//table_year

	   	export_month:function(){
	   		var uid    = localStorage.getItem('userId');
			var repAnio = localStorage.getItem('rep-anio');
			var repMes  = localStorage.getItem('rep-mes');

			$.ajax({
	          url: app.reportsExcelURL,
	          method: 'GET',
	          data: {
	            idUser  : uid,
	            repAnio  : repAnio,
	            repMes   : repMes,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));
	         	if(m.status === false) {
	         		$(".instant-alert").html("<strong>"+m.error+"</strong>");
	         	} else if(m.status === true) {
	         		$(".instant-alert").html("<strong>"+m.message+"</strong>");
	         	}
	         	$('.ico-email').attr('src','images/email.png');
	         	$(".instant-alert").toggle().fadeToggle(3000);
	     	});


	   	},//export_month

	   	export_year:function(){
	   		var uid    = localStorage.getItem('userId');
			var repAnio = localStorage.getItem('rep-anio');

			$.ajax({
	          url: app.reportsExcelURL,
	          method: 'GET',
	          data: {
	            idUser  : uid,
	            repAnio  : repAnio,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));
	         	if(m.status === false) {
	         		$(".instant-alert").html("<strong>"+m.error+"</strong>");
	         	} else if(m.status === true) {
	         		$(".instant-alert").html("<strong>"+m.message+"</strong>");
	         	}
	         	$('.ico-email').attr('src','images/email.png');
	         	$(".instant-alert").toggle().fadeToggle(3000);
	     	});


	   	},//export_year

	},//REPORTES

	historial:{

		list:function(){

			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.historialURL,
	          method: 'GET',
	          data: {
	            idUser  : uid,
 	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));


	         if(m.status === false) {
	           	alert(m.error);
	         } else if(m.status === true) {

	         	var anios = "";

	         	$.each(m.data, function(i, val) {
	         	    anios += '<li onclick="localStorage.setItem(\'anio\', '+val.year+'); location.href=\'02APP-HISTORIAL-INT.html\';">';
	         		anios += 	'<div>';
	         		anios += 		'<p class="mot">'+val.year+'</p>';
	         		//anios += 		'<p>Renta, Mantenimiento....</p>';
	         		anios += 	'</div>';
	         		anios += '</li>';
	         	});

	         	$("#years").html(anios);


			 }

	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });


		},//list

	},//HISTORIAL

	comparacion:{

		comparacion:function(){

			var uid  = localStorage.getItem('userId');

			$.ajax({
	          url: app.comparacionURL,
	          data: {
	          	id      : uid
	          },
	          beforeSend: function(jqXHR, settings) {
	          	jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
	          }
	        }).done(function(message) {
	         var m = $.parseJSON(JSON.stringify(message));

	         if(m.status === false) {
	           	conosle.log(m.error);
	         } else if(m.status === true) {
	         	console.log(m.data);
	         	if(m.data === true) {
	         		location.href = '01APP-HOME.html';
	         	} else if(m.data === false){
	         		$(".load").fadeOut("slow");
					$(".alerta").fadeIn("slow");
					//$("#mensaje").html('Haz sobrepasado tu presupuesto <button id="tips" class="conf" onclick="location.href = \'01APP-HOME.html\';">Tips</button>');
					$("#mensaje").html('Haz sobrepasado tu presupuesto');
	            	$("#imgAlerta").attr('src','images/icon45.png');
	            	$("#close").attr('onclick','location.href = \'01APP-HOME.html\';');
	         	}
	         }
	       }).fail(function(message) {
	         var m = JSON.stringify((message));
	        // alert("We couldn't complete your request at this time, please try again later.");
	       });

		},//get

	},//COMPARACION

  ins_storage: {

    data: [],

    list_ins: [],

    add: function(item) {

      // RECOLECTAR DATOS PREVIOS
      if(localStorage.getItem("ins_to_go"))
        this.data = JSON.parse(localStorage.getItem("ins_to_go"));
      else
        this.data = [];

      // GENERA UN ID
      item.item_id = this.data.length + 1;

      // GUARDAR VALORES EN DATA
      this.data.push(item);
      console.log(app.ins_storage.data);

      // GUARDAR VALORES EN LOCALSTORAGE
      localStorage.removeItem("ins_to_go");
      localStorage.setItem("ins_to_go", JSON.stringify(this.data));

      // REDIRECCIONAR A HOME
      location.href = '01APP-HOME.html';

    }, // ADD

    get_list: function() {

      var uid = localStorage.getItem('userId');

      $.ajax({
          url: app.ingresosUrl,
          method: 'GET',
          data: {
            id: uid
          },
          beforeSend: function(jqXHR, settings) {
            jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
          }
        }).done(function(message) {
         var m = $.parseJSON(JSON.stringify(message));

         if(m.status === false) {
            console.log('no hay datos');
            $('.loading').remove();
         } else if(m.status === true) {
           var ingresos = [];
           $.each(m.data, function(i, val) {
             ingresos.push({
               'id': val.idIngress,
               'nombre': val.nombre,
               'monto': val.monto,
               'fecha': val.fecha,
             });
           });
           localStorage.setItem("ingress_list", JSON.stringify(ingresos));
         }
       }).fail(function(message) {
         var m = JSON.stringify((message));
        // alert("We couldn't complete your request at this time, please try again later.");
       });

    }, // GET_LIST

    fill_data: function() {
      if(localStorage.getItem("ins_to_go"))
        this.data = JSON.parse(localStorage.getItem("ins_to_go"));
      else
        this.data = [];
    }, // FILL DATA

    send_edit: function() {

      ins_edit = localStorage.getItem("ingress_edit_list")

      if( !ins_edit )
        return false;

      data_tmp = {
        'data': ins_edit
      }

      $.ajax({
        url: app.ingresosEditUrl,
        method: 'PUT',
        data: data_tmp,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
        }
      }).done(function(message) {
        var m = $.parseJSON(JSON.stringify(message));

        if(m.status === false) {
          // Nothing
        } else if(m.status === true) {
          localStorage.removeItem("ingress_edit_list");
        }
      }).fail(function(message) {
        var m = $.parseJSON(JSON.stringify(message));
      });

    }, // SEND EDIT

    send_delete: function() {

      ins_del = localStorage.getItem("ingress_delete_list");

      if( !ins_del )
        return false;

      data_tmp = {
        'data': ins_del
      }

      $.ajax({
        url: app.ingresosDelUrl,
        method: 'DELETE',
        data: data_tmp,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
        }
      }).done(function(message) {
        var m = $.parseJSON(JSON.stringify(message));

        if(m.status === false) {
          // Nothing
        } else if(m.status === true) {
          localStorage.removeItem("ingress_delete_list");
        }
      }).fail(function(message) {
        var m = $.parseJSON(JSON.stringify(message));
      });

    }, // SEND DELETE

    send_all: function() {

      if( !localStorage.getItem("ins_to_go") )
        return false;

      this.fill_data();

      data_tmp = {
        'data': JSON.stringify(app.ins_storage.data)
      }
      console.log( app.ins_storage.data );

      $.ajax({
        url: app.ingresosArrUrl,
        method: 'PUT',
        data: data_tmp,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
        }
      }).done(function(message) {
        var m = $.parseJSON(JSON.stringify(message));

        if(m.status === false) {
          $(".instant-alert").html("<strong>Fallo al actualizar los datos:</strong>");
          $(".instant-alert").toggle().toggle(3000);
        } else if(m.status === true) {
          this.data = [];
          localStorage.removeItem("ins_to_go");
          $(".instant-alert").html("<strong>Datos actualizados</strong>");
          $(".instant-alert").toggle().toggle(3000);
        }
      }).fail(function(message) {
        var m = $.parseJSON(JSON.stringify(message));
      });

    }, // SEND ALL

    sum_all: function() {
      this.fill_data();
      tmp = 0;
      for(i = 0; i < app.ins_storage.data.length; i++)
        tmp += parseFloat(app.ins_storage.data[i].monto);
      return tmp;
    }, // SUM ALL

  }, // INS_STORAGE

  outs_storage: {

    data: [],

    add: function(item) {

      // RECOLECTAR DATOS PREVIOS
      if(localStorage.getItem("outs_to_go"))
        this.data = JSON.parse(localStorage.getItem("outs_to_go"));
      else
        this.data = [];

      // GENERA UN ID
      item.item_id = this.data.length + 1;

      // GUARDAR VALORES EN DATA
      this.data.push(item);
      console.log(app.outs_storage.data);

      // GUARDAR VALORES EN LOCALSTORAGE
      localStorage.removeItem("outs_to_go");
      localStorage.setItem("outs_to_go", JSON.stringify(this.data));

      // REDIRECCIONAR A HOME
      location.href = '01APP-HOME.html';

    }, // ADD

    get_list: function(event) {

      var uid = localStorage.getItem('userId');

      $.ajax({
          url: app.gastosUrl,
          data: {
            id: uid
          },
          beforeSend: function(jqXHR, settings) {
            jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
          }
        }).done(function(message) {
         var m = $.parseJSON(JSON.stringify(message));

         if(m.status === false) {
            console.log('no hay datos');
            $('.loading').remove();
         } else if(m.status === true) {
           var gastos = [];
           $.each(m.data, function(i, val) {
             gastos.push({
               'id':           val.idExpenses,
               'origen':       val.origen,
               'nombre':       val.nombre,
               'categoria':    val.categoria,
               'subcategoria': val.subcategoria,
               'monto':        val.monto,
               'fecha':        val.fecha,
             });
             localStorage.setItem("expenses_list", JSON.stringify(gastos));
           });
         }
       }).fail(function(message) {
         var m = JSON.stringify((message));
        // alert("We couldn't complete your request at this time, please try again later.");
       });

    }, // GET_LIST

    fill_data: function() {
      if(localStorage.getItem("outs_to_go"))
        this.data = JSON.parse(localStorage.getItem("outs_to_go"));
      else
        this.data = [];
    }, // FILL DATA

    send_all: function() {

      if( !localStorage.getItem("outs_to_go") )
        return false;

      this.fill_data();

      data_tmp = {
        'data': JSON.stringify(app.outs_storage.data)
      }
      console.log( app.outs_storage.data );

      $.ajax({
        url: app.gastosArrUrl,
        method: 'PUT',
        data: data_tmp,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
        }
      }).done(function(message) {
        var m = $.parseJSON(JSON.stringify(message));

        if(m.status === false) {
          $(".instant-alert").html("<strong>Fallo al actualizar los datos</strong>");
          $(".instant-alert").toggle().toggle(3000);
        } else if(m.status === true) {
          this.data = [];
          localStorage.removeItem("outs_to_go");
          $(".instant-alert").html("<strong>Datos actualizados</strong>");
          $(".instant-alert").toggle().toggle(3000);
        }
      }).fail(function(message) {
        var m = $.parseJSON(JSON.stringify(message));
      });

    }, // SEND ALL

    send_edit: function() {

      outs_edit = localStorage.getItem("expenses_edit_list");

      if( !outs_edit )
        return false;

      data_tmp = {
        'data': outs_edit
      }

      $.ajax({
        url: app.gastosEditUrl,
        method: 'PUT',
        data: data_tmp,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
        }
      }).done(function(message) {
        var m = $.parseJSON(JSON.stringify(message));

        if(m.status === false) {
          // Nothing
        } else if(m.status === true) {
          localStorage.removeItem("expenses_edit_list");
        }
      }).fail(function(message) {
        var m = $.parseJSON(JSON.stringify(message));
      });

    }, // SEND EDIT

    send_delete: function() {

      outs_del = localStorage.getItem("expenses_delete_list");

      if( !outs_del )
        return false;

      data_tmp = {
        'data': outs_del
      }

      $.ajax({
        url: app.gastosDelUrl,
        method: 'DELETE',
        data: data_tmp,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
        }
      }).done(function(message) {
        var m = $.parseJSON(JSON.stringify(message));

        if(m.status === false) {
          // Nothing
        } else if(m.status === true) {
          localStorage.removeItem("expenses_delete_list");
        }
      }).fail(function(message) {
        var m = $.parseJSON(JSON.stringify(message));
      });

    }, // SEND DELETE

    sum_all: function() {
      this.fill_data();
      tmp = 0;
      for(i = 0; i < app.outs_storage.data.length; i++)
        tmp += parseFloat(app.outs_storage.data[i].monto);
      return tmp;
    }, // SUM ALL

  }, // OUTS_STORAGE

  alarm_storage: {
    add: function(data) {

      // DECLARAR VARIABLES
      var uid = localStorage.getItem("userId"),
          id_tmp = 0,
          min = 10000,
          max = 99999,
          exist = 0;

      // GENERAR ID

        id_tmp = uid;
        id_tmp += "" + Math.round(Math.random() * (max - min) + min);

        cordova.plugins.notification.local.isPresent(id_tmp, function (present) {
          if(present) exist = 1;
          else exist = 0;
        });

      // CHECAR CUAL ES EL SIGUIENTE ID
      if(!localStorage.getItem("ins_alarm")) {

        // GENERAR ID
        data.id = id_tmp; // 60951648

        // GUARDAR DATOS EN LOCALSTORAGE
        localStorage.setItem("ins_alarm", JSON.stringify([data]));

      } else {

        // GENERAR SIGUIENTE ID
        var data_tmp = JSON.parse(localStorage.getItem("ins_alarm"));
        data.id = id_tmp;

        // GUARDAR DATOS EN EL LOCALSTORAGE
        data_tmp.push(data);
        localStorage["ins_alarm"] = JSON.stringify(data_tmp);

      }

      return data.id_tmp;

    }, // ADD

    generateId: function(uid) {
      var exist = 0,
          min = 10000,
          max = 99999;

      do {
        var id_tmp = uid;
        id_tmp += "" + Math.round(Math.random() * (max - min) + min);

        cordova.plugins.notification.local.getIds(function(ids) {
    		if(ids.indexOf(id_tmp) >= 0) exist = 1;
    		else exist = 0;
		}, cordova.plugins);
        //marca error en iOS
        /*cordova.plugins.notification.local.isPresent(id_tmp, function (present) {
          if(present) exist = 1;
          else exist = 0;
        });*/
      } while(exist == 1);

      return id_tmp;
    }, // GENERATEID

    send_all: function() {
      console.log("Enter");
      if( !localStorage.getItem("ins_alarm") )
        return false;

      var uid = localStorage.getItem("userId");
      var data = JSON.parse(localStorage.getItem("ins_alarm"));
      var data_tmp = [];

      for( i = 0; i < data.length; i++ ) {
        data_tmp.push({
          'id':           data.id,
          'idUser':       uid,
          'nombre':       data[i].nombre,
          'descripcion':  data[i].descripcion,
          'date':         data[i].date + " " + data[i].time,
          'recurrente':   data[i].repetir,
        });
      }

      data_send = {
        'data': JSON.stringify(data_tmp)
      }

      $.ajax({
        url: app.alarmArr,
        method: 'PUT',
        data: data_send,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
        }
      }).done(function(message) {
        var m = $.parseJSON(JSON.stringify(message));

        if(m.status) {
          localStorage.removeItem("ins_alarm");
        } else {
          console.log(m.status);
        }
      }).fail(function(message) {
        var m = $.parseJSON(JSON.stringify(message));
      });
      console.log("Leave");
    }, // SEND_ALL

    send_edit: function() {
      if( !localStorage.getItem("alarm_edit_list") )
        return false;

      var uid = localStorage.getItem("userId");
      var data = JSON.parse(localStorage.getItem("alarm_edit_list"));
      var data_tmp = [];

      for( i = 0; i < data.length; i++ ) {
        data_tmp.push({
          'id':           data.id,
          'idUser':       uid,
          'nombre':       data[i].nombre,
          'descripcion':  data[i].descripcion,
          'fecha_inicio': data[i].fecha + " " + data[i].hora,
          'recurrente':   data[i].repetir,
        });
      }

      data_send = {
        'data': JSON.stringify(data_tmp)
      }

      $.ajax({
        url: app.alarmEdit,
        method: 'PUT',
        data: data_send,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
        }
      }).done(function(message) {
        var m = $.parseJSON(JSON.stringify(message));

        if(m.status) {
          console.log(m.id);
        } else {
          console.log(m.status);
        }
      }).fail(function(message) {
        var m = $.parseJSON(JSON.stringify(message));
      });
    }, // send_edit

    send_delete: function() {
      if( !localStorage.getItem("alarm_delete_list") )
        return false;

      var uid = localStorage.getItem("userId");
      var data = JSON.stringify(localStorage.getItem("alarm_delete_list"));
      var data_tmp = [];

      for( i = 0; i < data.length; i++ ) {
        data_tmp.push({
          'id': data.id
        });
      }

      data_send = {
        'data': data_tmp
      }

      $.ajax({
        url: app.alarmDel,
        method: 'DELETE',
        data: data_send,
        dataType: 'json',
        beforeSend: function(jqXHR, settings) {
          jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
        }
      }).done(function(message) {
        var m = $.parseJSON(JSON.stringify(message));

        if(m.status) {
          console.log(m.id);
        } else {
          console.log(m.status);
        }
      }).fail(function(message) {
        var m = $.parseJSON(JSON.stringify(message));
      });
    }, // SEND_DELETE

    edit: function(idAlarm, data) {

      var alarm_list = JSON.parse(localStorage.getItem("alarm_list"));
      var alarm_edit_list = JSON.parse(localStorage.getItem("alarm_edit_list"));

      var record_index = 0;
      for(i = 0; i < alarm_list.length; i++) {
        if( alarm_list[i].id == idAlarm ) {
          record_index = i;
          record = alarm_list[i];
          break;
        }
      }

      // EDITAR CAMBIOS
      alarm_list[ record_index ].nombre = data.nombre;
      alarm_list[ record_index ].date = data.date;
      alarm_list[ record_index ].time = data.time;
      alarm_list[ record_index ].descripcion = data.descripcion;
      alarm_list[ record_index ].repetir = data.repetir;
      alarm_list[ record_index ].estatus = 1;

      // GENERAR LISTA DE CAMBIOS
      if( !alarm_edit_list ) {
        alarm_edit_list = [{
          'id':           idAlarm,
          'nombre':       data.nombre,
          'fecha_inicio': data.date + " " + data.time,
          'descripcion':  data.descripcion,
          'repetir':      data.repetir,
        }];
        localStorage.setItem("alarm_edit_list", JSON.stringify(alarm_edit_list));
      } else {
        alarm_edit_list.push({
          'id':          idAlarm,
          'nombre':      data.nombre,
          'fecha_inicio': data.date + " " + data.time,
          'descripcion': data.descripcion,
          'repetir':     data.repetir,
        });
        localStorage["alarm_edit_list"] = JSON.stringify(alarm_edit_list);
      }

      // GUARDAR LISTA DE INGRESOS
      localStorage.removeItem("alarm_list");
      localStorage.setItem("alarm_list", JSON.stringify(alarm_list));

      // REDIRECT
      location.href = "01APP-RECORDATORIOS.html";

    }, // EDIT

    delete: function(idAlarm) {
      var alarm_list = JSON.parse(localStorage.getItem("alarm_list"));
      var alarm_delete_list = JSON.parse(localStorage.getItem("alarm_delete_list"));

      // BUSCAR REGISTRO EN ALARM_LIST
      var record_index = 0;
      for(i = 0; i < alarm_list.length; i++) {
        if( alarm_list[i].id == idAlarm ) {
          record_index = i;
          break;
        }
      }

      // BORRAR DEL ARRAY
      alarm_list.splice(record_index, 1);

      // GENERAR LISTA DE CAMBIOS
      if( !alarm_delete_list ) {
        alarm_delete_list = [{
          'id': idAlarm,
        }];
        localStorage.setItem("ingress_delete_list", JSON.stringify(alarm_delete_list));
      } else {
        alarm_delete_list.push({'id': idAlarm});
        localStorage["alarm_delete_list"] = JSON.stringify(alarm_delete_list);
      }

      // GUARDAR LISTA DE ALARMAS
      localStorage.removeItem("alarm_list");
      localStorage.setItem("alarm_list", JSON.stringify(alarm_list));

      // RELOAD
      location.reload();

    }, // DELETE
  }, // ALARM_STORAGE

  online: {

    // ESTA FUNCION SINCRONIZA TODOS LOS REGISTROS
    syncronize: function() {

      // BORRAR ALERTA OFFLINE
      localStorage.removeItem("cnn_alert");

      // MOSTRAR MENSAJE DE CONEXION INICIADA
      $(".instant-alert").html("<strong>Usted se ha conectado</strong>");
      $(".instant-alert").toggle().fadeToggle(3000);

      // ENVIAR EDICIONES DE INGRESOS, GASTOS Y ALARMAS
      app.ins_storage.send_edit();
      app.outs_storage.send_edit();

      // ENVIAR ELIMINADOS DE INGRESOS, GASTOS Y ALARMAS
      app.ins_storage.send_delete();
      app.outs_storage.send_delete();
      // #:app.alarm_storage.send_delete();

      // ENVIAR INGRESOS Y GASTOS DEL LOCALSTORAGE
      app.ins_storage.send_all();
      app.outs_storage.send_all();
      app.alarm_storage.send_all();

      // RECOLECTAR LISTA DE INGRESOS Y LISTA DE GASTOS
      app.ins_storage.get_list();
      app.outs_storage.get_list();

      // RECOLECTAR LISTA DE CATEGORIAS
      app.categoria.get_list();

      // RECOLECTAR LISTA DE SUBCATEGORIAS
      app.subcategoria.get_list();

      // RECOLECTAR LISTA DE ALARMAS
      app.alarmas.get_list();

      // RECIBIR TOTAL DE INGRESOS, GASTOS, PRESUPUESTO Y AHORRO
      app.ingresos.total_mes();
      app.gastos.total_mes();
      app.presupuesto.total();
      app.presupuesto.total_ahorrado();

    }, // SYNCRONIZE

    // ESTA FUNCION SE DISPARA AL INICIAR LA CONEXIÓN
    triggerAction: function() {

      // SINCRONIZAR DATOS EXISTENTES
      app.online.syncronize();

    }
  }, // ONLINE

	offline:{

		check:function(){

		 	networkState = navigator.connection.type;

		    if(networkState == Connection.NONE) {



        }

		},//check

    check_n_get: function() {
      networkState = navigator.connection.type;

      console.log(networkState);

	    if(networkState == Connection.NONE)
	   		return 0;
      else
        return 1;
    }, // CHECK 'N GET

    // ESTA FUNCIÓN SE DISPARA AL NO HABER CONEXIÓN
    triggerAction: function() {

      // CREAR ALERTA
      cnn_alert = localStorage.getItem("cnn_alert");
      if(!cnn_alert) {
        //$(".offline").fadeIn("slow");
        localStorage.setItem("cnn_alert", 1);
      } else {
        if(cnn_alert !== "1") {
          //$(".offline").fadeIn("slow");
          localStorage.setItem("cnn_alert", 1);
        }
      }

      // MUESTRA MENSAJE DE DESCONEXION
      $(".instant-alert").html("<strong>Usted se ha desconectado</strong>");
      var display = $(".instant-alert").css("display");
      //$(".instant-alert").toggle().fadeToggle(3000);
      //$(".instant-alert").toggle().toggle(3000);

      // ENTRA EN MODO DESCONECTADO

    }, // TRIGGERACTION

	}, // OFFLINE

	aniosSelect:{

		aniosSelect_check:function(){

			var uid      = localStorage.getItem('userId');
			var category = localStorage.getItem('gr-category');

			if(category == 'Ingresos')
	 			category = 'ingress';
	 		else if(category == 'Gastos')
	 			category = 'expenses';
	 		else if(category == 'Presupuesto Mensual')
	 			category = 'estimation';

    		if(app.offline.check_n_get() === 1)
				app.aniosSelect.online(uid, category);
		    else
		    	app.aniosSelect.offline(uid, category);

    	},//aniosSelect_check
    	online:function(uid, category){

    		 $.ajax({
		        url: urlAPI+category+'/years',
		        method: 'GET',
		        data: { idUser: uid },
		        dataType: 'json',
		        beforeSend: function(jqXHR, settings) {
		          jqXHR.setRequestHeader('Authorization', 'Basic ' + base64_encode(app.username+':'+app.password) );
		        }
		      }).done(function(message) {

		        var m = $.parseJSON(JSON.stringify(message));
		        var f = new Date();			
				var currentYear = f.getFullYear();
				var options     = '';

		        if(m.status) {

		           	localStorage.setItem('aniosSelect'+category,JSON.stringify(m.years));

		           	$.each(m.years, function(i, val) {
		           		select = (val.year == currentYear)?'selected':'';
						options += '<option value="'+val.year+'" '+select+'>';
						options += val.year;
						options += '</option>';
		           	});

		           	$(".select-anio").html(options);

		        } else {
		          console.log(m.status);
		        }
		      }).fail(function(message) {
		        var m = $.parseJSON(JSON.stringify(message));
		      });

    	},//aniosSelect
    	offline:function(uid,category){

    		var f = new Date();			
			var currentYear = f.getFullYear();
			var options     = '';

    		var aniosSelect = localStorage.getItem('aniosSelect'+category);
			    aniosSelect = $.parseJSON(aniosSelect);

			if(aniosSelect){
				$.each(aniosSelect, function(i, val) {
		        	select = (val.year == currentYear)?'selected':'';
					options += '<option value="'+val.year+'" '+select+'>';
					options += val.year;
					options += '</option>';
		        });

			} 
			else {
				options += '<option value="'+currentYear+'">';
				options += currentYear;
				options += '</option>';
			}

			$(".select-anio").html(options);

    	},//aniosSelect_offline

    },//aniosSelect

}//app

 document.addEventListener('deviceready', function () {

 		cordova.plugins.notification.local.on('click', function (notification) {

 			if(notification.id == 0){
 				$(".alerta_default").fadeIn("slow");
 			} else {
 			localStorage.setItem('alarmaIdInvasive',notification.id);

 				if(localStorage.getItem("userId") !== null){
                   location.href="01APP-HOME.html";
            	}
        	}



        });

 });

 /* global prototype formatMoney function
 	    * 	params:
 	    *		c (integer): count numbers of digits after sign
 	    *		d (string): decimals sign separator
 	    *		t (string): miles sign separator
 	    *
 	    *	example:
 	    *		(123456789.12345).formatMoney(2, ',', '.');
 	    *			=> "123.456.789,12" Latinoamerican moneyFormat
 	    */
 Number.prototype.formatMoney = function(c, d, t){
	var n = this,
	    c = isNaN(c = Math.abs(c)) ? 2 : c,
	    d = d == undefined ? "." : d,
	    t = t == undefined ? "," : t,
	    s = n < 0 ? "-" : "",
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

 function aniosSelect(){
 	
	var f=new Date();

	var firstYear   = 2010;
	var currentYear = f.getFullYear();
	var finalYear   = currentYear+1;
	var options     = '';

	for(i = firstYear; i <= finalYear; i++){
		select = (i == currentYear)?'selected':'';
		options += '<option value="'+i+'" '+select+'>';
		options += i;
		options += '</option>';
	}

 	$(".select-anio").html(options);

 }

 function mesesSelect(){

 	var f=new Date();

 	var meses        = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var options      = '';
	var currentMonth = f.getMonth();

	meses.forEach(function(val,i){
		select = (i == currentMonth)?'selected':'';
		options += '<option value="'+(i+1)+'" '+select+'>';
		options += val;
		options += '</option>';

	});
		
	$(".select-meses").html(options);

 }

 function base64_encode(data) {
  var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
    ac = 0,
    enc = '',
    tmp_arr = [];

  if (!data) {
    return data;
  }

  do { // pack three octets into four hexets
    o1 = data.charCodeAt(i++);
    o2 = data.charCodeAt(i++);
    o3 = data.charCodeAt(i++);

    bits = o1 << 16 | o2 << 8 | o3;

    h1 = bits >> 18 & 0x3f;
    h2 = bits >> 12 & 0x3f;
    h3 = bits >> 6 & 0x3f;
    h4 = bits & 0x3f;

    // use hexets to index into b64, and append result to encoded string
    tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
  } while (i < data.length);

  enc = tmp_arr.join('');

  var r = data.length % 3;

  return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
}
