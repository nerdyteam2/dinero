<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Reports_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function reports_get($idUser, $anio, $mes){

    	$this->db->query('
    			SELECT * FROM dnrapp_ingresos
				UNION ALL
				SELECT * FROM dnrapp_gastos
				WHERE 
					fecha = YEAR(fecha)
				AND
					fecha = MONTH(fecha)
				ORDER BY fecha DESC;');
    	
        if ($this->db->affected_rows() > 0) return $this->db->result();
		else return FALSE;

	}

}
