<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Estimation_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function put_estimation($monto, $idUser, $idCat, $idSubCat){
		
		$this->db->select('*');
        $this->db->where('categorias_id' , $idCat);
        $this->db->where('subcategorias_id' , $idSubCat);
        $this->db->where('usuarios_id' , $idUser);
        $this->db->from('dnrapp_presupuesto');

		if($this->db->count_all_results() > 0){

			$this->db->where('categorias_id' , $idCat);
       		$this->db->where('subcategorias_id' , $idSubCat);
        	$this->db->where('usuarios_id' , $idUser);
        	$this->db->set('monto' , $monto);
        	$this->db->update('dnrapp_presupuesto');
	        if ($this->db->affected_rows() > 0) return TRUE;
			else return FALSE;

		} else {

			$this->db->set('monto'  , $monto ); 
	        $this->db->set('usuarios_id' , $idUser);
	        $this->db->set('categorias_id' , $idCat);
	        $this->db->set('subcategorias_id' , $idSubCat);
	        $this->db->insert('dnrapp_presupuesto');
	        if ($this->db->affected_rows() > 0) return TRUE;
			else return FALSE;

		}
		
	}

	function get_estimation_table($idUser){
		
		$this->db->select('*');
		$this->db->select_sum('monto','total');
        $this->db->where('usuarios_id' , $idUser);
        $this->db->group_by('categorias_id');
        $sql = $this->db->get('dnrapp_presupuesto');

        return $sql->result_array();
		
	}

	function get_estimation_ahorraste($idUser){
		
		$this->db->select_sum('monto', 'total');
        $this->db->where('usuarios_id' , $idUser);
        $sql = $this->db->get('dnrapp_presupuesto');

        return $sql->result_array();
		
	}

	function get_presupuesto_vs_gastos($idUser){
		
		$this->db->select('*');
		$this->db->select_sum('monto','total');
        $this->db->where('dnrapp_presupuesto.usuarios_id' , $idUser);
        $this->db->where('dnrapp_categorias.estatus', 1);
        $this->db->where('dnrapp_subcategorias.estatus', 1);
        $this->db->join('dnrapp_categorias', 'dnrapp_categorias.id = dnrapp_presupuesto.categorias_id');
        $this->db->join('dnrapp_subcategorias', 'dnrapp_subcategorias.id = dnrapp_presupuesto.subcategorias_id');
        $this->db->group_by('dnrapp_presupuesto.categorias_id');

        $sql = $this->db->get('dnrapp_presupuesto');

        return $sql->result_array();
		
	}

	function total($idUser){
		
		$this->db->select_sum('monto','total');
        $this->db->where('dnrapp_presupuesto.usuarios_id' , $idUser);
        $this->db->where('dnrapp_categorias.estatus', 1);
        $this->db->where('dnrapp_subcategorias.estatus', 1);
        $this->db->join('dnrapp_categorias', 'dnrapp_categorias.id = dnrapp_presupuesto.categorias_id');
        $this->db->join('dnrapp_subcategorias', 'dnrapp_subcategorias.id = dnrapp_presupuesto.subcategorias_id');

        $sql = $this->db->get('dnrapp_presupuesto');

        return $sql->result_array();
		
	}

	function get_estimation($id){
		$this->db->select_sum("monto", "total");
		$this->db->where('usuarios_id', $id);
        $sql = $this->db->get("dnrapp_presupuesto");
        return $sql->result_array();
	}

	function get_gastos($id){
		$this->db->select_sum("monto", "total");
		$this->db->where('usuarios_id', $id);
        $sql = $this->db->get("dnrapp_gastos");

        return $sql->result_array();
	}

	function get_ingresos($id){
		$this->db->select_sum("monto", "total");
		$this->db->where('usuarios_id', $id);
        $sql = $this->db->get("dnrapp_ingresos");

        return $sql->result_array();
	}

	function get_estimation_years($idUser){
		
        $sql = ' select year from(

    			SELECT YEAR(fecha) as year
    			FROM dnrapp_gastos
				WHERE dnrapp_gastos.usuarios_id = ' . $idUser . ' 

				UNION ALL 

    			SELECT YEAR(fecha) as year
    			FROM dnrapp_ingresos 
    			WHERE dnrapp_ingresos.usuarios_id = ' . $idUser . ' 
				)
				Derived GROUP BY year';
    	
    	$query =  $this->db->query($sql);

        return $query->result_array();
		
	}

}