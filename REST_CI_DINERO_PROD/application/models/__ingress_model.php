<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Ingress_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function put_ingress($idUser, $monto, $idOrigen, $fecha){
		
    	$this->db->set('usuarios_id' , $idUser);
    	$this->db->set('monto' , $monto);
    	$this->db->set('origenes_id' , $idOrigen);
    	$this->db->set('fecha' , $fecha);
    	$this->db->insert('dnrapp_ingresos');
        if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
		
	}

	function post_ingress($id,$update){

		$data = $update;
		$this->db->where('id', $id);
		$this->db->update('dnrapp_ingresos', $data); 

		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function get_ingress_month($idUser){

		$this->db->select_sum('monto');
		$this->db->where('usuarios_id',$idUser);
		$this->db->where('MONTH(fecha)', date('n') , FALSE);
		$query = $this->db->get('dnrapp_ingresos');
		$total_ingress_month = $query->result_array();
		
		return $total_ingress_month;
		
	}

	function get_ingress_list($idUser){

		$this->db->select("*");
		$this->db->select('DATE_FORMAT(fecha,\'%d-%b-%Y\') as fecha ',false );
		$this->db->select('dnrapp_ingresos.id as idIngress',false );
		$this->db->where('usuarios_id',$idUser);
		$this->db->where('YEAR(fecha)', date('Y') , FALSE);
		$this->db->where('MONTH(fecha)', date('m') , FALSE);
		$this->db->order_by('fecha');
		$this->db->from('dnrapp_ingresos');
	    $this->db->join('dnrapp_origenes', 'dnrapp_origenes.id = dnrapp_ingresos.origenes_id'); 
		$query = $this->db->get();
		return $query->result_array();
		
	}

	function get_ingress($id){
		$this->db->select("*");
		$this->db->where('id', $id);
        $sql = $this->db->get("dnrapp_ingresos");
        return $sql->result_array();
	}

	function get_graph($idUser,$anio){

		$this->db->select('*, MONTH(fecha) as mes');
		$this->db->select_sum('monto', 'total');
		$this->db->where('usuarios_id',$idUser);
		$this->db->where('YEAR(fecha)', $anio , FALSE);
		$this->db->group_by('MONTH(fecha), origenes_id');
		$this->db->from('dnrapp_ingresos');
		$query = $this->db->get();
		return $query->result_array();
		
	}

	function get_graph_days($idUser){

		$this->db->select('*, day(fecha) as dia');
		$this->db->select_sum('monto', 'total');
		$this->db->where('usuarios_id',$idUser);
		$this->db->where('YEAR(fecha)', date("Y") , FALSE);
		$this->db->where('MONTH(fecha)', date("m") , FALSE);
		$this->db->group_by('DAY(fecha), origenes_id');
		$this->db->from('dnrapp_ingresos');
		$query = $this->db->get();
		return $query->result_array();
		
	}

	function delete_ingress($id)
	{
		$this->db->where('id', $id);
		$eliminado = $this->db->delete('dnrapp_ingresos'); 
		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;

	}

}