<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Patient_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function get_patients_list($limit=10, $offset=0,$user=null){
		$this->db->select("*");

		if($user)
			$this->db->where('user_own', $user);

        $this->db->order_by("p_date", "desc");
        $this->db->limit($limit,$offset);
        $sql = $this->db->get("alpine_hc_patient");
        return $sql->result_array();
	}
	function get_patient($id){
		$this->db->select("*");
		$this->db->where('p_id', $id);
        $sql = $this->db->get("alpine_hc_patient");
        return $sql->result_array();
	}

	function put_patient($user_own,$p_first_name,$p_last_name,$p_location,$p_state,$p_state2,$p_street1,$p_street2,$p_city,$p_zip,$p_discharge_date,$p_notes,$p_contact_name,$p_contact_relation,$p_cellphone,$p_homephone,$p_date,$p_condition){
		
		$this->db->set('user_own', $user_own ); //traer desde los datos de la ssesion del usuario
        $this->db->set('p_first_name',  $p_first_name);
        $this->db->set('p_last_name', 	$p_last_name);
        $this->db->set('p_location', 	$p_location);
        $this->db->set('p_state',   	$p_state);
        $this->db->set('p_state2',   	$p_state2);
        $this->db->set('p_street1',		$p_street1);
        $this->db->set('p_street2',  	$p_street2);
        $this->db->set('p_city',  	  	$p_city);
        $this->db->set('p_zip',  	  	$p_zip);
        $this->db->set('p_date',  	  $p_discharge_date);
        $this->db->set('p_notes',  	  		$p_notes);
        $this->db->set('p_contact_name',  	$p_contact_name);
        $this->db->set('p_contact_relation',$p_contact_relation);
        $this->db->set('p_cellphone',  	  	$p_cellphone);
        $this->db->set('p_homephone',  	  	$p_homephone);
        $this->db->set('p_date',  	  	$p_date);
        $this->db->set('p_condition',  	  	$p_condition);

        $this->db->insert('alpine_hc_patient');
        if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function post_patient($id,$update){

		$data = $update;
		$this->db->where('p_id', $id);
		$this->db->update('alpine_hc_patient', $data); 

		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function delete_patient($id)
	{
		$this->db->where('p_id', $id);
		$eliminado = $this->db->delete('alpine_hc_patient'); 
		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;

	}

}