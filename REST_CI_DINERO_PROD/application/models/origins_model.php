<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Origins_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function get_data(){

		$this->db->select('id,nombre as name');
		$query = $this->db->get('dnrapp_origenes');
		
		return $query->result_array();
		
	}

}