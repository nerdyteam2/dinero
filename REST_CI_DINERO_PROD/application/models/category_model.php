<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Category_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function get_categories_list($userId){
		$this->db->select("*, dnrapp_categorias.usuarios_id as usuarios_id, dnrapp_categorias.id as id, nombre as name");
		//$this->db->select_sum("monto");
		$this->db->where('dnrapp_categorias.usuarios_id', $userId);
		$this->db->where('dnrapp_categorias.estatus', 1);
		$this->db->or_where('dnrapp_categorias.usuarios_id', NULL);
		//$this->db->order_by("monto", "desc"); 
		$this->db->order_by("dnrapp_categorias.id");
		$this->db->group_by('dnrapp_categorias.id');
		//$this->db->join('dnrapp_presupuesto', 'dnrapp_presupuesto.categorias_id = dnrapp_categorias.id','left'); 
        $sql = $this->db->get("dnrapp_categorias");
        $cats = $sql->result_array();
        
        foreach ($cats as $key => $value) {

        	$this->db->select("nombre");
			$this->db->where('dnrapp_subcategorias.categorias_id', $value['id']);
			$this->db->where('dnrapp_subcategorias.estatus', 1);
			//$this->db->order_by("dnrapp_subcategorias.id");
			//$this->db->order_by("monto", "desc"); 
			//$this->db->join('dnrapp_presupuesto', 'dnrapp_presupuesto.subcategorias_id = dnrapp_subcategorias.id','left'); 
	        $sql = $this->db->get("dnrapp_subcategorias",2);
	        $subcats = $sql->result_array();
	
        	array_push($cats[$key], $subcats);
        	$cats[$key]['subcats'] = $cats[$key][0];
        	unset($cats[$key][0]);
        }

        return $cats;

	}

	function get_only_categories_list($userId){
		$this->db->select("id, nombre");
		$this->db->where('usuarios_id', $userId);
		$this->db->or_where('usuarios_id', NULL);
        $sql = $this->db->get("dnrapp_categorias");
        return $sql->result_array();
	}

	function get_category($id){
		$this->db->select("*");
		$this->db->where('id', $id);
        $sql = $this->db->get("dnrapp_usuarios");
        return $sql->result_array();
	}
	

	function put_category($nombre,$idUser){

		$this->db->set('nombre'  , $nombre ); //traer desde los datos de la ssesion del usuario
        $this->db->set('usuarios_id' , $idUser);
        $this->db->insert('dnrapp_categorias');
        if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function post_category($id,$update){

		$data = $update;
		$this->db->where('id', $id);
		$this->db->update('dnrapp_usuarios', $data); 

		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}


	function delete_category($id){

		$this->db->where('id', $id);
		$this->db->set('estatus', 0);
		$this->db->update('dnrapp_categorias'); 

		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

}