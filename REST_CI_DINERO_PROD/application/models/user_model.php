<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class User_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function get_users_list($limit=10, $offset=0){
		$this->db->select("*");
        $this->db->order_by("fecha_alta", "desc");
        $this->db->limit($limit,$offset);
        $sql = $this->db->get("dnrapp_usuarios");
        return $sql->result_array();
	}
	function get_user_email($email){
	    $this->db->select("id,proceso,email");
		$this->db->where('email', $email);
		$this->db->limit(1,0);
        $sql = $this->db->get("dnrapp_usuarios");
        return $sql->result_array();
	}
	function get_user($id){
		$this->db->select("*");
		$this->db->where('id', $id);
        $sql = $this->db->get("dnrapp_usuarios");
        return $sql->result_array();
	}
	function get_userbyemail($email){
		$this->db->select("*");
		$this->db->where('email', $email);
        $sql = $this->db->get("dnrapp_usuarios");
        return $sql->result_array();
	}

	function put_user($nombres,$email,$pass, $confirm){

		if(count($this->get_userbyemail($email))){
			return FALSE;
		}
		
		$this->db->set('nombres'   , $nombres ); //traer desde los datos de la ssesion del usuario
        $this->db->set('email'     , $email);
        $this->db->set('password'  , $pass);
        $this->db->set('confirm'   , $confirm);
        $this->db->set('fecha_alta', date('Y-m-d'));
        $this->db->insert('dnrapp_usuarios');
        if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function post_user($id,$update){

		$data = $update;
		$this->db->where('id', $id);
		$this->db->update('dnrapp_usuarios', $data); 

		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function delete_user($id)
	{
		$this->db->where('id', $id);
		$eliminado = $this->db->delete('dnrapp_usuarios'); 
		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;

	}

	function login_user($nombre,$pass){
		$this->db->select('*');

		if (filter_var($nombre, FILTER_VALIDATE_EMAIL)) 
		  $this->db->where('email', "'{$nombre}'", FALSE);
		else
		  $this->db->where('nombres', "'{$nombre}'", FALSE);
		
		$this->db->where('password' , "'{$pass}'", FALSE);
		$this->db->where('estatus' , 1);
		//$this->db->where('platform >= '.$platform);
		$login = $this->db->get('dnrapp_usuarios');

		if ($login->num_rows() > 0)
		return $login->result();
		else return FALSE;
		$login->free_result();
	}

	function post_paso($idUser, $nStep){
		
    	$this->db->where('id' , $idUser);
    	$this->db->set('proceso' , $nStep);
    	$this->db->update('dnrapp_usuarios');
        if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
		
	}

	function post_change_pass($idUser,$pass){
		
    	$this->db->where('id' , $idUser);
    	$this->db->set('password', $pass);
    	$this->db->update('dnrapp_usuarios');
        if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
		
	}

	function post_limit($idUser,$limit){
		
    	$this->db->where('id' , $idUser);
    	$this->db->set('limite', $limit);
    	$this->db->update('dnrapp_usuarios');
        if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
		
	}

	function get_limit($idUser){
		
		$this->db->select('*');
    	$this->db->where('id' , $idUser);
        $sql = $this->db->get("dnrapp_usuarios");
        return $sql->result_array();
	}



}