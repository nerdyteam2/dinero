<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Nurse_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function get_nurses_list($limit=10, $offset=0){
		$this->db->select("*");
        $this->db->order_by("n_date", "desc");
        $this->db->limit($limit,$offset);
        $sql = $this->db->get("alpine_hc_nurse");
        return $sql->result_array();
	}
	function get_nurse($id){
		$this->db->select("*");
		$this->db->where('n_id', $id);
        $sql = $this->db->get("alpine_hc_nurse");
        return $sql->result_array();
	}
	function put_nurse($first_name,$last_name,$email,$phone){

		$this->db->set('n_first_name', $first_name ); //traer desde los datos de la ssesion del usuario
        $this->db->set('n_last_name',  $last_name);
        $this->db->set('n_email', 	   $email);
        $this->db->set('n_phone', 	   $phone);
        $this->db->insert('alpine_hc_nurse');
        if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function post_nurse($id,$update){

		$data = $update;
		$this->db->where('n_id', $id);
		$this->db->update('alpine_hc_nurse', $data); 

		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function delete_nurse($id)
	{
		$this->db->where('n_id', $id);
		$eliminado = $this->db->delete('alpine_hc_nurse'); 
		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;

	}


}