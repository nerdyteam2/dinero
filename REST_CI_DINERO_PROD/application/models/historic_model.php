<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Historic_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function get_years_ingress($idUser){

		$this->db->select('YEAR(fecha) as year');
		$this->db->where('usuarios_id', $idUser);
		$this->db->group_by('YEAR(fecha)');
		$query = $this->db->get('dnrapp_ingresos');
		
		return $query->result_array();
		
	}

	function get_years_expenses($idUser){

		$this->db->select('YEAR(fecha) as year');
		$this->db->where('usuarios_id', $idUser);
		$this->db->group_by('YEAR(fecha)');
		$query = $this->db->get('dnrapp_gastos');
		
		return $query->result_array();
		
	}

}