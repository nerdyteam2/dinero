<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Expenses_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function put_expenses($idUser, $monto, $idOrigen, $fecha, $idCat, $idSubCat, $descripcion){
		
    	$this->db->set('usuarios_id' , $idUser);
    	$this->db->set('monto' , $monto);
    	$this->db->set('origenes_gastos_id' , $idOrigen);
    	$this->db->set('categorias_id' , $idCat);
    	$this->db->set('subcategorias_id' , $idSubCat);
    	$this->db->set('fecha' , $fecha);
    	$this->db->set('descripcion' , $descripcion);
    	$this->db->insert('dnrapp_gastos');
        if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
		
	}

	function post_expenses($id,$update){

		$data = $update;
		$this->db->where('id', $id);
		$this->db->update('dnrapp_gastos', $data); 

		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function get_expenses_list($idUser){

		$this->db->select("*");
		$this->db->select("dnrapp_categorias.nombre as categoria", false);
		$this->db->select("dnrapp_subcategorias.nombre as subcategoria", false);
		$this->db->select("dnrapp_origenes_gastos.nombre as origen", false);
		$this->db->select("dnrapp_gastos.id as idExpenses", false);
		$this->db->select('DATE_FORMAT(fecha,\'%d-%b-%Y\') as fecha ',false );
		$this->db->where('dnrapp_gastos.usuarios_id',$idUser);
		$this->db->where('YEAR(fecha)', date('Y') , FALSE);
		$this->db->where('MONTH(fecha)', date('m') , FALSE);
		$this->db->order_by('fecha');
		$this->db->from('dnrapp_gastos');
	    $this->db->join('dnrapp_origenes_gastos', 'dnrapp_origenes_gastos.id = dnrapp_gastos.origenes_gastos_id'); 
	    $this->db->join('dnrapp_categorias', 'dnrapp_categorias.id = dnrapp_gastos.categorias_id'); 
	    $this->db->join('dnrapp_subcategorias', 'dnrapp_subcategorias.id = dnrapp_gastos.subcategorias_id'); 
		$query = $this->db->get();
		return $query->result_array();
		
	}

	function get_expenses_month($idUser){

		$this->db->select_sum('monto');
		$this->db->where('usuarios_id',$idUser);
		$this->db->where('MONTH(fecha)', date('n') , FALSE);
		$query = $this->db->get('dnrapp_gastos');
		$total_ingress_month = $query->result_array();
		
		return $total_ingress_month;
		
	}

	function get_expenses_table($idUser,$mes,$anio){

		$this->db->select('*');
		$this->db->select_sum('monto','total' );
		$this->db->where('usuarios_id',$idUser);
		$this->db->where('MONTH(fecha)', $mes , FALSE);
		$this->db->where('YEAR(fecha)', $anio , FALSE);
		$this->db->group_by('categorias_id');
		$query = $this->db->get('dnrapp_gastos');
		$total_ingress_month = $query->result_array();
		
		return $total_ingress_month;
		
	}

	function get_expenses_ahorraste($idUser,$mes,$anio){

		$this->db->select_sum('monto','total' );
		$this->db->where('usuarios_id',$idUser);
		$this->db->where('MONTH(fecha)', $mes , FALSE);
		$this->db->where('YEAR(fecha)', $anio , FALSE);
		$query = $this->db->get('dnrapp_gastos');

		return $query->result_array();;
		
	}

	function get_graph($idUser,$anio){

		$this->db->select('*, MONTH(fecha) as mes');
		$this->db->select_sum('monto', 'total');
		$this->db->where('usuarios_id',$idUser);
		$this->db->where('YEAR(fecha)', $anio , FALSE);
		$this->db->group_by('MONTH(fecha), origenes_gastos_id');
		$this->db->from('dnrapp_gastos');
		$query = $this->db->get();
		return $query->result_array();
		
	}

	function get_graph_days($idUser){

		$this->db->select('*, day(fecha) as dia');
		$this->db->select_sum('monto', 'total');
		$this->db->where('usuarios_id',$idUser);
		$this->db->where('YEAR(fecha)', date("Y") , FALSE);
		$this->db->where('MONTH(fecha)', date("m") , FALSE);
		$this->db->group_by('DAY(fecha), origenes_gastos_id');
		$this->db->from('dnrapp_gastos');
		$query = $this->db->get();
		return $query->result_array();
		
	}

	function get_gastos_vs_presupuesto($idUser, $anio, $mes){

		$this->db->select('*');
		$this->db->select_sum('monto', 'total');	
		$this->db->where('usuarios_id',$idUser);
		$this->db->where('YEAR(fecha)', $anio , FALSE);
        $this->db->where('MONTH(fecha)', $mes , FALSE);
		$this->db->group_by('categorias_id');
		$this->db->from('dnrapp_gastos');
		$query = $this->db->get();
		$total_ingress = $query->result_array();
		
		return $total_ingress;
		
	}

	function get_expenses($id){
		$this->db->select_sum("monto", "total");
		$this->db->where('usuarios_id', $id);
		$this->db->where('MONTH(fecha)', date('n') , FALSE);
        $sql = $this->db->get("dnrapp_gastos");
        return $sql->result_array();
	}


	function get_expenses_unic($id){
		$this->db->select("*");
		$this->db->where('id', $id);
        $sql = $this->db->get("dnrapp_gastos");
        return $sql->result_array();
	}

	function delete_expenses($id)
	{
		$this->db->where('id', $id);
		$eliminado = $this->db->delete('dnrapp_gastos'); 
		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;

	}

}