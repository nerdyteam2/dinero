<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Subcategory_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function get_subcategories_list($idUser,$idCat){

		// SELECT * FROM (`dnrapp_subcategorias`) 
		// WHERE (`categorias_id` = '1' AND `usuarios_id` IS NULL ) 
		// OR (`usuarios_id` = '1' AND `categorias_id` = '1');

		$this->db->select("*, dnrapp_subcategorias.usuarios_id as usuarios_id, dnrapp_subcategorias.id as id");
		$this->db->where   ("(dnrapp_subcategorias.categorias_id = ".$idCat." AND dnrapp_subcategorias.usuarios_id IS NULL AND dnrapp_subcategorias.estatus = 1)",NULL,FALSE);
		$this->db->or_where("(dnrapp_subcategorias.usuarios_id = ".$idUser." AND dnrapp_subcategorias.categorias_id = ".$idCat." AND dnrapp_subcategorias.estatus = 1)",NULL,FALSE);
		//$this->db->order_by("dnrapp_subcategorias.id");
		//$this->db->order_by("monto", "desc"); 
		$this->db->group_by("dnrapp_subcategorias.id"); 
		//$this->db->join('dnrapp_presupuesto', 'dnrapp_presupuesto.subcategorias_id = dnrapp_subcategorias.id','left'); 
        $sql = $this->db->get("dnrapp_subcategorias");
       
        $subcats = $sql->result_array();
        
        foreach ($subcats as $key => $value) {

        	$this->db->select("monto");
			$this->db->where('subcategorias_id', $value['id']);
			$this->db->where('categorias_id', $idCat);
			$this->db->where('usuarios_id', $idUser);
	        $sql = $this->db->get("dnrapp_presupuesto");
	        $monto = $sql->result_array();
  				
        	array_push($subcats[$key], $monto);
        	$subcats[$key]['monto'] = $subcats[$key][0];
        	unset($subcats[$key][0]);
        }

		$this->db->select_sum('monto');
		$this->db->where('dnrapp_presupuesto.usuarios_id',$idUser);
		$this->db->where('dnrapp_presupuesto.categorias_id',$idCat);
		$this->db->where('dnrapp_subcategorias.estatus', 1);
		$this->db->join('dnrapp_subcategorias', 'dnrapp_subcategorias.id = dnrapp_presupuesto.subcategorias_id','left'); 
		$query = $this->db->get('dnrapp_presupuesto');
		$monto_total = $query->result_array();

		$subcats['total_cat'] = $monto_total[0]['monto'];

        return $subcats;

	}

	function get_category($id){
		$this->db->select("*");
		$this->db->where('id', $id);
        $sql = $this->db->get("dnrapp_usuarios");
        return $sql->result_array();
	}
	

	function put_subcategory($nombre,$idUser,$idCat){

		$this->db->set('nombre'  , $nombre ); //traer desde los datos de la ssesion del usuario
        $this->db->set('usuarios_id' , $idUser);
        $this->db->set('categorias_id' , $idCat);
        $this->db->insert('dnrapp_subcategorias');
        if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function post_category($id,$update){

		$data = $update;
		$this->db->where('id', $id);
		$this->db->update('dnrapp_usuarios', $data); 

		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function login_category($email,$pass){
		$this->db->select('*');
		$this->db->where('email', "'{$email}'", FALSE);
		$this->db->where('password' , "'{$pass}'", FALSE);
		$this->db->where('estatus' , "1");
		//$this->db->where('platform >= '.$platform);
		$login = $this->db->get('dnrapp_usuarios');

		if ($login->num_rows() > 0)
		return $login->result();
		else return FALSE;
		$login->free_result();
	}

	function delete_subcategory($id){

		$this->db->where('id', $id);
		$this->db->set('estatus', 0);
		$this->db->update('dnrapp_subcategorias'); 

		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}



}