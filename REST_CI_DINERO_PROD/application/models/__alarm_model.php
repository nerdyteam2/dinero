<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Alarm_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function put_alarm($idUser, $nombre, $descripcion, $fecha, $repetir){
		
    	$this->db->set('usuarios_id' , $idUser);
    	$this->db->set('nombre' , $nombre);
    	$this->db->set('descripcion' , $descripcion);
    	$this->db->set('fecha_inicio' , $fecha);
    	$this->db->set('recurrente' , $repetir);
    	$this->db->insert('dnrapp_alarmas');
        if ($this->db->affected_rows() > 0) return $this->db->insert_id();
		else return FALSE;
		
	}

	function get_alarms_list($idUser){
		$this->db->select("*");
		$this->db->where('usuarios_id', $idUser);
		//$this->db->where('estatus', 1);
		$this->db->order_by('id');
		$this->db->order_by('estatus','desc');
        $sql = $this->db->get("dnrapp_alarmas");
        return $sql->result_array();
	}
	function get_alarm($id){
		$this->db->select("*");
		$this->db->select("YEAR(fecha_inicio) as year");
		$this->db->select("MONTH(fecha_inicio) as month");
		$this->db->select("DAY(fecha_inicio) as day");
		$this->db->select("HOUR(fecha_inicio) as hour");
		$this->db->select("MINUTE(fecha_inicio) as minute");
		$this->db->where('id', $id);
        $sql = $this->db->get("dnrapp_alarmas");
        return $sql->result_array();
	}

	function post_alarm($idAlarma,$update){

		$this->db->where('id', $idAlarma);
		$this->db->update('dnrapp_alarmas',$update); 

		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;
	}

	function delete_alarm($id)
	{
		$this->db->where('id', $id);
		$eliminado = $this->db->delete('dnrapp_alarmas'); 
		if ($this->db->affected_rows() > 0) return TRUE;
		else return FALSE;

	}


}