<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

class Reports_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database("default");
	}

	function reports_get($idUser, $anio, $mes){

    	$sql = ' 
    			SELECT fecha, monto, dnrapp_origenes_gastos.nombre as origen, dnrapp_categorias.nombre as nom_cat, dnrapp_subcategorias.nombre as nom_subcat, dnrapp_gastos.descripcion 
    			FROM dnrapp_gastos
				LEFT JOIN dnrapp_origenes_gastos
				ON dnrapp_gastos.origenes_gastos_id = dnrapp_origenes_gastos.id  
				LEFT JOIN dnrapp_categorias
				ON dnrapp_gastos.categorias_id = dnrapp_categorias.id
				LEFT JOIN dnrapp_subcategorias
				ON dnrapp_gastos.subcategorias_id = dnrapp_subcategorias.id
				WHERE  
					YEAR(fecha) = '. $anio . '
				AND ';
		if($mes)
		$sql .=	' MONTH(fecha) = '. $mes . ' AND ';

		$sql .=	' dnrapp_gastos.usuarios_id	= ' . $idUser . ' 

				UNION ALL 

    			SELECT fecha, monto, dnrapp_origenes.nombre as origen, "-" as nom_cat, "-" as nom_subcat, dnrapp_ingresos.descripcion 
    			FROM dnrapp_ingresos 
    			LEFT JOIN dnrapp_origenes
				ON dnrapp_ingresos.origenes_id = dnrapp_origenes.id 
    			WHERE  
					YEAR(fecha) = '. $anio . '
				AND';
		if($mes)
		$sql .= ' MONTH(fecha) = '. $mes . ' AND '; 

		$sql .=	' dnrapp_ingresos.usuarios_id	= ' . $idUser . ' 
				
				ORDER BY fecha ASC;';
    	
    	$query =  $this->db->query($sql);

        return $query->result();

	}

	function gastos_get($idUser, $anio, $mes){

		$this->db->select_sum('monto');
		$this->db->where('usuarios_id', $idUser);
		$this->db->where('YEAR(fecha)', $anio);
		if($mes)
		$this->db->where('MONTH(fecha)', $mes);
		$this->db->group_by('usuarios_id');
		$gastos = $this->db->get('dnrapp_gastos');

		return $gastos->row();

	}

	function ingresos_get($idUser, $anio, $mes){

		$this->db->select_sum('monto');
		$this->db->where('usuarios_id', $idUser);
		$this->db->where('YEAR(fecha)', $anio);
		if($mes)
		$this->db->where('MONTH(fecha)', $mes);
		$this->db->group_by('usuarios_id');
		$gastos = $this->db->get('dnrapp_ingresos');

		return $gastos->row();

	}

	function email_get($idUser){
		$this->db->select('email');
		$this->db->where('id',$idUser);
		$gastos = $this->db->get('dnrapp_usuarios');

		return $gastos->row()->email;
	}

}
