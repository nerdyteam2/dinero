<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';


if (! function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( !array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( !array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}

class Expenses extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Expenses_Model');
	}

    function data_put()
    {

        $data  = array();
        $error = '';

        $idUser   = $this->put('idUser');
        $monto    = $this->put('monto');
        $idOrigen = $this->put('idOrigen');
        $idCat    = $this->put('idCat');
        $idSubCat = $this->put('idSubCat');
        $fecha    = $this->put('date');
        if($this->put('descripcion'))
            $descripcion = $this->put('descripcion');
        else
            $descripcion = '';

        if(!$idUser && is_numeric($idUser))
            $error = 'id user incorrect';
        if(!$idCat && is_numeric($idCat))
            $error = 'Debe ingresar todos los datos';
        if(!$idOrigen && is_numeric($idOrigen))
            $error = 'Debe ingresar todos los datos';
        if(!$idSubCat && is_numeric($idSubCat))
            $error = 'Debe ingresar todos los datos';
        if(!$fecha)
            $error = 'Debe ingresar todos los datos';
        if(!$monto)
            $error = 'Debe ingresar todos los datos';

        $monto = str_replace(",", "", $monto);

        if($error=='')
            {

                $insert = $this->Expenses_Model->put_expenses($idUser, $monto, $idOrigen, $fecha, $idCat, $idSubCat, $descripcion);

                if($insert)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database'));
            }
        else
            $this->response(array('status'=>false,'error'=>$error));
    }

    function data_post()
    {
        $data   = array();
        $update = array();
        $error  = '';

        $monto = $this->post('monto');
        $monto = str_replace(",", "", $monto);
        $id   = $this->post('idExpenses');
        if($id && is_numeric($id)){
            //Peticion especifica
            $data = $this->Expenses_Model->get_expenses_unic($id);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
        }

        //Validar los datos
        if(!$this->post('monto'))
            $error = 'Debe ingresar todos los datos';
        else
            $update['monto'] = $monto;

        if($error=='')
            {
                $update = $this->Expenses_Model->post_expenses($id,$update);

                if($update)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not update data into database'));
            }
        else
            $this->response(array('status'=>false,'error'=>$error));


        $this->response($data);
    }

    function data_get()
    {
        $idUser     = $this->get('id');
        $idExpenses = $this->get('idExpenses');
        $data       = array();

        if($idUser && is_numeric($idUser)){

            if($idExpenses && is_numeric($idExpenses)){

                 //Peticion especifica
                $data = $this->Expenses_Model->get_expenses_unic($idExpenses);
                if(count($data)==0)
                    $data = array('status'=>false,'error'=>'Without results for this criteria');
                else
                    $data = array('status'=>true,'data'=>$data);

            } else {

                //Peticion generica
                $data = $this->Expenses_Model->get_expenses_list($idUser);

                if(count($data)==0)
                    $data = array('status'=>false,'error'=>'Without results for this criteria');
                else
                    $data = array('status'=>true,'data'=>$data);

            }

        }

         $this->response($data);
    }

    function total_mes_get()
    {
        $list   = false;
        $idUser = $this->get('idUser');
        $data   = array();

        if($idUser && is_numeric($idUser)){
            //Peticion especifica
            $data = $this->Expenses_Model->get_expenses_month($idUser);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
            else
                $data = array('status'=>true,'data'=>$data);
        }

        $this->response($data);
    }

    /*grafica anterior con origenes como datos*/
    /*function graph_get()
    {
        $list   = false;
        $idUser = $this->get('idUser');
        $anio   = $this->get('anio');
        $data   = array();

        $anio = (!$anio || $anio == 0) ? date('Y') : $anio;

        if($idUser && is_numeric($idUser)){

            $expenses = $this->Expenses_Model->get_graph($idUser,$anio);

            $this->load->model('Origins_Expenses_Model');
            $origin_expenses = $this->Origins_Expenses_Model->get_data();

            foreach ($origin_expenses as $key => $value) {

                $expenses_month = array(0,0,0,0,0,0,0,0,0,0,0,0);

                foreach ($expenses as $key_expenses => $value_expenses) {

                    if($value['id'] == $value_expenses['origenes_gastos_id']){

                        $expenses_month[$value_expenses['mes'] - 1] = floatval($value_expenses['total']);

                    }

                $origin_expenses[$key]['data'] = $expenses_month;

                }


            }

                $data = array('status'=>true,'data'=>$origin_expenses);
        }

        $this->response($data);

    }*/

    function graph_get()
    {
        $list   = false;
        $idUser = $this->get('idUser');
        $anio   = $this->get('anio');
        $data   = array();
        $categories_data = array();

        $anio = (!$anio || $anio == 0) ? date('Y') : $anio;

        if($idUser && is_numeric($idUser)){

            $this->load->model('Category_Model');
            $categories = $this->Category_Model->get_only_categories_list($idUser);

            $expenses = $this->Expenses_Model->get_gastos_vs_presupuesto_month($idUser,$anio);

            foreach ($categories as $key => $category) {

                $expenses_month = array(0,0,0,0,0,0,0,0,0,0,0,0);

                $key_expenses   = false;
                $key_expenses   = array_search($category['id'], array_column($expenses, 'categorias_id'));

                if($key_expenses !== false){
                    
                    foreach ($expenses as $key_expenses => $value_expenses){ 
                        if($value_expenses['categorias_id'] == $category['id']){
                            $expenses_month[$value_expenses['mes'] - 1] = floatval($value_expenses['total']);
                            
                        }
                    }

                    $values = array('id'=>$category['id'],'name'=>$category['nombre'],'data'=>$expenses_month);
                    array_push($categories_data, $values );
                    
                }

           }

            //totales
            $sumArray=array();
            foreach($categories_data as $value)
            {
              foreach($value['data'] as $key=>$secondValue)
               {
                   if(!isset($sumArray[$key]))
                    {
                       $sumArray[$key]=0;
                    }
                   $sumArray[$key]+=$secondValue;
               }
            }

            array_push($categories_data,array('id'=>0,'name'=>'Total','data'=>$sumArray));

            $data = array('status'=>true,'data'=>$categories_data);
        }

        $this->response($data);

    }

/*grafica anterior con origenes como datos*/
    function graph_days_origins_get()
    {
        $list   = false;
        $data   = array();

        $idUser = $this->get('idUser');
        $grAnio = $this->get('grAnio');
        $grMes  = $this->get('grMes');

        $grAnio = (!$grAnio || $grAnio == 0) ? date('Y') : $grAnio;
        $grMes = (!$grMes || $grMes == 0) ? date('m') : $grMes;

        if($idUser && is_numeric($idUser)){

            $expenses = $this->Expenses_Model->get_graph_new_days($idUser,$grAnio, $grMes);

            $this->load->model('Origins_Expenses_Model');
            $origin_expenses = $this->Origins_Expenses_Model->get_data();

            $daysMonth = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));

            foreach ($origin_expenses as $key => $value) {

                 $expenses_days = array_fill(0, $daysMonth, 0);

                foreach ($expenses as $key_expenses => $value_expenses) {

                    if($value['id'] == $value_expenses['origenes_gastos_id']){

                        $expenses_days[$value_expenses['dia'] - 1] = floatval($value_expenses['total']);

                    }

                }

                $origin_expenses[$key]['data'] = $expenses_days;

            }

            $values = array_combine(range(0,$daysMonth-1),range(1,$daysMonth));
            $data = array('status'=>true,'data'=>$origin_expenses, 'values'=>$values);
        }

        $this->response($data);

    }

    function graph_days_get()
    {
        $list   = false;
        $data   = array();

        $idUser = $this->get('idUser');

        if($idUser && is_numeric($idUser)){

            $expenses = $this->Expenses_Model->get_graph_days($idUser);

            $this->load->model('Origins_Expenses_Model');
            $origin_expenses = $this->Origins_Expenses_Model->get_data();

            $daysMonth = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));

            foreach ($origin_expenses as $key => $value) {

                 $expenses_days = array_fill(0, $daysMonth, 0);

                foreach ($expenses as $key_expenses => $value_expenses) {

                    if($value['id'] == $value_expenses['origenes_gastos_id']){

                        $expenses_days[$value_expenses['dia'] - 1] = floatval($value_expenses['total']);

                    }

                }

                $origin_expenses[$key]['data'] = $expenses_days;

            }

            $values = array_combine(range(0,$daysMonth-1),range(1,$daysMonth));
            $data = array('status'=>true,'data'=>$origin_expenses, 'values'=>$values);
        }

        $this->response($data);

    }

    function graph_days_new_get()
    {

        $data   = array();
        $days   = array();

        $idUser = $this->get('idUser');
        $grAnio = $this->get('grAnio');
        $grMes  = $this->get('grMes');

        $grAnio = (!$grAnio || $grAnio == 0) ? date('Y') : $grAnio;
        $grMes = (!$grMes || $grMes == 0) ? date('m') : $grMes;

        if($idUser && is_numeric($idUser)){

            $this->load->model('Category_Model');
            $categories = $this->Category_Model->get_only_categories_list($idUser);
            
            $expenses = $this->Expenses_Model->get_gastos_vs_presupuesto_day($idUser,$grAnio,$grMes);

            $categories_data = array();

            $daysMonth = cal_days_in_month(CAL_GREGORIAN, $grMes, $grAnio);

            foreach ($categories as $key => $category) {

               $key_expenses   = false;
               $key_expenses   = array_search($category['id'], array_column($expenses, 'categorias_id'));

               if($key_expenses !== false){
                    
                    foreach ($expenses as $key_expenses => $value_expenses){ 
                        if($value_expenses['categorias_id'] == $category['id']){
                            $expenses_days = array_fill(0, $daysMonth, 0);
                            $expenses_days[$value_expenses['dia'] - 1] = floatval($value_expenses['total']);
                            
                        }
                    }

                    $values_ = array('id'=>$category['id'],'name'=>$category['nombre'],'data'=>$expenses_days);
                    array_push($categories_data, $values_ );
                    
                }

            }

            //totales
            $sumArray=array();
            foreach($categories_data as $value)
            {
              foreach($value['data'] as $key=>$secondValue)
               {
                   if(!isset($sumArray[$key]))
                    {
                       $sumArray[$key]=0;
                    }
                   $sumArray[$key]+=$secondValue;
               }
            }
           
            array_push($categories_data,array('id'=>0,'name'=>'Total','data'=>$sumArray));

            $values = array_combine(range(0,$daysMonth-1),range(1,$daysMonth));
            $data = array('status'=>true,'data'=>$categories_data, 'values'=>$values);
        }

        $this->response($data);

    }

    function data_delete()
    {
        $data   = array();
        $error  = '';

        $idExpenses   = $this->delete('idExpenses');
        if($idExpenses && is_numeric($idExpenses)){
            $data = $this->Expenses_Model->get_expenses_unic($idExpenses);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
        }

        $delete = $this->Expenses_Model->delete_expenses($idExpenses);

                if($delete)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not delete data from database'));

        $this->response($data);
    }

		function data_arr_put() {

			if( !$this->put('data') )
				$data_get   = json_decode($this->put('data'));
			else
				$this->response(
					array(
						'status' => false,
						'error'  => 'No existen datos'
					)
				);

			if( empty($data_get) || !is_array($data_get) )
				$this->response(
					array(
						'status' => false,
						'error'  => 'Datos inválidos'
					)
				);

			$data_go = array();
			$error = "";
			$count = count($data_get);

			for($i = 0; $i < $count; $i++) {
				$data_go[$i] = array(
					"origenes_gastos_id" 	 => isset($data_get[$i]->idOrigen) ? $data_get[$i]->idOrigen : '',
					"usuarios_id"   			 => isset($data_get[$i]->idUser) ? $data_get[$i]->idUser : '',
					"categorias_id"    		 => isset($data_get[$i]->idCat) ? $data_get[$i]->idCat : '',
					"subcategorias_id"     => isset($data_get[$i]->idSubCat) ? $data_get[$i]->idSubCat : '',
					"monto"    						 => isset($data_get[$i]->monto) ? str_replace(",", "", $data_get[$i]->monto) : '',
					"fecha"    						 => isset($data_get[$i]->date) ? $data_get[$i]->date : '',
					"descripcion"			     => isset($data_get[$i]->descripcion) ? $data_get[$i]->descripcion : '',
				);
			}

			for($i = 0; $i < $count; $i++) {
				if(empty($data_go[$i]["origenes_gastos_id"]) && is_numeric($data_go[$i]["origenes_gastos_id"])) $error = "Campo Origen inválido";
				if(empty($data_go[$i]["usuarios_id"]) && is_numeric($data_go[$i]["usuarios_id"])) $error = "Campo Usuario inválido";
				if(empty($data_go[$i]["categorias_id"]) && is_numeric($data_go[$i]["categorias_id"])) $error = "Campo Categorías inválido";
				if(empty($data_go[$i]["subcategorias_id"]) && is_numeric($data_go[$i]["subcategorias_id"])) $error = "Campo Subcategorías inválido";
				if(empty($data_go[$i]["monto"])) $error = "Campo Monto inválido";
				if(empty($data_go[$i]["fecha"])) $error = "Campo Fecha inválido";
			}

			if($error === '') {

				$insert = $this->Expenses_Model->put_arr_expenses($data_go);
				if($insert)
						$this->response(array('status'=>true));
				else
						$this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database'));

			} else
				$this->response(
					array(
						'status' => false,
						'error'  => $error
					)
				);

		}

		function data_edit_put() {

			if( !$this->put('data') )
				$data_get   = json_decode($this->put('data'));
			else
				$this->response(
					array(
						'status' => false,
						'error'  => 'No existen datos'
					)
				);

			if( empty($data_get) || !is_array($data_get) )
				$this->response(
					array(
						'status' => false,
						'error'  => 'Datos inválidos'
					)
				);

			$data_go = array();
			$count = count($data_get);

			for($i = 0; $i < $count; $i++) {
				$data_go[$i] = array(
					"monto"    => isset($data_get[$i]->monto) ? str_replace(",", "", $data_get[$i]->monto) : '',
				);
			}

			for( $i = 0; $i < $count; $i++ )
				$insert = $this->Expenses_Model->put_edit_expenses($data_get[$i]->id, $data_go[$i]);

			if($insert)
					$this->response(array('status'=>true));
			else
					$this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database'));


		}

		function data_del_delete() {

			if( !$this->delete('data') )
				$data_get   = json_decode($this->delete('data'));
			else
				$this->response(
					array(
						'status' => false,
						'error'  => 'No existen datos'
					)
				);

			if( empty($data_get) || !is_array($data_get) )
				$this->response(
					array(
						'status' => false,
						'error'  => 'Datos inválidos'
					)
				);

			$data_go = array();
			$count = count($data_get);

			for( $i = 0; $i < $count; $i++ )
				$insert = $this->Expenses_Model->delete_del_expenses($data_get[$i]->id);

			if($insert)
					$this->response(array('status'=>true));
			else
					$this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database'));


		}

        function years_get()
        {
            $idUser = $this->get('idUser');

            if($idUser && is_numeric($idUser)){
                $years = $this->Expenses_Model->get_expenses_years($idUser);

                if($years)
                    $this->response(array('status'=>true,'years'=>$years));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not connect to database'));

            }

            $this->response(array('status'=>false));
        }

}
