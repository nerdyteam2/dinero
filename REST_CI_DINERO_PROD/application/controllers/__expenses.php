<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Expenses extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Expenses_Model');
	}

    function data_put()
    {

        $data  = array();
        $error = '';

        $idUser   = $this->put('idUser');
        $monto    = $this->put('monto');
        $idOrigen = $this->put('idOrigen');
        $idCat    = $this->put('idCat');
        $idSubCat = $this->put('idSubCat');
        $fecha    = $this->put('date');
        if($this->put('descripcion'))
            $descripcion = $this->put('descripcion'); 
        else
            $descripcion = '';

        if(!$idUser && is_numeric($idUser))
            $error = 'id user incorrect';
        if(!$idCat && is_numeric($idCat))
            $error = 'Debe ingresar todos los datos';
        if(!$idOrigen && is_numeric($idOrigen))
            $error = 'Debe ingresar todos los datos';
        if(!$idSubCat && is_numeric($idSubCat))
            $error = 'Debe ingresar todos los datos';
        if(!$fecha)
            $error = 'Debe ingresar todos los datos';
        if(!$monto)
            $error = 'Debe ingresar todos los datos';

        $monto = str_replace(",", "", $monto);
        
        if($error=='')
            {

                $insert = $this->Expenses_Model->put_expenses($idUser, $monto, $idOrigen, $fecha, $idCat, $idSubCat, $descripcion);

                if($insert)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database'));     
            }
        else
            $this->response(array('status'=>false,'error'=>$error));
    }

    function data_post()
    {       
        $data   = array();
        $update = array();
        $error  = '';

        $monto = $this->post('monto');
        $monto = str_replace(",", "", $monto);
        $id   = $this->post('idExpenses');
        if($id && is_numeric($id)){
            //Peticion especifica
            $data = $this->Expenses_Model->get_expenses_unic($id);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
        }

        //Validar los datos
        if(!$this->post('monto'))
            $error = 'Debe ingresar todos los datos';
        else
            $update['monto'] = $monto;
        
        if($error=='')
            {
                $update = $this->Expenses_Model->post_expenses($id,$update);

                if($update)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not update data into database'));     
            }
        else
            $this->response(array('status'=>false,'error'=>$error));


        $this->response($data);
    }

    function data_get()
    {
        $idUser     = $this->get('id');
        $idExpenses = $this->get('idExpenses');
        $data       = array();

        if($idUser && is_numeric($idUser)){

            if($idExpenses && is_numeric($idExpenses)){

                 //Peticion especifica
                $data = $this->Expenses_Model->get_expenses_unic($idExpenses);
                if(count($data)==0)
                    $data = array('status'=>false,'error'=>'Without results for this criteria');
                else
                    $data = array('status'=>true,'data'=>$data);

            } else {

                //Peticion generica
                $data = $this->Expenses_Model->get_expenses_list($idUser);

                if(count($data)==0)
                    $data = array('status'=>false,'error'=>'Without results for this criteria');
                else
                    $data = array('status'=>true,'data'=>$data);

            }

        }

         $this->response($data);
    }

    function total_mes_get()
    {
        $list   = false;
        $idUser = $this->get('idUser');
        $data   = array();

        if($idUser && is_numeric($idUser)){
            //Peticion especifica
            $data = $this->Expenses_Model->get_expenses_month($idUser);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
            else
                $data = array('status'=>true,'data'=>$data);
        }
        
        $this->response($data);
    }

    function graph_get()
    {
        $list   = false;
        $idUser = $this->get('idUser');
        $anio   = $this->get('anio');
        $data   = array();

        $anio = (!$anio || $anio == 0) ? date('Y') : $anio;

        if($idUser && is_numeric($idUser)){
      
            $expenses = $this->Expenses_Model->get_graph($idUser,$anio);

            $this->load->model('Origins_Expenses_Model');
            $origin_expenses = $this->Origins_Expenses_Model->get_data();

            foreach ($origin_expenses as $key => $value) {

                $expenses_month = array(0,0,0,0,0,0,0,0,0,0,0,0);

                foreach ($expenses as $key_expenses => $value_expenses) {
                    
                    if($value['id'] == $value_expenses['origenes_gastos_id']){

                        $expenses_month[$value_expenses['mes'] - 1] = floatval($value_expenses['total']);

                    }

                $origin_expenses[$key]['data'] = $expenses_month;

                }
              

            }
           
                $data = array('status'=>true,'data'=>$origin_expenses);
        }

        $this->response($data);

    }

    function graph_days_get()
    {
        $list   = false;
        $data   = array();

        $idUser = $this->get('idUser');

        if($idUser && is_numeric($idUser)){
      
            $expenses = $this->Expenses_Model->get_graph_days($idUser);

            $this->load->model('Origins_Expenses_Model');
            $origin_expenses = $this->Origins_Expenses_Model->get_data();

            $daysMonth = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
           
            foreach ($origin_expenses as $key => $value) {

                 $expenses_days = array_fill(0, $daysMonth, 0);

                foreach ($expenses as $key_expenses => $value_expenses) {

                    if($value['id'] == $value_expenses['origenes_gastos_id']){

                        $expenses_days[$value_expenses['dia'] - 1] = floatval($value_expenses['total']);

                    }

                }

                $origin_expenses[$key]['data'] = $expenses_days;

            }
            
            $values = array_combine(range(0,$daysMonth-1),range(1,$daysMonth));
            $data = array('status'=>true,'data'=>$origin_expenses, 'values'=>$values);
        }

        $this->response($data);

    }

    function data_delete()
    {
        $data   = array();
        $error  = '';

        $idExpenses   = $this->delete('idExpenses');
        if($idExpenses && is_numeric($idExpenses)){
            $data = $this->Expenses_Model->get_expenses_unic($idExpenses);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
        }

        $delete = $this->Expenses_Model->delete_expenses($idExpenses);

                if($delete)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not delete data from database'));     

        $this->response($data);
    }
    
}
