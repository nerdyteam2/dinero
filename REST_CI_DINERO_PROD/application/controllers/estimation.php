<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

if (! function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( !array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( !array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}

class Estimation extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Estimation_Model');
	}
     
    function data_put()
    {       
        $data  = array();
        $error = '';

        $monto    = $this->put('monto');
        $idCat    = $this->put('idCat');
        $idSubCat = $this->put('idSubCat');
        $idUser   = $this->put('idUser');

        $monto = str_replace(",", "", $monto);
        //echo $monto;exit;
        //Validar los datos 
       
        if(!$idUser && is_numeric($idUser))
            $error = 'id user incorrect';
        if(!$idCat && is_numeric($idCat))
            $error = 'id Category incorrect';
        if(!$idSubCat && is_numeric($idSubCat))
            $error = 'id Sub Category incorrect';
        if(!$monto)
            $error = 'Monto incorrecto';

        $monto = str_replace(",", "", $monto);

        if($error=='')
        	{
        		$insert = $this->Estimation_Model->put_estimation($monto,$idUser,$idCat,$idSubCat);

        		if($insert)
        			$this->response(array('status'=>true));
        		else
        			$this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database'));		
        	}
        else
        	$this->response(array('status'=>false,'error'=>$error));
    }

    function table_get(){

        $list   = false;
        $idUser = $this->get('idUser');
        $anio   = $this->get('anio');
        $mes    = $this->get('mes');
        $data   = array();

        if($idUser && is_numeric($idUser)){

            $anio = (!$anio || $anio == 0) ? date('Y') : $anio;
            $mes  = (!$mes  || $mes  == 0) ? date('n') : $mes;

            $this->load->model('Category_Model');
            $cat = $this->Category_Model->get_categories_list($idUser);

            $estimation = $this->Estimation_Model->get_estimation_table($idUser);

            $this->load->model('Expenses_Model');
            $expenses = $this->Expenses_Model->get_expenses_table($idUser,$mes,$anio);

            $total_pre   = 0;
            $total_gasto = 0;

            foreach ($cat as $key => $value) {
                foreach ($estimation as $keyEst => $valueEst) {
                    if ($valueEst['categorias_id'] == $value['id']){
                        $cat[$key]['presupuesto'] = $valueEst['total'];
                        $total_pre += $valueEst['total'];
                    }
                }

                foreach ($expenses as $keyExp => $valueExp) {
                    if ($valueExp['categorias_id'] == $value['id']){
                        $cat[$key]['gasto'] = $valueExp['total'];
                        $total_gasto += $valueExp['total'];
                    }
                }
            }
           
            $data = array('status'=>true,'total_pre'=>$total_pre,'total_gasto'=>$total_gasto,'data'=>$cat);

        } else {

            $data = array('status'=>false,'error'=>'error');
        }

        $this->response($data);


    }

    function graph_get(){

        $idUser = $this->get('idUser');
        $anio   = $this->get('anio');
        $mes    = $this->get('mes');
        $data   = array();

        if($idUser && is_numeric($idUser)){

            $anio = (!$anio || $anio == 0) ? date('Y') : $anio;
            $mes  = (!$mes  || $mes  == 0) ? date('n') : $mes;

            $this->load->model('Category_Model');
            $categories = $this->Category_Model->get_only_categories_list($idUser);
            
            $this->load->model('Expenses_Model');
            $expenses = $this->Expenses_Model->get_gastos_vs_presupuesto($idUser,$anio,$mes);

            $estimation = $this->Estimation_Model->get_presupuesto_vs_gastos($idUser);

            $categorieas = array();
            $presupuesto = array();
            $gasto       = array();
            $saldo       = array();

            foreach ($categories as $key => $category) {

               $key_estimation = false;
               $key_expenses   = false;

               $key_estimation = array_search($category['id'], array_column($estimation, 'categorias_id'));
               $key_expenses   = array_search($category['id'], array_column($expenses, 'categorias_id'));

               if($key_estimation !== false || $key_expenses !== false){
                    
                if(strlen($category['nombre']) > 7 )
                    array_push($categorieas, substr($category['nombre'], 0, 7).'...');
                else
                    array_push($categorieas, $category['nombre']);

                    array_push($presupuesto, ($key_estimation !== false) ? floatval($estimation[$key_estimation]['total']) : 0 ) ;
                    array_push($gasto, ($key_expenses !== false) ? floatval($expenses[$key_expenses]['total']): 0 ) ;
                    
                    if($key_estimation !== false && $key_expenses !== false)
                        $saldo_ = $estimation[$key_estimation]['total'] - $expenses[$key_expenses]['total'];
                    else if ($key_estimation == false && $key_expenses !== false)
                        $saldo_ = 0;
                    else if ($key_estimation !== false && $key_expenses == false)
                        $saldo_ = $estimation[$key_estimation]['total'];
                        
                    array_push( $saldo, (int)$saldo_ );

                }

            }

            $data = array('status'=>true,'categorias'=>$categorieas, 'presupuesto' => $presupuesto, 'gasto' => $gasto,  'saldo' => $saldo);

        } else {

            $data = array('status'=>false,'error'=>'error');
        }

        $this->response($data);

    }

    function graph_anual_get(){

        $idUser = $this->get('idUser');
        $anio   = $this->get('anio');
        $data   = array();
        $expenses_month = array();
        $saldo_month = array();

        if($idUser && is_numeric($idUser)){

            $anio = (!$anio || $anio == 0) ? date('Y') : $anio;

            $this->load->model('Expenses_Model');
            $expenses = $this->Expenses_Model->get_gastos_vs_presupuesto_anual($idUser,$anio);

            $estimation = $this->Estimation_Model->total($idUser);
            $estimation = intval ($estimation[0]['total']);

            $estimation_month = array($estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation);

            foreach ($expenses as $key => $value) {

               $expenses_month = array(0,0,0,0,0,0,0,0,0,0,0,0);
               $expenses_month[$value['mes'] - 1] = floatval($value['total']);
               $saldo_month = array($estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation,$estimation);
               $saldo = $estimation - $value['total'];
               $saldo_month[$value['mes'] - 1] = floatval($saldo);

            }
            
            $data = array('status'=>true, 'presupuesto' => $estimation_month, 'gasto' => $expenses_month, 'saldo' => $saldo_month);

        } else {

            $data = array('status'=>false,'error'=>'error');
        }

        $this->response($data);

    }

    function ahorraste_get(){

        $list = false;
        $idUser   = $this->get('idUser');
        $data = array();

        if($idUser && is_numeric($idUser)){

           $estimation = $this->Estimation_Model->get_estimation_ahorraste($idUser);
          
            $this->load->model('Expenses_Model');
            $expenses = $this->Expenses_Model->get_expenses_ahorraste($idUser,date('n'),date('Y'));

            $total_pre   = $estimation[0]['total'];
            $total_gasto = $expenses[0]['total'];
           
            $total =  $total_pre - $total_gasto;

            $data = array('status'=>true,'total'=>$total);

        } else {

            $data = array('status'=>false,'error'=>'error');
        }

         $this->response($data);

    }

    function total_get()
    {
        $list   = false;
        $idUser = $this->get('idUser');
        $data   = array();

        if($idUser && is_numeric($idUser)){
            //Peticion especifica
            $data = $this->Estimation_Model->total($idUser);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
            else
                $data = array('status'=>true,'data'=>$data);
        }
        
        $this->response($data);
    }

    function ahorrado_get()
    {
        $list   = false;
        $idUser = $this->get('idUser');
        $total  = array();
        $data   = array();

        if($idUser && is_numeric($idUser)){
            //Peticion especifica
            $data_g = $this->Estimation_Model->get_gastos($idUser);
            $data_i = $this->Estimation_Model->get_ingresos($idUser);

            $total[0]['total'] = floatval($data_i[0]['total']) - floatval($data_g[0]['total']);

            $data = array('status'=>true,'data'=>$total);
        }
        
        $this->response($data);
    }

    function years_get()
        {
            $idUser = $this->get('idUser');

            if($idUser && is_numeric($idUser)){
                $years = $this->Estimation_Model->get_estimation_years($idUser);

                if($years)
                    $this->response(array('status'=>true,'years'=>$years));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not connect to database'));

            }

            $this->response(array('status'=>false));
        }
    
}
