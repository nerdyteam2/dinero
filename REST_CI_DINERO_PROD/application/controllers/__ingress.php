<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Ingress extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Ingress_Model');
	}

    function data_put()
    {

        $data  = array();
        $error = '';

        $idUser   = $this->put('idUser');
        $monto    = $this->put('monto');
        $idOrigen = $this->put('idOrigen');
        $fecha    = $this->put('date');

        if(!$idUser && is_numeric($idUser))
            $error = 'id user incorrect';
        if(!$idOrigen && is_numeric($idOrigen))
            $error = 'Debes ingresar todos los datos';
        if(!$fecha)
            $error = 'Debes ingresar todos los datos';
        if(!$monto)
            $error = 'Debes ingresar todos los datos';

        $monto = str_replace(",", "", $monto);
        
        if($error=='')
            {

                $insert = $this->Ingress_Model->put_ingress($idUser, $monto, $idOrigen, $fecha);

                if($insert)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database'));     
            }
        else
            $this->response(array('status'=>false,'error'=>$error));
    }

    function data_post()
    {       
        $data   = array();
        $update = array();
        $error  = '';

        $monto = $this->post('monto');
        $monto = str_replace(",", "", $monto);
        $id    = $this->post('idIngress');
        if($id && is_numeric($id)){
            //Peticion especifica
            $data = $this->Ingress_Model->get_ingress($id);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
        }

        //Validar los datos
        if(!$this->post('monto'))
            $error = 'Debe ingresar todos los datos';
        else
            $update['monto'] = $monto;
        
        if($error=='')
            {
                $update = $this->Ingress_Model->post_ingress($id,$update);

                if($update)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not update data into database'));     
            }
        else
            $this->response(array('status'=>false,'error'=>$error));


        $this->response($data);
    }

    function data_get()
    {
        $idUser    = $this->get('id');
        $idIngress = $this->get('idIngress');
        $data      = array();

        if($idUser && is_numeric($idUser)){

            if($idIngress && is_numeric($idIngress)){

                //Peticion especifica
                $data = $this->Ingress_Model->get_ingress($idIngress);
                if(count($data)==0)
                    $data = array('status'=>false,'error'=>'Without results for this criteria');
                else
                    $data = array('status'=>true,'data'=>$data);

            } else {

                //Peticion generica
                $data = $this->Ingress_Model->get_ingress_list($idUser);

                if(count($data)==0)
                    $data = array('status'=>false,'error'=>'Without results for this criteria');
                else
                    $data = array('status'=>true,'data'=>$data);

            }

        }

         $this->response($data);
    }

    function total_mes_get()
    {
        $list   = false;
        $idUser = $this->get('idUser');
        $data   = array();

        if($idUser && is_numeric($idUser)){
            //Peticion especifica
            $data = $this->Ingress_Model->get_ingress_month($idUser);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
            else
                $data = array('status'=>true,'data'=>$data);
        }
        
        $this->response($data);
    }

    function graph_get()
    {
        $list   = false;
        $idUser = $this->get('idUser');
        $anio = $this->get('anio');
        $data   = array();

        if($idUser && is_numeric($idUser)){
            
            $anio = (!$anio || $anio == 0) ? date('Y') : $anio;

            $ingress = $this->Ingress_Model->get_graph($idUser,$anio);

            $this->load->model('Origins_Model');
            $origin = $this->Origins_Model->get_data();

            foreach ($origin as $key => $value) {

                $ingress_month = array(0,0,0,0,0,0,0,0,0,0,0,0);

                foreach ($ingress as $key_ingress => $value_ingress) {
                    
                    if($value['id'] == $value_ingress['origenes_id']){

                        $ingress_month[$value_ingress['mes'] - 1] = floatval($value_ingress['total']);

                    }

                $origin[$key]['data'] =  $ingress_month;

                }
              

            }
           
            $data = array('status'=>true,'data'=>$origin);

        }
        
        $this->response($data);
    }


    function graph_days_get()
    {
        $list   = false;
        $data   = array();

        $idUser = $this->get('idUser');

        if($idUser && is_numeric($idUser)){
      
            $ingress = $this->Ingress_Model->get_graph_days($idUser);

            $this->load->model('Origins_Model');
            $origin = $this->Origins_Model->get_data();

            $daysMonth = cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y"));
           
            foreach ($origin as $key => $value) {

                 $ingress_days = array_fill(0, $daysMonth, 0);

                foreach ($ingress as $key_ingress => $value_ingress) {

                    if($value['id'] == $value_ingress['origenes_id']){

                        $ingress_days[$value_ingress['dia'] - 1] = floatval($value_ingress['total']);

                    }

                }

                $origin[$key]['data'] = $ingress_days;

            }

            $values = array_combine(range(0,$daysMonth-1),range(1,$daysMonth));
            $data = array('status'=>true,'data'=>$origin, 'values'=>$values);
        }

        $this->response($data);

    }

    function data_delete()
    {
        $data   = array();
        $error  = '';

        $idIngress   = $this->delete('idIngress');
        if($idIngress && is_numeric($idIngress)){
            $data = $this->Ingress_Model->get_ingress($idIngress);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
        }

        $delete = $this->Ingress_Model->delete_ingress($idIngress);

                if($delete)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not delete data from database'));     

        $this->response($data);
    }
    
    
}
