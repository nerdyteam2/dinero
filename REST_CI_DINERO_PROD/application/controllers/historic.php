<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Historic extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Historic_Model');
	}

    

    function data_get()
    {
        $list   = false;
        $years   = array();

        $idUser = $this->get('idUser');

            $years_ingress  = $this->Historic_Model->get_years_ingress($idUser);
            $years_expenses = $this->Historic_Model->get_years_expenses($idUser);

            if (count($years_expenses) > count($years_ingress))
                $data = array('status'=>true,'data'=>$years_expenses);
            else
                $data = array('status'=>true,'data'=>$years_ingress);
        
        $this->response($data);
    }
    
}
