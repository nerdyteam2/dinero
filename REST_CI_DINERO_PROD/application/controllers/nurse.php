<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Nurse extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Nurse_Model');
	}

	function data_get()
    {
    	$list = false;
    	$id   = $this->get('id');
    	$data = array();

    	if($id && is_numeric($id)){
    		//Peticion especifica
    		$data = $this->Nurse_Model->get_nurse($id);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this criteria');
            else
                $data = array('status'=>true,'data'=>$data);
    	}
    	else
    	{
    		//Peticion generica
    		$limit  = 10;
    		$offset = 0;

    		if($this->get('limit') && is_numeric($this->get('limit')))
    			$limit = $this->get('limit');
    		if($this->get('offset') && is_numeric($this->get('offset')))
    			$offset = $this->get('offset');

    		$data = $this->Nurse_Model->get_nurses_list($limit,$offset);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this request');
    	}

        //$data = array('returned: '. $this->get('id'));
        $this->response($data);
    }
     
    function data_put()
    {       
        $data = array();
        $error = '';

        $first_name 	= $this->put('n_first_name');
        $last_name		= $this->put('n_last_name');
        $email			= $this->put('n_email');
        $phone		    = $this->put('n_phone');


        //Validar los datos
        if(!$first_name || strlen($first_name)<3 || !is_string($first_name))
        	$error = 'first_name incorrect format';
        if(!$last_name || strlen($last_name)<3 || !is_string($last_name))
        	$error = 'last_name incorrect format';
        if(!$email || strlen($email)<3 || !is_string($email) || !(filter_var($email, FILTER_VALIDATE_EMAIL) 
        && preg_match('/@.+\./', $email)))
        	$error = 'email incorrect format';
        if(!$phone || strlen($phone)<5 )
        	$error = 'phone incorrect format';
       

        if($error=='')
        	{
        		$insert = $this->Nurse_Model->put_nurse($first_name,$last_name,$email,$phone);
                if($insert)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Invalid data for nurse')); 
            }
        else
        	$this->response(array('status'=>false,'error'=>$error));
    }
 
    function data_post()
    {       
        $data   = array();
        $update = array();
        $error  = '';

        $first_name 	= $this->post('n_first_name');
        $last_name		= $this->post('n_last_name');
        $email			= $this->post('n_email');
        $phone		    = $this->post('n_phone');
       
        $id   = $this->post('n_id');
        if($id && is_numeric($id)){
    		//Peticion especifica
    		$data = $this->Nurse_Model->get_nurse($id);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this criteria');
		}

        //Validar los datos
        if($this->post('n_first_name'))
        	if(strlen($first_name)<3 || !is_string($first_name))
        		$error = 'first_name incorrect format';
        	else
        		$update['n_first_name'] = $first_name;
        if($this->post('n_last_name'))
        	if(strlen($last_name)<3 || !is_string($last_name))
        		$error = 'last_name incorrect format';
        	else
        		$update['n_last_name'] = $last_name;
        if($this->post('n_email'))
        	if(strlen($email)<3 || !is_string($email) || !(filter_var($email, FILTER_VALIDATE_EMAIL) 
        	&& preg_match('/@.+\./', $email)))
        		$error = 'email incorrect format';
        	else
        		$update['n_email'] = $email;
        if($this->post('n_phone'))
        	if(strlen($phone)<5 )
        		$error = 'phone incorrect format';
        	else
        		$update['n_phone'] = $phone;
        

        if($error=='')
        	{
        		$update = $this->Nurse_Model->post_nurse($id,$update);

        		if($update)
        			$this->response(array('status'=>true));
        		else
        			$this->response(array('status'=>false,'error'=>'Unknown error, the system can not update data into database'));		
        	}
        else
        	$this->response(array('status'=>false,'error'=>$error));


        $this->response($data);
    }
 
    function data_delete()
    {
        $data   = array();
        $error  = '';

        $id   = $this->delete('n_id');
        if($id && is_numeric($id)){
    		$data = $this->Nurse_Model->get_nurse($id);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this criteria');
		}

		$delete = $this->Nurse_Model->delete_nurse($id);

        		if($delete)
        			$this->response(array('status'=>true));
        		else
        			$this->response(array('status'=>false,'error'=>'Unknown error, the system can not delete data from database'));		

        $this->response($data);
    }
	
	
}
