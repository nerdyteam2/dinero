<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Subcategory extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Subcategory_Model');
	}

	function data_get()
    {
    	$list   = false;
    	$idUser = $this->get('idUser');
        $idCat  = $this->get('idCat');
    	$data   = array();

    	if($idUser && is_numeric($idUser) && $idCat && is_numeric($idCat)){
    		//Peticion especifica
    		$data = $this->Subcategory_Model->get_subcategories_list($idUser,$idCat);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this criteria');
            else{
               $total = $data['total_cat'];
               unset($data['total_cat']);
                $data = array('status'=>true,'data'=>$data, 'total' => $total);
            }
    	}
    	/*else
    	{
    		//Peticion generica
    		$limit  = 10;
    		$offset = 0;

    		if($this->get('limit') && is_numeric($this->get('limit')))
    			$limit = $this->get('limit');
    		if($this->get('offset') && is_numeric($this->get('offset')))
    			$offset = $this->get('offset');

    		$data = $this->Category_Model->get_ctegories_list($limit,$offset);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this request');
    	}*/

        //$data = array('returned: '. $this->get('id'));
        $this->response($data);
    }

		function data_all_get()
	    {
	    	$list   = false;
	    	$idUser = $this->get('idUser');
	    	$data   = array();

	    	if($idUser && is_numeric($idUser)){
	    		//Peticion especifica
	    		$data = $this->Subcategory_Model->get_all_subcategories($idUser);
	    		if(count($data)==0)
	    			$data = array('status'=>false,'error'=>'Without results for this criteria');
	        else{
	          $data = array('status'=>true,'data'=>$data);
	        }
	    	}
				$this->response($data);
	    }

    function data_put()
    {
        $data  = array();
        $error = '';

        $nombre  = $this->put('nameSubCat');
        $idCat  = $this->put('idCat');
        $idUser  = $this->put('idUser');

        //Validar los datos
        if(!$nombre || strlen($nombre)<3 || !is_string($nombre))
        	$error = 'Subcategoria incorrecta';
        if(!$idUser && is_numeric($idUser))
            $error = 'id user incorrect';
        if(!$idCat && is_numeric($idCat))
            $error = 'id Category incorrect';


        if($error=='')
        	{
        		$insert = $this->Subcategory_Model->put_subcategory($nombre,$idUser,$idCat);

        		if($insert)
        			$this->response(array('status'=>true));
        		else
        			$this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database'));
        	}
        else
        	$this->response(array('status'=>false,'error'=>$error));
    }


    function data_delete()
    {
        $data   = array();
        $error  = '';

        $id   = $this->delete('idSubCat');

        $delete = $this->Subcategory_Model->delete_subcategory($id);

                if($delete)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not delete data from database'));

    }


}
