<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Origins extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Origins_Model');
	}

    

    function data_get()
    {
        $list   = false;
        $data   = array();

            //Peticion especifica
            $data = $this->Origins_Model->get_data();
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
            else
                $data = array('status'=>true,'data'=>$data);

        $this->response($data);
    }
    
}
