<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Patient extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Patient_Model');

        if($this->session->userdata('hc_session') != TRUE )
            $this->response(array('status'=>false,'error'=>'Without session started'));
	}

	function data_get()
    {
    	$list = false;
    	$id   = $this->get('id');
    	$data = array();

    	if($id && is_numeric($id)){
    		//Peticion especifica
    		$data = $this->Patient_Model->get_patient($id);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this criteria');
    	}
    	else
    	{
    		//Peticion generica
    		$limit  = 10;
    		$offset = 0;
            $user   = $this->get('user');

    		if($this->get('limit') && is_numeric($this->get('limit')))
    			$limit = $this->get('limit');
    		if($this->get('offset') && is_numeric($this->get('offset')))
    			$offset = $this->get('offset');

            if($this->get('user'))
    		   $data = $this->Patient_Model->get_patients_list($limit,$offset,$user);
            else
               $data = $this->Patient_Model->get_patients_list($limit,$offset);


    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this request');
            else
                $data = array('status'=>true,'data'=>$data);
    	}

        //$data = array('returned: '. $this->get('id'));
        $this->response($data);
    }
     
    function data_put()
    {       
        $data = array();
        $error = '';

        $user_own 	     = $this->put('user_own');
        $p_first_name	 = $this->put('p_first_name');
        $p_last_name	 = $this->put('p_last_name');
        $p_location		 = $this->put('p_location');
        $p_state		 = $this->put('p_state');
        $p_state2        = $this->put('p_state2');
        $p_street1	     = $this->put('p_street1');
        $p_street2		 = $this->put('p_street2');
        $p_city          = $this->put('p_city');
        $p_zip           = $this->put('p_zip');
        $p_date   = $this->put('p_date');
        $p_notes         = $this->put('p_notes');
        $p_contact_name     = $this->put('p_contact_name');
        $p_contact_relation = $this->put('p_contact_relation');
        $p_cellphone        = $this->put('p_cellphone');
        $p_homephone        = $this->put('p_homephone');
	$p_date        	    = $this->put('p_date');
	$p_condition  	    = $this->put('p_condition');
        $p_nurse_asigned    = $this->put('p_nurse_asigned');

        //Validar los datos
        if(!$user_own)
            $error = "Bad request not own";
        if(!$p_first_name)
            $error = "Bad request: firstname";
        if(!$p_last_name)
            $error = "Bad request: lastname";
        if(!$p_location)
            $error = "Bad request: location";
        if(!$p_state)
            $error = "Bad request: state";
        if(!$p_state2)
            $error = "Bad request: state2";
        //if(!$p_street1)
        //    $error = "Bad request: street1";
        //if(!$p_street2)
        //    $error = "Bad request: street2";
        //if(!$p_city)
        //    $error = "Bad request: city";
        if(!$p_zip)
            $error = "Bad request: zip code";
        //if(!$p_discharge_date)
        //    $update['p_discharge_date'] = $p_discharge_date;
        //if(!$p_notes)
        //    $update['p_notes'] = $p_notes;
        if(!$p_contact_name)
            $error = "Bad request: contact name";
        if(!$p_contact_relation)
            $error = "Bad request: contact relation";
        if(!$p_cellphone)
            $error = "Bad request: cell phone";
        if(!$p_homephone)
            $error = "Bad request: home phone";
	if(!$p_date)
	    $p_date = date('Y-m-d');
	//if(!$p_condition)
    //        $error = "Bad request: p_condition";


	$p_date = str_replace('-','/',$p_date);

        //Validar los datos

        if($error=='')
        	{
        		$insert = $this->Patient_Model->put_patient($user_own,$p_first_name,$p_last_name,$p_location,$p_state,$p_state2,$p_street1,$p_street2,$p_city,$p_zip,$p_date,$p_notes,$p_contact_name,$p_contact_relation,$p_cellphone,$p_homephone,$p_date,$p_condition);

        		if($insert)
        			$this->response(array('status'=>true));
        		else
        			$this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database' ));		
        	}
        else
        	$this->response(array('status'=>false,'error'=>$error));
    }
 
    function data_post()
    {       
        $data   = array();
        $update = array();
        $error  = '';

        $user_own        = $this->post('user_own');
        $p_first_name    = $this->post('p_first_name');
        $p_last_name     = $this->post('p_last_name');
        $p_location      = $this->post('p_location');
        $p_state         = $this->post('p_state');
        $p_state2        = $this->post('p_state2');
        $p_street1       = $this->post('p_street1');
        $p_street2       = $this->post('p_street2');
        $p_city          = $this->post('p_city');
        $p_zip           = $this->post('p_zip');
        $p_date   = $this->post('p_date');
        $p_notes         = $this->post('p_notes');
        $p_contact_name     = $this->post('p_contact_name');
        $p_contact_relation = $this->post('p_contact_relation');
        $p_cellphone        = $this->post('p_cellphone');
        $p_homephone        = $this->post('p_homephone');
	$p_date		    = $this->post('p_date');
	$p_condition	    = $this->post('p_condition');

        $p_nurse_asigned    = $this->post('p_nurse_asigned');

        $id   = $this->post('p_id');
        if($id && is_numeric($id)){
    		//Peticion especifica
    		$data = $this->Patient_Model->get_patient($id);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this criteria');
		}

        //Validar los datos
        if($user_own)
          	$update['user_own'] = $user_own;
        if($p_first_name)
            $update['p_first_name'] = $p_first_name;
        if($p_last_name)
            $update['p_last_name'] = $p_last_name;
        if($p_location)
            $update['p_location'] = $p_location;
        if($p_state)
            $update['p_state'] = $p_state;
        if($p_state2)
            $update['p_state2'] = $p_state2;
        //if($p_street1)
            $update['p_street1'] = $p_street1;
        //if($p_street2)
            $update['p_street2'] = $p_street2;
        //if($p_city)
            $update['p_city'] = $p_city;
        if($p_zip)
            $update['p_zip'] = $p_zip;
        if($p_date)
            $update['p_date'] = $p_date;
        //if($p_notes)
            $update['p_notes'] = $p_notes;
        if($p_contact_name)
            $update['p_contact_name'] = $p_contact_name;
        if($p_contact_relation)
            $update['p_contact_relation'] = $p_contact_relation;
        if($p_cellphone)
            $update['p_cellphone'] = $p_cellphone;
        if($p_homephone)
            $update['p_homephone'] = $p_homephone;

        if($p_nurse_asigned)
            $update['p_nurse_asigned'] = $p_nurse_asigned;
	if($p_date)
            $update['p_date'] = $p_date;
	if($p_condition)
            $update['p_condition'] = $p_condition;

	$p_date = str_replace('-','/',$p_date);

        if($error=='')
        	{
        		$update = $this->Patient_Model->post_patient($id,$update);

        		if($update)
        			$this->response(array('status'=>true));
        		else
        			$this->response(array('status'=>false,'error'=>'Unknown error, the system can not update data into database '));		
        	}
        else
        	$this->response(array('status'=>false,'error'=>$error));


        $this->response($data);
    }
 
    function data_delete()
    {
        $data   = array();
        $error  = '';

        $id   = $this->delete('id');
        if($id && is_numeric($id)){
    		$data = $this->Patient_Model->get_patient($id);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this criteria');
		}

		$delete = $this->Patient_Model->delete_patient($id);

        		if($delete)
        			$this->response(array('status'=>true));
        		else
        			$this->response(array('status'=>false,'error'=>'Unknown error, the system can not delete data from database'));		

        $this->response($data);
    }
	
	
}
