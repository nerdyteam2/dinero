<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class User extends REST_Controller  {


    function __construct(){
        parent::__construct();
        $this->load->model('User_Model');
    }

    function step_post()
    {

        $data  = array();
        $error = '';

        $idUser   = $this->post('idUser');  
        $nStep    = $this->post('nStep');    
         
         if(!$idUser && is_numeric($idUser))
            $error = 'id user incorrect';
        if(!$nStep && is_numeric($nStep))
            $error = 'Step incorrect';

        if($error=='')
            {
                $update = $this->User_Model->post_paso($idUser,$nStep);
                
                    $this->response(array('status'=>true));
            }
        else
            $this->response(array('status'=>false,'error'=>$error));
    }
    function isregistered_get(){
        $mail    = $this->get('email');
        $user    = $this->User_Model->get_user_email($mail);
        $this->response(array('user'=>$user));
    }
    function login_get(){
        $user    = $this->get('user');
        $pass    = $this->get('pass');
        //$aviso   = $this->get('aviso');
        $error   = '';


        if(!$user)
            $error = 'email incorrect format';
        if(!$pass || strlen($pass) < 5 )
            $error = 'pass incorrect format';
        /*if(!$aviso)
            $error = 'Debe leer y aceptar el Aviso de privacidad';*/


        if($error=='')
            {
                $login = $this->User_Model->login_user($user,$pass);
                // $user = $this->rest->get('user', array('id' => $id), 'json');
                if($login)
                    {
                        $this->session->set_userdata('dnr_session', TRUE);
                        $this->response(array('status'=>true,'data'=>$login));
                    }
                else
                    $this->response(array('status'=>false,'error'=>'Usuario o password incorrectos. Por favor intente de nuevo'));      
            }
        else
            $this->response(array('status'=>false,'error'=>$error));
        
        
    }

   /* function recover_get(){
        $email   = $this->get('email');
        $data = array();

        if($email){
            if((filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email))){
                $data = $this->User_Model->get_userbyemail($email);
                if($data){
                    $data = $data[0];
                    $this->load->library('email');
                    $this->email->from('', 'HomeCare');
                    $this->email->to($email); 
                    $this->email->subject('Alpine: recovery password');
                    $this->email->message('Your password is: '.$data['pass']);  

                    $this->email->send();
                    //echo $this->email->print_debugger();
                    $this->response(array('status'=>true,'message'=>'Notification send'));
                }
                else{
                    $this->response(array('status'=>false,'error'=>'User is not registered'));
                }
            }
            else{
                $this->response(array('status'=>false,'error'=>'Email is required, bad format'));
            }
        }
        else{
            $this->response(array('status'=>false,'error'=>'Email is required'));
        }

    }*/

     function recover_get(){
        $email   = $this->get('email');
        $data = array();

        if($email){
            if((filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email))){
                $data = $this->User_Model->get_userbyemail($email);
                if($data){

                    $data = $data[0];
                    $new_pass = $this->generateRandomString();
                    if($this->change_pass($data['id'],md5($new_pass))){
                       //$this->response(array('status'=>true,'message'=>'Notification send','pass'=>$new_pass));
                        
                        $this->load->library('email');

                        $configMail = array(
                            'protocol'  => 'smtp',
                            'smtp_host' => 'ssl://gator3235.hostgator.com',
                            'smtp_port' => 465,
                            'smtp_user' => 'app@reconfiguracionfinanciera.com',
                            'smtp_pass' => 'Abundantia',
                            'mailtype'  => 'html',
                            'charset'   => 'utf-8',
                            'newline'   => "\r\n"
                        );    
                        $this->email->initialize($configMail);
                        $this->email->from('app@reconfiguracionfinanciera.com','Presupuesto Mensual');
                        $this->email->to($email); 
                        $this->email->subject('Presupuesto Mensual: recuperar contraseña');
                        $this->email->message('Tu usuario es: '.$data['nombres'].' y tu nueva contraseña es: '.$new_pass.'. Por favor cambiela una vez que se haya logeado.');  

                        $this->email->send();
                        $this->response(array('status'=>true,'message'=>$this->email->print_debugger(),'pass'=>$new_pass));
                    }
                }
                else{
                    $this->response(array('status'=>false,'error'=>'Usuario no registrado'));
                }
            }
            else{
                $this->response(array('status'=>false,'error'=>'Email es incorrecto'));
            }
        }
        else{
            $this->response(array('status'=>false,'error'=>'Email es requerido'));
        }

    }

    function pass_post()
    {
        $id      = $this->post('id');
        $pass    = $this->post('pass');
        $passc   = $this->post('passc');
        $data    = array();
        $error   = '';

        if(!$id && !is_numeric($id))
            $error = 'user incorrect format';
        if(!$pass || strlen($pass) < 5 || $pass == "d41d8cd98f00b204e9800998ecf8427e"){
            $error = 'contraseña incorrecta';
        } else {
        if($pass != $passc )
            $error = 'Las contraseñas no coinciden';
        }

        if($error==''){

        $changePass = $this->change_pass($id,$pass);

            if($changePass){
                    $this->response(array('status'=>true,'message'=>'La contraseña ha sido actualizada'));
                
            } else {

                $this->response(array('status'=>false));
            }

        } else {
             $this->response(array('status'=>false, 'error'=>$error));
        } 
           

    }

    function data_get()
    {
        $list = false;
        $id   = $this->get('id');
        $data = array();

        if($id && is_numeric($id)){
            //Peticion especifica
            $data = $this->User_Model->get_user($id);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
            else
                $data = array('status'=>true,'data'=>$data);
        }
        else
        {
            //Peticion generica
            $limit  = 10;
            $offset = 0;

            if($this->get('limit') && is_numeric($this->get('limit')))
                $limit = $this->get('limit');
            if($this->get('offset') && is_numeric($this->get('offset')))
                $offset = $this->get('offset');

            $data = $this->User_Model->get_users_list($limit,$offset);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this request');
        }

        //$data = array('returned: '. $this->get('id'));
        $this->response($data);
    }
     
    function data_put()
    {       
        $data = array();
        $error = '';

        $nombres  = $this->put('nombres');
        //$apellido_paterno = $this->put('last_name');
        //$apellido_materno = $this->put('last_name');
        $confirm  = $this->generateRandomString();
        $email    = $this->put('email');
        $password = $this->put('pass');
        $passc    = $this->put('passc');
        $aviso    = $this->put('aviso');

        //Validar los datos
        if(!$nombres || strlen($nombres)<3 || !is_string($nombres))
            $error = 'Debes ingresar todos los datos';
       /* if(!$last_name || strlen($last_name)<3 || !is_string($last_name))
            $error = 'last_name incorrect format';*/
        if(!$email || strlen($email)<3 || !is_string($email) || !(filter_var($email, FILTER_VALIDATE_EMAIL) 
        && preg_match('/@.+\./', $email)))
            $error = 'Debes ingresar todos los datos';
        if(!$password || strlen($password) < 5 || $password == "d41d8cd98f00b204e9800998ecf8427e"){
            $error = 'Debes ingresar todos los datos';
        } else {
        if($password != $passc )
            $error = 'Las contraseñas no coinciden';
        }
        if(!$aviso)
            $error = 'Debe leer y aceptar el Aviso de privacidad';
        
        if($error=='')
            {
                $insert = $this->User_Model->put_user($nombres,$email,$password, $confirm);

                if($insert)
                    {

                        $this->load->library('email');

                        $configMail = array(
                            'protocol'  => 'smtp',
                            'smtp_host' => 'ssl://gator3235.hostgator.com',
                            'smtp_port' => 465,
                            'smtp_user' => 'app@reconfiguracionfinanciera.com',
                            'smtp_pass' => 'Abundantia',
                            'mailtype'  => 'html',
                            'charset'   => 'utf-8',
                            'newline'   => "\r\n"
                        );    
                        $this->email->initialize($configMail);
                        $this->email->from('app@reconfiguracionfinanciera.com','Presupuesto Mensual');
                        $this->email->to($email); 
                        $this->email->subject('Presupuesto Mensual: Confimación de correo electrónico');  
                        $this->email->message('Da <a href ="http://api.reconfiguracionfinanciera.com.mx/rf_app/confirm-app-PM/?c='.$confirm.'" >click aquí</a> para confirmar tu correo');  

                        $this->email->send();
                        $this->response(array('status'=>true, 'message'=>$this->email->print_debugger()));

                    }
                else
                    $this->response(array('status'=>false,'error'=>'El email ya existe en la base de datos'));      
            }
        else
            $this->response(array('status'=>false,'error'=>$error));
    }
 
    function data_post()
    {       
        $data   = array();
        $update = array();
        $error  = '';

        $first_name     = $this->post('first_name');
        $last_name      = $this->post('last_name');
        $email          = $this->post('email');
        $company        = $this->post('company');
        $facility       = $this->post('facility');
        $phone_number   = $this->post('phone_number');
        $pass           = $this->post('pass');
        $platform       = $this->post('platform');

        $id   = $this->post('id');
        if($id && is_numeric($id)){
            //Peticion especifica
            $data = $this->User_Model->get_user($id);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
        }


        //Validar los datos

        if(!$this->post('platform'))
            $platform = 1;
        $update['platform'] = $platform;
        
        if($this->post('first_name'))
            if(strlen($first_name)<3 || !is_string($first_name))
                $error = 'first_name incorrect format';
            else
                $update['first_name'] = $first_name;
        if($this->post('last_name'))
            if(strlen($last_name)<3 || !is_string($last_name))
                $error = 'last_name incorrect format';
            else
                $update['last_name'] = $last_name;
        if($this->post('email'))
            if(strlen($email)<3 || !is_string($email) || !(filter_var($email, FILTER_VALIDATE_EMAIL) 
            && preg_match('/@.+\./', $email)))
                $error = 'email incorrect format';
            else
                $update['email'] = $email;
        if($this->post('company'))
            if(strlen($company)<2 )
                $error = 'company incorrect format';
            else
                $update['company'] = $company;
        if($this->post('facility'))
            if(strlen($facility)<2 )
                $error = 'facility incorrect format';
            else
                $update['facility'] = $facility;
        if($this->post('phone_number'))
            if(strlen($phone_number) < 5 )
                $error = 'phone_number incorrect format';
            else
                $update['phone_number'] = $phone_number;
        if($this->post('pass'))
            if(strlen($pass) < 5 )
                $error = 'pass incorrect format';
            else
                $update['pass'] = $pass;


        if($error=='')
            {
                $update = $this->User_Model->post_user($id,$update);

                if($update)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not update data into database'));     
            }
        else
            $this->response(array('status'=>false,'error'=>$error));


        $this->response($data);
    }
 
    function data_delete()
    {
        $data   = array();
        $error  = '';

        $id   = $this->delete('id');
        if($id && is_numeric($id)){
            $data = $this->User_Model->get_user($id);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
        }

        $delete = $this->User_Model->delete_user($id);

                if($delete)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not delete data from database'));     

        $this->response($data);
    }

    function limit_post()
    {
        $idUser  = $this->post('idUser');
        $limit    = $this->post('limit');
       
        $data    = array();
        $error   = '';

        if(!$idUser && !is_numeric($idUser))
            $error = 'user incorrect format';
        if(!$limit && !is_numeric($limit))
            $error = 'limit incorrect format';

        if($error==''){

            $update = $this->User_Model->post_limit($idUser,$limit);

            if($update)
                $this->response(array('status'=>true));
            else
                $this->response(array('status'=>false,'error'=>'Unknown error, the system can not update data into database'));     

        } else {
             $this->response(array('status'=>false, 'error'=>$error));
        } 
           

    }

     function limit_get()
    {
        $idUser  = $this->get('idUser');
       
        $data    = array();
        $error   = '';

        if(!$idUser && !is_numeric($idUser))
            $error = 'user incorrect format';

        if($error==''){

            $data = $this->User_Model->get_limit($idUser);

            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
            else{
                $data = $data[0];
                $this->response(array('status'=>true, 'data'=>$data));
            }

        } else {
             $this->response(array('status'=>false, 'error'=>$error));
        } 
           

    }

    private function change_pass($id,$pass){

        $data   = array();
        $update = array();
        $error  = '';

        if($pass)
            if(strlen($pass) < 5 )
                $error = 'pass incorrect format';


        if($error=='')
            {
                $update = $this->User_Model->post_change_pass($id,$pass);

                if($update)
                    return true;
                else
                   return false;
            }
        else
            return false;

    }

    private function generateRandomString($length = 5) {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
    
    
}
