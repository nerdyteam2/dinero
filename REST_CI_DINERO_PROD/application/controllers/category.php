<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Category extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Category_Model');
	}

	function data_get()
    {
    	$list = false;
    	$id   = $this->get('id');
    	$data = array();

    	if($id && is_numeric($id)){
    		//Peticion especifica
    		$data = $this->Category_Model->get_categories_list($id);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this criteria');
            else
                $data = array('status'=>true,'data'=>$data);
    	}
    	/*else
    	{
    		//Peticion generica
    		$limit  = 10;
    		$offset = 0;

    		if($this->get('limit') && is_numeric($this->get('limit')))
    			$limit = $this->get('limit');
    		if($this->get('offset') && is_numeric($this->get('offset')))
    			$offset = $this->get('offset');

    		$data = $this->Category_Model->get_ctegories_list($limit,$offset);
    		if(count($data)==0)
    			$data = array('status'=>false,'error'=>'Without results for this request');
    	}*/

        //$data = array('returned: '. $this->get('id'));
        $this->response($data);
    }
     
    function data_put()
    {       
        $data  = array();
        $error = '';

        $nombre  = $this->put('nameCat');
        $idUser  = $this->put('idUser');

        //Validar los datos
        if(!$nombre || strlen($nombre)<3 || !is_string($nombre))
        	$error = 'Categoria incorrecta';
        if(!$idUser && is_numeric($idUser))
            $error = 'id user incorrect';
       

        if($error=='')
        	{
        		$insert = $this->Category_Model->put_category($nombre,$idUser);

        		if($insert)
        			$this->response(array('status'=>true));
        		else
        			$this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database'));		
        	}
        else
        	$this->response(array('status'=>false,'error'=>$error));
    }
 

    function data_delete()
    {
        $data   = array();
        $error  = '';

        $id   = $this->delete('idCat');

		$delete = $this->Category_Model->delete_category($id);

        		if($delete)
        			$this->response(array('status'=>true));
        		else
        			$this->response(array('status'=>false,'error'=>'Unknown error, the system can not delete data from database'));		

    }
	
	
}
