<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Alarm extends REST_Controller  {


	function __construct(){
		parent::__construct();
		$this->load->model('Alarm_Model');
	}

    function data_put()
    {

        $data  = array();
        $error = '';

        $idUser      = $this->put('idUser');
        $nombre      = $this->put('nombre');
        $descripcion = $this->put('descripcion');
        $date        = $this->put('date');
        $time        = $this->put('time');
        $repetir     = $this->put('repetir');

        if(!$nombre)
            $error = 'Debes ingresar todos los datos';
        if(!$descripcion)
            $error = 'Debes ingresar todos los datos';
        if(!$date)
            $error = 'Debes ingresar todos los datos';
        if(!$time)
            $error = 'Debes ingresar todos los datos';

          $fecha = $date .' '. $time .':00'; 

        if($error=='')
            {

                $insert = $this->Alarm_Model->put_alarm($idUser, $nombre, $descripcion, $fecha, $repetir);

                if(!$insert === false)
                    $this->response(array('status'=>true, 'idAlarma' => $insert));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not insert data into database'));     
            }
        else
            $this->response(array('status'=>false,'error'=>$error));
    }

    function data_get()
    {
        $list = false;
        $id   = $this->get('idAlarma');
        $data = array();

        if($id && is_numeric($id)){
            //Peticion especifica
            $data = $this->Alarm_Model->get_alarm($id);
            if(count($data)==0)    
                $data = array('status'=>false,'error'=>'Without results for this criteria');
            else{
                $data = $data[0];
                $data = array('status'=>true,'data'=>$data);
            }
        }
        else
        {
            //Peticion generica
            $idUser   = $this->get('idUser');

            $data = $this->Alarm_Model->get_alarms_list($idUser);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this request');
            else
                $data = array('status'=>true,'data'=>$data);
        }

        //$data = array('returned: '. $this->get('id'));
        $this->response($data);
    }

    /*function data_post()
    {       
        $data   = array();
        $update = array();
        $error  = '';

        $idUser   = $this->post('idUser');
        $idAlarma = $this->post('idAlarma');
        $estatus  = $this->post('estatus');

         if($estatus == 0){
                //    $data = $this->Alarm_Model->get_alarm($id);
                //    $data = $data[0];
                  //  $idUnic = $data['id_unico'];
                } 

        if($error=='')
            {   
                $update = $this->Alarm_Model->post_alarm($idUser,$idAlarma,$estatus);

                if($update)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not update data into database'));     
            }
        else
            $this->response(array('status'=>false,'error'=>$error));


        $this->response($data);
    }*/


    function data_delete()
    {
        $data   = array();
        $error  = '';

        $id   = $this->delete('idAlarma');

        $delete = $this->Alarm_Model->delete_alarm($id);

                if($delete)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Unknown error, the system can not delete data from database'));     

    }

    function data_post()
    {       
        $data   = array();
        $update = array();
        $error  = '';

        $idAlarma     = $this->post('idAlarma');
        $nombre       = $this->post('nombre');
        $descripcion  = $this->post('descripcion');
        $date         = $this->post('date');
        $time         = $this->post('time');
        $recurrente   = $this->post('repetir');
        $estatus      = $this->post('estatus');

        
       /* if($id && is_numeric($id)){
            //Peticion especifica
            $data = $this->Alarm_Model->get_user($id);
            if(count($data)==0)
                $data = array('status'=>false,'error'=>'Without results for this criteria');
        }*/


        //Validar los datos
        
        if($this->post('nombre'))
            if(!is_string($nombre))
                $error = 'nombre incorrect format';
            else
                $update['nombre'] = $nombre;

        if($this->post('descripcion'))
            if(!is_string($descripcion))
                $error = 'descripcion incorrect format';
            else
                $update['descripcion'] = $descripcion;
      
        if($this->post('date') && $this->post('time')){
                 $fecha = $date .' '. $time.':00'; 
                $update['fecha_inicio'] = $fecha;
        }

        if($this->post('repetir')) 
                $update['recurrente'] = $recurrente;

        if($this->post('estatus')!== null) 
                $update['estatus'] = $estatus;


        if($error=='')
            {
                $update = $this->Alarm_Model->post_alarm($idAlarma,$update);

                if($update)
                    $this->response(array('status'=>true));
                else
                    $this->response(array('status'=>false,'error'=>'Debe editar por lo menos un dato'));     
            }
        else
            $this->response(array('status'=>false,'error'=>$error));


        //$this->response($data);
    }
    
}
