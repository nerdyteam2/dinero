<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Reports extends REST_Controller  {

	function __construct(){
		parent::__construct();
		$this->load->model('Reports_Model');
	}
     
    function table_get(){

        $idUser = $this->get('idUser');
        $anio   = $this->get('repAnio');
        $mes    = $this->get('repMes');
        $data   = array();

        if($idUser && is_numeric($idUser)){

            $anio = (!$anio || $anio == 0) ? date('Y') : $anio;
            $mes  = (!$mes  || $mes  == 0) ? false : $mes;

            $reports  = $this->Reports_Model->reports_get($idUser, $anio, $mes);
            $gastos   = $this->Reports_Model->gastos_get($idUser, $anio, $mes);
            $ingresos = $this->Reports_Model->ingresos_get($idUser, $anio, $mes);

            $balance = @$ingresos->monto - @$gastos->monto;

            $data = array('status'=>true, 'data'=>$reports, 'balance'=>$balance);

        } else {

            $data = array('status'=>false,'error'=>'error');
        }

        $this->response($data);

    }

    public function excel_report_get()
    {

        $idUser = $this->get('idUser');
        $anio   = $this->get('repAnio');
        $mes    = $this->get('repMes');
        $data   = array();

        if($idUser && is_numeric($idUser)){

            $anio = (!$anio || $anio == 0) ? date('Y') : $anio;
            $mes  = (!$mes  || $mes  == 0) ? false : $mes;
            $type = ($mes)?"Mensual":"Anual";
            $file = APPPATH."uploads/".$idUser."_Reporte_".$type;

            if($mes)
                $file .= "_" . $this->getMES($mes) . "_" . $anio . ".xls";
            else
                $file .= "_" . $anio . ".xls";

            $this->load->library("excel");

            $this->excel->setActiveSheetIndex(0);

            $reports  = $this->Reports_Model->reports_get($idUser, $anio, $mes);
            $gastos   = $this->Reports_Model->gastos_get($idUser, $anio, $mes);
            $ingresos = $this->Reports_Model->ingresos_get($idUser, $anio, $mes);
            $email    = $this->Reports_Model->email_get($idUser);

            $balance = @$ingresos->monto - @$gastos->monto;

            if($mes)
                array_push($data, array('Reporte '.$type ,$this->getMES($mes).' - '. $anio));
            else
                array_push($data, array('Reporte '.$type , $anio));

            array_push($data, array('Fecha','Ingresos / Gastos', 'Origen', 'Categoría', 'Sub Categoría','Notas'));

            foreach ($reports as $key => $value) {
                array_push($data,array($value->fecha,"$".$this->formatMoney($value->monto, true),$value->origen,$value->nom_cat,$value->nom_subcat,$value->descripcion));                
            }
            
            array_push($data, array('Balance',"$".$this->formatMoney($balance,true)));

            $this->excel->getActiveSheet()->fromArray($data, null, 'A1');

            $this->excel->save($file);

            $this->send_mail($file,$type,$email);

        }
    
    }

    private function send_mail($file,$type,$email){

        $this->load->library('email');
        $this->load->helper('config_email');   

        $this->email->initialize(config_email());
        $this->email->from('app@reconfiguracionfinanciera.com','Reconfiguracion Financiera');
        $this->email->to($email); 
        $this->email->subject('Reconfiguración Financiera: Reporte '.$type);
        $this->email->message('Reporte '.$type); 
        $this->email->attach($file); 

        if($this->email->send())
            $data = array('status'=>true, 'message'=>'Reporte '.$type.' se envío correctamente');
        else
            $data = array('status'=>false, 'error'=>'error');

        unlink($file); 

        $this->response($data);

    }

    private function formatMoney($number, $fractional=false) { 
        if ($fractional) { 
            $number = sprintf('%.2f', $number); 
        } 
        while (true) { 
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number); 
            if ($replaced != $number) { 
                $number = $replaced; 
            } else { 
                break; 
            } 
        } 
        return $number; 
    } 

    private function getMes($nMES){
        $mes = array('ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC');
        return $mes[$nMES-1];
    }
    
}
