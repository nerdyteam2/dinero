<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Comparison extends REST_Controller  {


	function __construct(){
		parent::__construct();
	}

    function data_get()
    {
        $list   = false;
        $data   = array();

        $id   = $this->get('id');

            //Peticion especific
        $this->load->model('User_Model');
        $User = $this->User_Model->get_user($id);

        $this->load->model('Expenses_Model');
        $expenses_total = $this->Expenses_Model->get_expenses_month($id);

        $this->load->model('Estimation_Model');
        $estimation_total = $this->Estimation_Model->get_estimation($id);

        $porcentaje = ($User[0]['limite'] * $estimation_total[0]['total'])/100;

        if($expenses_total[0]['monto'] > $porcentaje)
            $value = false;
        else
            $value = true;
        
        $data = array('status'=>true,'data'=>$value);

        $this->response($data);
    }
    
}
