$(function() {

  $('.form').submit(function() {
    $(this).ajaxSubmit({

      // BEFORE SUBMIT THE LOADER WILL BE SHOWN
      beforeSubmit: function() {
        $('#loader').css('display', 'block');
      },

      // IF THE REQUEST WAS PROCESSED CORRECTLY
      success: function(data) {
        if(!data.success) {
          setTimeout( function() {
            $('#loader').css('display', 'none');
            $('.notification-text').addClass('alert-danger');
            $('.notification-text').html(data.errors);
            $('.notification').css('display', 'block');
            $('#user-password').val('');
          }, 3000);
        } else {
          setTimeout( function() {
            $('#loader').css('display', 'none');
            if(data.redirect)
              window.location.href = data.redirect;
          }, 3000);
        }
      },

      // IF THE REQUEST FAILS THEN ...
      error: function(data) {
        console.log(data.errors);
      }

    });
    return false;
  }); // .form

});
