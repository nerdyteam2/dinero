/*
 *  @file
 *  Script for taking control over the table
 *
 * * * * * * * * * * * * * * * * * * * * * * * */
$(function() {

  // DECLARE VARIABLES
  var current_url = $('#base_url').val();
  var $table = $("#tableUser");

  // FUNCTION WHICH RETURNS THE BASE_URL
  var base_url = function(url) {
    return current_url + ( url !== "" ? url : "" );
  }

  // TRIGGERS ON CLICK TO A ROW
  $('#tableUser').on('click-row.bs.table', function (e, row, $element) {
    location.href = base_url('User_Detail/get/' + row.id);
  });

  // SHOWS TOOLTIP OVER EACH ROW
  $('[data-toggle="tooltip"]').tooltip();

  $('[data-hover="tooltip"]').tooltip();

  // EXPORTING TOOLBAR
  $('#toolbar').find('select').change(function () {
      $table.bootstrapTable('refreshOptions', {
        exportDataType: $(this).val()
      });
    });

});
