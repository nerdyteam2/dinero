// $(function () {
$(document).ready(function() {

    // CHARTS FOR INS
    $('#highchart_ins_monthly').highcharts({
        title: {
            text: 'Gráfica de Ingresos Mensuales'
        },
        xAxis: {
            categories: window.dataIns_Monthly.fecha
        },
        yAxis: {
            title: {
                text: null
            }
        },
        colors: ['#67bc45'],
        series: [{
            name: 'Ingresos',
            data: window.dataIns_Monthly.monto
        }]
    });

    $('#highchart_ins_history').highcharts({
        title: {
            text: 'Gráfica de Ingresos - Histórico'
        },
        xAxis: {
            categories: window.dataIns_History.fecha
        },
        yAxis: {
            title: {
                text: null
            }
        },
        colors: ['#67bc45'],
        series: [{
            name: 'Ingresos',
            data: window.dataIns_History.monto
        }]
    });


    // CHARTS FOR OUTS
    $('#highchart_outs_monthly').highcharts({
        title: {
            text: 'Gráfica de Gastos Mensuales'
        },
        xAxis: {
            categories: window.dataOuts_Monthly.fecha
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            }
        },
        colors: ['#67bc45'],
        series: [{
            name: 'Gastos',
            data: window.dataOuts_Monthly.monto
        }]
    });

    $('#highchart_outs_history').highcharts({
        title: {
            text: 'Gráfica de Gastos - Histórico'
        },
        xAxis: {
            categories: window.dataOuts_History.fecha
        },
        yAxis: {
            title: {
                text: null
            }
        },
        colors: ['#67bc45'],
        series: [{
            name: 'Gastos',
            data: window.dataOuts_History.monto
        }]
    });

});
