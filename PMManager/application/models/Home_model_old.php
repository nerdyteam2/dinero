<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

	class Home_model extends CI_Model {

      private $tab_users = "dnrapp_usuarios";
      private $tab_ins = "dnrapp_ingresos";
      private $tab_outs = "dnrapp_gastos";

      function __construct() {
  			parent::__construct();
  			$this->load->database("default");
  			$this->load->helper('file');
      }

      public function get_all_info() {
        $this->db->select('id, nombres, email');
        $this->db->from($this->tab_users);
        $this->db->where('estatus', 1);
        $result = $this->db->get();
        $i = 0;
        foreach( $result->result() as $row ) {
          $data[$i]['id'] = $row->id;
          $data[$i]['name'] = $row->nombres;
          $data[$i]['email'] = $row->email;
          $data[$i]['rate_month'] = $this->get_rate_per_month($row->id);
          $data[$i]['rate_total'] = $this->get_total_rate($row->id);
          $i += 1;
        }
        return $data;
      }

      public function get_rate_per_month($uid) {
        $time = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

        $this->db->select('SUM(monto) AS imonto');
        $this->db->where('usuarios_id', $uid);
        $this->db->where('fecha >=', $time);
        $res1 = $this->db->get('dnrapp_ingresos');
        if( !empty($res1) )
          $imonto = $res1->result()[0]->imonto;
        else
          $imonto = 0;

        $this->db->select('SUM(monto) AS gmonto');
        $this->db->where('usuarios_id', $uid);
        $this->db->where('fecha >=', $time);
        $res2 = $this->db->get('dnrapp_gastos');
        if( !empty($res2) )
          $gmonto = $res2->result()[0]->gmonto;
        else
          $gmonto = 0;

        return $imonto - $gmonto;
      }

      public function get_total_rate($uid) {
        $this->db->select('SUM(monto) AS imonto');
        $this->db->where('usuarios_id', $uid);
        $res1 = $this->db->get($this->tab_ins);
        if( !empty($res1) )
          $imonto = $res1->result()[0]->imonto;
        else
          $imonto = 0;

        $this->db->select('SUM(monto) AS gmonto');
        $this->db->where('usuarios_id', $uid);
        $res2 = $this->db->get($this->tab_outs);
        if( !empty($res2) )
          $gmonto = $res2->result()[0]->gmonto;
        else
          $gmonto = 0;

        return $imonto - $gmonto;
      }

    }

/*
  -- QUERIES --

    -- USERS
    GET ALL THE USERS
        SELECT * FROM dnrapp_usuarios

    -- INS
    GET SUM OF THE INS OF THE LAST MONTH PER USER
        SELECT usuarios_id, SUM(monto), fecha
        FROM dnrapp_ingresos
        WHERE usuarios_id = 6
        AND (fecha >= '2015-05-04' AND fecha <= '2015-06-04');

    GET SUM OF ALL THE INS (TOTAL) PER USER
        SELECT usuarios_id, SUM(monto), fecha
        FROM dnrapp_ingresos
        WHERE usuarios_id = 6;

    -- OUTS
    GET SUM OF THE OUTS OF THE LAST MONTH PER USER
        SELECT usuarios_id, SUM(monto), fecha
        FROM dnrapp_gastos
        WHERE usuarios_id = 1
        AND (fecha >= '2015-05-04' AND fecha <= '2015-06-04');

    GET SUM OF ALL THE OUTS (TOTAL) PER USER
        SELECT usuarios_id, SUM(monto), fecha
        FROM dnrapp_gastos
        WHERE usuarios_id = 6;
*/

?>
