<?php if(! defined('BASEPATH')) exit('No tienes permiso para acceder a este archivo');

	class UserDetail_model extends CI_Model {

      private $tab_users = "dnrapp_usuarios";
      private $tab_ins = "dnrapp_ingresos";
      private $tab_outs = "dnrapp_gastos";

      function __construct() {
  			parent::__construct();
  			$this->load->database("default");
      }

      public function get_rate_per_month($uid) {
        $time = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

        $this->db->select('SUM(monto) AS imonto');
        $this->db->where('usuarios_id', $uid);
        $this->db->where('fecha >=', $time);
        $res1 = $this->db->get('dnrapp_ingresos');
        if( !empty($res1) )
          $imonto = $res1->result()[0]->imonto;
        else
          $imonto = 0;

        $this->db->select('SUM(monto) AS gmonto');
        $this->db->where('usuarios_id', $uid);
        $this->db->where('fecha >=', $time);
        $res2 = $this->db->get('dnrapp_gastos');
        if( !empty($res2) )
          $gmonto = $res2->result()[0]->gmonto;
        else
          $gmonto = 0;

        return $imonto - $gmonto;
      }

      public function get_total_rate($uid) {
        $this->db->select('SUM(monto) AS imonto');
        $this->db->where('usuarios_id', $uid);
        $res1 = $this->db->get($this->tab_ins);
        if( !empty($res1) )
          $imonto = $res1->result()[0]->imonto;
        else
          $imonto = 0;

        $this->db->select('SUM(monto) AS gmonto');
        $this->db->where('usuarios_id', $uid);
        $res2 = $this->db->get($this->tab_outs);
        if( !empty($res2) )
          $gmonto = $res2->result()[0]->gmonto;
        else
          $gmonto = 0;

        return $imonto - $gmonto;
      }

      public function get_id_exists($id) {
        $this->db->select('id');
        $this->db->where('id', $this->db->escape_str($id));
        $res = $this->db->get($this->tab_users);
        return $res->num_rows();
      }

      public function get_user_info($id) {
        $this->db->select('id, nombres, apellido_paterno, apellido_materno, email');
        $this->db->where('id', $id);
        $this->db->limit(1);
        $res = $this->db->get($this->tab_users);
        $i = 0;
        foreach( $res->result() as $row ) {
          $data[$i]['id'] = $row->id;
          $data[$i]['name'] = $row->nombres;
          $data[$i]['fullname'] = $row->nombres . ' ' . $row->apellido_paterno . ' ' . $row->apellido_materno;
          $data[$i]['email'] = $row->email;
          $data[$i]['rate_month'] = $this->get_rate_per_month($row->id);
          $data[$i]['rate_total'] = $this->get_total_rate($row->id);
          $i += 1;
        }
        return $data;
      }

      public function get_all_ins($id) {
        $this->db->select('monto, fecha');
        $this->db->where('usuarios_id', $id);
        return $this->db->get($this->tab_ins)->result();
      }

      public function get_all_outs($id) {
        $this->db->select('monto, fecha');
        $this->db->where('usuarios_id', $id);
        return $this->db->get($this->tab_outs)->result();
      }

      public function get_all_ins_per_month($id) {
        $this->db->select('monto, fecha');
        $this->db->where('usuarios_id', $id);
        $this->db->where('MONTH(fecha)', date('n'), FALSE);
				$this->db->order_by('fecha', 'asc');
        return $this->db->get($this->tab_ins)->result();
      }

      public function get_all_outs_per_month($id) {
        $this->db->select('monto, fecha');
        $this->db->where('usuarios_id', $id);
        $this->db->where('MONTH(fecha)', date('n'), FALSE);
        return $this->db->get($this->tab_outs)->result();
      }

			public function get_all_ins_history($id) {
				$this->db->select("monto, fecha");
				$this->db->where('usuarios_id',$id);
				$this->db->where('YEAR(fecha) >=', date('Y', strtotime(date('Y-m-d') . "-1 year")) , FALSE);
				$this->db->group_by('MONTH(fecha)');
				$this->db->order_by('fecha');
				return $this->db->get($this->tab_ins)->result();
			}

			public function get_all_outs_history($id) {
				$this->db->select("monto, fecha");
				$this->db->where('usuarios_id',$id);
				$this->db->where('YEAR(fecha) >=', date('Y', strtotime(date('Y-m-d') . "-1 year")) , FALSE);
				$this->db->group_by('MONTH(fecha)');
				$this->db->order_by('fecha');
				return $this->db->get($this->tab_outs)->result();
			}

      public function get_ins_per_time($id, $init, $end) {
        $this->db->select('SUM(monto) AS Sum');
        $this->db->where('usuarios_id', $id);
        $this->db->where('fecha >= ', $init);
        $this->db->where('fecha <= ', $end);
        return $this->db->get($this->tab_ins)->result();
      }

      public function get_outs_per_time($id, $init, $end) {
        $this->db->select('SUM(monto) AS Sum');
        $this->db->where('usuarios_id', $id);
        $this->db->where('fecha >= ', $init);
        $this->db->where('fecha <= ', $end);
        return $this->db->get($this->tab_outs)->result();
      }


      public function set_new_password($email,$password){
        $this->db->where('email',$email);
        $data = array('password' => md5($this->db->escape_str( $password )) );
        $this->db->update($this->tab_users, $data); 

        if ($this->db->affected_rows() > 0) return TRUE;
        else return FALSE;
      }

  }
