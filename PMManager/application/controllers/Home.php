<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  private $titlepage = "Presupuesto Mensual";
  private $subtitlepage = "";

	function __construct() {
		parent::__construct();

    // CHECK IF THERE IS AN ACTIVE SESSION
    if(!$this->session->userdata('session'))
      redirect('login');

    // LOAD MODULE(S)
		$this->load->model('Home_model', 'home');

	}

	public function index() {

		// GETTING DATA
    $this->subtitlepage = "Página Principal";

    // PREPARE DATA
    $dataHead = array(
      'title' => $this->titlepage,
      'subtitle' => $this->subtitlepage,
      'username' => $_SESSION['nombres'],
    );

		$dataBody = array(
			'users' => $this->home->get_all_info()
		);

		$dataFoot = array(
			'js' => array('table')
		);

		$this->load->view('header', $dataHead);
    $this->load->view('home', $dataBody);
    $this->load->view('footer', $dataFoot);

	}
}
