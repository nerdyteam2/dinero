<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  private $titlepage = "Presupuesto Mensual";
  private $subtitlepage = "";

  function __construct() {
		parent::__construct();

    // LOAD MODULE(S)
    $this->load->model('Login_model', 'loginModel');
	}

	public function index() {

    // CHECK IF THERE IS AN ACTIVE SESSION
    if($this->session->userdata('session') == TRUE )
      redirect('home');

    // GETTING DATA
    $this->subtitlepage = "Login";

    // PREPARE DATA
    $dataHead = array(
      'title' => $this->titlepage,
      'subtitle' => $this->subtitlepage,
      'css' => array('loader')
    );

    $dataFoot = array(
      'js' => array('form-ajax')
    );

		$this->load->view('header', $dataHead);
    $this->load->view('login');
    $this->load->view('footer', $dataFoot);

	}

  public function check() {
    if (!$this->input->is_ajax_request())
			return $this->output
					->set_content_type( 'application/json' )
					->set_output( json_encode( [ 'success' => false, 'errors' => '<span class="error">Entrada inválida</span>' ] ) );

		$this->form_validation->set_rules('user-email','Usuario','trim|required|valid_email|xss_clean');
		$this->form_validation->set_rules('user-password','Contraseña','trim|required|xss_clean');

		if ( $this->form_validation->run() == FALSE )
			return $this->output
					->set_content_type( 'application/json' )
					->set_output( json_encode( [ 'success' => false, 'errors' => validation_errors('<span class="error">','</span>') ] ) );

    		$data['email']		=	$this->input->post('user-email');
    		$data['pass']		=	$this->input->post('user-password');
        $data 				= 	$this->security->xss_clean($data);
        $login_check = $this->loginModel->check_login($data);

        if ( $login_check == FALSE )
        		return $this->output
					->set_content_type('application/json')
					->set_output( json_encode( [ 'success' => false, 'errors' => '<span class="error">Usuario o contraseña inválidos</span>' ] ) );

        	$this->session->set_userdata('session', TRUE);
        	$this->session->set_userdata('email', $data['email']);

        	if (is_array($login_check))
        		foreach ($login_check as $login_element) {
        			$this->session->set_userdata('id', $login_element->id);
    					$this->session->set_userdata('nombres', $login_element->nombres);
    					$this->session->set_userdata('email', $login_element->email);
              $this->session->set_userdata('tipo', $login_element->tipo);
        	}

        return $this->output
					->set_content_type('application/json')
					->set_output( json_encode( [ 'success' => true, 'message'=> '<span class="success"><b>¡Bienvenido! </b></span>', 'redirect' => base_url('home') ] ) );
  }

  public function logout() {

    $this->session->unset_userdata('session');
    $this->session->unset_userdata('id');
    $this->session->unset_userdata('nombres');
    $this->session->unset_userdata('email');
    redirect('login');

  }

}
