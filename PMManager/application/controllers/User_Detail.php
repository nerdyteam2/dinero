<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Detail extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  private $titlepage = "Presupuesto Mensual";
  private $subtitlepage = "";

	function __construct() {
		parent::__construct();

    // CHECK IF THERE IS AN ACTIVE SESSION
    if(!$this->session->userdata('session'))
      redirect('login');

    // LOAD MODULE(S)
		$this->load->model('UserDetail_model', 'user_detail');
	}


  public function index() {
    header('location: ' . base_url('home'));
  }

  public function set_new_password(){
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    $r = $this->user_detail->set_new_password($email,$password);    
    if($r)
      echo $r;
    else
      echo 'Invalid request';
  }

	public function get($id) {

    // CHECK COMING DATA
    if( !isset( $id ) || empty( $id ) || !is_numeric($id) || $this->user_detail->get_id_exists($id) < 1 )
      header('location: ' . base_url('home'));

		// GETTING DATA
    $this->subtitlepage = "Detalle del Usuario";
    $today = date("Y-m-d");
    $month_before = date("Y-m-d", strtotime( $today . "-1 month" ) );
    $year_before = date("Y-m-d", strtotime( $today . "-1 year" ) );
    $ins_monthly = json_encode($this->fillDays($this->user_detail->get_all_ins_per_month($id)), JSON_NUMERIC_CHECK);
    $outs_monthly = json_encode($this->fillDays($this->user_detail->get_all_outs_per_month($id)), JSON_NUMERIC_CHECK);
    $ins_history = json_encode($this->fillMonths($this->user_detail->get_all_ins_history($id)), JSON_NUMERIC_CHECK);
    $outs_history = json_encode($this->fillMonths($this->user_detail->get_all_outs_history($id)), JSON_NUMERIC_CHECK);

    // PREPARE DATA
    $dataHead = array(
      'title' => $this->titlepage,
      'subtitle' => $this->subtitlepage,
      'username' => $_SESSION['nombres'],
    );

		$dataBody = array(
			'user' => $this->user_detail->get_user_info($id),
      'ins_monthly' => $ins_monthly,
      'outs_monthly' => $outs_monthly,
      'ins_history' => $ins_history,
      'outs_history' => $outs_history,
      'ins_day' => $this->getAvgs($id, 1, 1),
      'outs_day' => $this->getAvgs($id, 2, 1),
      'ins_fortnight' => $this->getAvgs($id, 1, 2),
      'outs_fortnight' => $this->getAvgs($id, 2, 2),
      'ins_month' => $this->getAvgs($id, 1, 3),
      'outs_month' => $this->getAvgs($id, 2, 3),
      'ins_year' => $this->getAvgs($id, 1, 4),
      'outs_year' => $this->getAvgs($id, 2, 4),
      'date' => $time = date("Y-m-d"),
		);

		$dataFoot = array(
			'js' => array('table', 'highcharts', 'chart')
		);

		$this->load->view('header', $dataHead);
    $this->load->view('user_detail', $dataBody);
    $this->load->view('footer', $dataFoot);

	}

  // GET AVERAGE PER TIME
  private function getAvgs($id, $tab, $time) {

    // CHECK ARGUMENTS
    if( $tab !== 1 && $tab !== 2 )
      return false;

    switch( $time ) {
      case 1:
        $time = "+1 day";
        $minus = 365;
        break;
      case 2:
        $time = "+1 fortnight";
        $minus = 15;
        break;
      case 3:
        $time = "+1 month";
        $minus = 30;
        break;
      case 4:
        $time = "+1 year";
        $minus = 1;
        break;
      default:
        return false;
    }

    // DECLARE NEEDED VARIABLES
    $today = date("Y-m-d");
    $date = date("Y-m-d", strtotime( $today . "-1 year" ) );
    $tmp_date = $date;
    $end_date = date("Y-m-d", strtotime(date("Y-m-d") . "-1 month"));
    $month_start = date("d", strtotime( $today . "-1 year" ) );
    $tmp_month_start = $month_start;

    $average = 0;
    $count = 0;

    // PREPARE VARIABLES
    $month_start = $month_start - 1;
    $month_start = $tmp_month_start - $month_start;
    $month_start = date("Y-m-0" . $month_start, strtotime( $today . "-1 year" ) );

    $date = $month_start;

    // PROCESS AVERAGE OF EACH DAY, FORTNIGHT, MONTH OR YEAR
    while( strtotime($date) <= strtotime($end_date) ) {

      // MINUS A DAY, FORTNIGHT, MONTH OR YEAR
      $tmp_date = $date;
      $date = date( "Y-m-d", strtotime($time, strtotime($date)));

      // RETRIEVE DATA
      if( $tab === 1 )
        $tmp = $this->user_detail->get_ins_per_time($id, $tmp_date, $date)[0]->Sum;
      elseif( $tab === 2 )
        $tmp = $this->user_detail->get_outs_per_time($id, $tmp_date, $date)[0]->Sum;

      $average += (empty($tmp) ? 0 : $tmp);

      // SUM COUNTER
      $count += 1;

    }

    return number_format( $average / $count, 2 );

  } // AVERAGE PER TIME


  private function fillDays( $arr ) {
    $month_days = date("t");
    $days_month = array();

    $lim = count($arr);

    for($i = 0; $i < $month_days; $i++) {
      if( $lim > 0 ) {
        for($j = 0; $j < $lim; $j++) {
          if( (date('Y-m-') . $i) !== $arr[$j]->fecha ) {
            $days_month["monto"][$i] = 0;
            $days_month["fecha"][$i] = (date('Y-m-' . ($i + 1 < 10 ? "0" : "")) . ($i + 1));
          } else {
            $days_month["monto"][$i] = $arr[$j]->monto;
            $days_month["fecha"][$i] = $arr[$j]->fecha;
          }
        }
      } else {
        $days_month["monto"][$i] = 0;
        $days_month["fecha"][$i] = (date('Y-m-' . ($i + 1 < 10 ? "0" : "")) . ($i + 1));
      }
    }

    return $days_month;
  }

  private function fillMonths( $arr ) {
    $month = array();

    $date_init = date("Y-m-01", strtotime(date("Y-m-01") . "-1 year +1 month"));
    $date_end = date("Y-m-01");

    $i = 0;
    $j = 0;

    if( count($arr) > 0 ) {
      while( strtotime($date_init) <= strtotime($date_end) ) {

        $tmp_month = substr( $date_init, 0, 7 );
        $tmp_date = substr($arr[$j]->fecha, 0, 7);

        if( $tmp_date !== $tmp_month ) {
          $month["monto"][$i] = 0;
          $month["fecha"][$i] = $date_init;
        } else {
          $month["monto"][$i] = $arr[$j]->monto;
          $month["fecha"][$i] = $arr[$j]->fecha;
          if( isset($arr[$j+1]) ) $j += 1;
        }

        $date_init = date( "Y-m-d", strtotime("+1 month", strtotime($date_init)));
        $i += 1;

      }
    } else {
      for($i = 0; $i < 12; $i++) {
        $month["monto"][$i] = 0;
        $month["fecha"][$i] = $date_init;
        $date_init = date( "Y-m-d", strtotime("+1 month", strtotime($date_init)));
      }
    }

    return $month;
  }

}
