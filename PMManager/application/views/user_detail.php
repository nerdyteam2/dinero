
      <script>
    <?php
      echo "    window.dataIns_Monthly = " . $ins_monthly . ";\n";
      echo "    window.dataIns_History = " . $ins_history . ";\n";
      echo "    window.dataOuts_Monthly = " . $outs_monthly . ";\n";
      echo "    window.dataOuts_History = " . $outs_history . ";\n";
    ?>

      </script>

      <!-- USER INFO -->
      <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">

          <div class="panel panel-default">
            <div class="panel-heading">
              <a href="<?=base_url('home')?>" title="Volver">
                <img src="<?=base_url('assets/images/site/arrowLeft.png')?>">
              </a>
              <strong><?=$user[0]['name']?></strong>
            </div>
            <div class="panel-body">

              <!-- USER INFO -->
              <table class="table table-bordered table_user">
                <tbody class="tab_user_info">
                  <tr>
                    <td class="td-striped"><strong>Id</strong></td>
                    <td><?=$user[0]['id']?></td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Email</strong></td>
                    <td id="email"><?=$user[0]['email']?></td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Rate Mensual</strong></td>
                    <td class="<?=($user[0]['rate_month'] > 0 ? 'high-rate' : 'low-rate')?>">$<?=number_format($user[0]['rate_month'], 2)?></td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Rate Total</strong></td>
                    <td class="<?=($user[0]['rate_total'] > 0 ? 'high-rate' : 'low-rate')?>">$<?=number_format($user[0]['rate_total'], 2)?></td>
                  </tr>
                </tbody>
              </table>

              <!-- INS INFO -->
              <table class="table table-bordered table_user">
                <tbody class="tab_user_info">
                  <tr>
                    <td class="td-title" colspan="2"><strong>Promedio de Ingresos</strong> (De un año atrás a la fecha)</td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Diarios</strong></td>
                    <td class="<?=($ins_day > 0 ? 'high-rate' : 'low-rate')?>">
                      <?="$".$ins_day?>
                    </td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Quincenales</strong></td>
                    <td class="<?=($ins_fortnight > 0 ? 'high-rate' : 'low-rate')?>">
                      <?="$".$ins_fortnight?>
                    </td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Mensual</strong></td>
                    <td class="<?=($ins_month > 0 ? 'high-rate' : 'low-rate')?>">
                      <?="$".$ins_month?>
                    </td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Anual</strong></td>
                    <td class="<?=($ins_year > 0 ? 'high-rate' : 'low-rate')?>">
                      <?="$".$ins_year?>
                    </td>
                  </tr>
                </tbody>
              </table>

              <!-- OUTS INFO -->
              <table class="table table-bordered table_user">
                <tbody class="tab_user_info">
                  <tr>
                    <td class="td-title" colspan="2"><strong>Promedio de Gastos</strong> (De un año atrás a la fecha)</td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Diarios</strong></td>
                    <td class="<?=($outs_day > 0 ? 'high-rate' : 'low-rate')?>">
                      <?="$".$outs_day?>
                    </td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Quincenal</strong></td>
                    <td class="<?=($outs_fortnight > 0 ? 'high-rate' : 'low-rate')?>">
                      <?="$".$outs_fortnight?>
                    </td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Mensual</strong></td>
                    <td class="<?=($outs_month > 0 ? 'high-rate' : 'low-rate')?>">
                      <?="$".$outs_month?>
                    </td>
                  </tr>
                  <tr>
                    <td class="td-striped"><strong>Anual</strong></td>
                    <td class="<?=($outs_year > 0 ? 'high-rate' : 'low-rate')?>">
                      <?="$".$outs_year?>
                    </td>
                  </tr>
                </tbody>
              </table>

              <!-- CHARTS -->
              <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#ins" aria-controls="ins" role="tab" data-toggle="tab">Ingresos</a></li>
                  <li role="presentation"><a href="#outs" aria-controls="outs" role="tab" data-toggle="tab">Gastos</a></li>
                  <li class="tab-title" role="presentation"><a href="javascript:void()">Gráficas</a></li>
                </ul>

                <!-- TAB PANES -->
                <div class="tab-content">

                  <!-- INS TAB -->
                  <div role="tabpanel" class="tab-pane fade in active" id="ins">
                    <div class="col-xs-11">
                      <div class="collapse in" id="monthly_chart_ins">
                        <div id="highchart_ins_monthly" style="margin: 20px auto; width: 600px; height: 400px;"></div>
                      </div>
                      <div class="collapse" id="history_chart_ins">
                        <div id="highchart_ins_history" style="margin: 20px auto; width: 600px; height: 400px;"></div>
                      </div>
                    </div>
                    <div class="col-xs-1">
                      <button class="btn btn-default glyphicon glyphicon-calendar pull-right margintop-20px"
                              type="button" data-toggle="collapse" data-hover="tooltip" data-target="#monthly_chart_ins"
                              aria-expanded="false" aria-controls="monthly_chart_ins" title="Mensual">
                      </button>
                      <button class="btn btn-default glyphicon glyphicon-object-align-horizontal pull-right margintop-20px"
                              id="tooltip_history_ins" type="button" data-toggle="collapse" data-hover="tooltip"
                              data-target="#history_chart_ins" aria-expanded="false" aria-controls="history_chart_ins"
                              title="Histórico">
                      </button>
                    </div>
                  </div>

                  <!-- OUTS TAB -->
                  <div role="tabpanel" class="tab-pane fade" id="outs">


                    <div class="col-xs-11">
                      <div class="collapse in" id="monthly_chart_outs">
                        <div id="highchart_outs_monthly" style="margin: 20px auto; width: 600px; height: 400px;"></div>
                      </div>
                      <div class="collapse" id="history_chart_outs">
                        <div id="highchart_outs_history" style="margin: 20px auto; width: 600px; height: 400px;"></div>
                      </div>
                    </div>
                    <div class="col-xs-1">
                      <button class="btn btn-default glyphicon glyphicon-calendar pull-right margintop-20px"
                              type="button" data-toggle="collapse" data-hover="tooltip" data-target="#monthly_chart_outs"
                              aria-expanded="false" aria-controls="monthly_chart_outs" title="Mensual">
                      </button>
                      <button class="btn btn-default glyphicon glyphicon-object-align-horizontal pull-right margintop-20px"
                              id="tooltip_history_ins" type="button" data-toggle="collapse" data-hover="tooltip"
                              data-target="#history_chart_outs" aria-expanded="false" aria-controls="history_chart_outs"
                              title="Histórico">
                      </button>
                    </div>


                  </div>

                </div>

              </div>


              <!-- INS INFO -->
              <table class="table table-bordered table_user">
                <tbody class="tab_user_info">
                  <tr>
                    <td class="td-striped">Cambiar contraseña</td>
                    <td > <input type="text" placeholder="Nueva Contraseña" name="newpassword" value=""> &nbsp;&nbsp; <button onclick="javascript:changepassword();">Cambiar</button> </td>
                  </tr>
                </tbody>
              </table>


            </div>

          </div>

        </div>
      </div>


<script type="text/javascript">
  function  changepassword(){
    var password = $('input[name="newpassword"]').val().trim();
    var email = $('#email').text();

    if(password.length < 3){
      alert('Contraseña no valida, trate con un valor diferente.');
      return;
    }

    $.ajax({
      method:"POST",
      url:"<?=base_url('User_Detail/set_new_password')?>",
      data:{
        email:email,
        password:password
      }
    }).done(function(response){
      console.log(response);
      if(response=='1'){
        alert('Operación realizada exitosamente.');
      }
      else{
        alert('No se pudo actualizar, la contraseña, intente con un valor diferente.');
      }
      $('input[name="newpassword"]').val('');
    });


    
  }
</script>
