
  <!-- MESSAGES -->
  <div class="row notification">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
      <div class="alert notification-text" role="alert">
        Hello
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 access-form">

      <!-- ACCESS FORM -->
      <div class="row">
        <div class="col-xs-12 col-sm-6 logo">
          <img src="<?=base_url('assets/images/site/logo.png')?>" style="height: 250px;">
        </div>
        <?= form_open('login/check', ['id'           => 'form_login',
                                      'name'         => 'form_login',
                                      'class'        => 'form col-xs-12 col-sm-6',
                                      'method'       => 'POST',
                                      'autocomplete' => 'off',
                                      'role'         => 'form']); ?>
          <div class="form-group">
            <h2>Login</h2>
          </div>
          <div class="form-group">
            <label for="user-email">Usuario</label>
            <input class="form-control" id="user-email" name="user-email" placeholder="..." type="text">
          </div>
          <div class="form-group">
            <label for="user-password">Contraseña</label>
            <input class="form-control" id="user-password" name="user-password" placeholder="..." type="password">
          </div>
          <div class="form-group">
            <button class="btn btn-success pull-right" type="submit">Accesar</button>
          </div>
        <?= form_close(); ?>
      </div> <!-- ACCESS FORM -->

    </div>
  </div>
