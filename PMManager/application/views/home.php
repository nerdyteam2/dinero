
  <div class="row">
    <div class="col-xs-12 col-sm-10 col-sm-offset-1 initPage">

      <!-- EXPORTING TOOLS -->
      <div id="toolbar">
        <select class="form-control">
          <option value="">Export Basic</option>
          <option value="all">Export All</option>
          <option value="selected">Export Selected</option>
        </select>
      </div>

      <table id="tableUser"
             data-toggle="table"
             data-classes="table table-hover table-condensed"
             data-striped="true"
             data-sort-name="name"
             data-sort-order="asc"
             data-search="true"
             data-show-toggle="true"
             data-show-columns="true"
             data-query-params="queryParams"
             data-toolbar="#toolbar"
             data-pagination="true"
             data-search="true"
             data-height="500"
             data-show-export="true"
             data-export-types="['csv', 'json', 'xml']">
        <thead>
          <tr>
            <th data-field="state" data-checkbox="true"></th>
            <th data-field="id"
                data-sortable="true">
                ID
            </th>
            <th data-field="name"
                data-sortable="true">
                Nombres
            </th>
            <th data-field="email"
                data-sortable="true">
                Email
            </th>
            <th data-field="fecha"
                data-sortable="true">
                Fecha de alta
            </th>
            <th data-field="rate_month"
                data-sortable="true">
                Rate Mensual
            </th>
            <th data-field="rate_total"
                data-sortable="true">
                Rate Total
            </th>
          </tr>
        </thead>
        <tbody>
<?php for($i = 0; $i < count($users); $i++):?>
          <tr>
            <td></td>
            <td><?=$users[$i]['id']?></td>
            <td><?=$users[$i]['name']?></td>
            <td><?=$users[$i]['email']?></td>
            <td><?=$users[$i]['fecha_alta']?></td>
            <td class="<?=($users[$i]['rate_month'] > 0 ? 'high-rate' : 'low-rate' )?>"><?=number_format($users[$i]['rate_month'], 2, '.', '')?></td>
            <td class="<?=($users[$i]['rate_total'] > 0 ? 'high-rate' : 'low-rate' )?>"><?=number_format($users[$i]['rate_total'], 2, '.', '')?></td>
          </tr>
<?php endfor; ?>
        </tbody>
      </table>

    </div>
  </div>
