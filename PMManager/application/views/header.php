<!DOCTYPE html>
<html>
  <head>
    <title><?=$title . ' - ' . $subtitle?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=100, initial-scale=1">
    <link rel="shortcut icon" href="<?=base_url('assets/images/site/icon.png')?>">
    <link rel="stylesheet" href="<?=base_url('vendor/bootstrap/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('vendor/bootstrap-table-master/dist/bootstrap-table.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/styles/main.css')?>">
<?php
    if( isset($css) && !empty($css) )
      foreach ($css as $key => $value)
        echo '    <link rel="stylesheet" href="'.base_url('assets/styles/'.$value).'.css">';
?>

  </head>
  <body>

    <!-- BASE_URL -->
    <input id="base_url" name="base_url" type="hidden" value="<?=base_url()?>">

    <!-- SPINNER -->
    <div id="loader">
      <div id="tout">
        <div>
          <div>
            <div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- BODY CONTENT -->
    <div class="container-fluid">

      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=base_url()?>">
              <img class="navbar-icon" src="<?=base_url('assets/images/site/icon.png')?>">
              <span style="margin-left: 10px;">Presupuesto Mensual</span>
            </a>
          </div>

<?php if(isset($username) && !empty($username)): ?>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Bienvenido <?=$username?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?=base_url('login/logout')?>">Cerrar sesión</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->

<?php endif; ?>

        </div><!-- /.container-fluid -->
      </nav>
