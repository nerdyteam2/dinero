
      <!-- SCRIPTS -->
      <script src="<?=base_url('assets/scripts/jquery-1.12.1.min.js')?>"></script>
      <script src="<?=base_url('assets/scripts/jquery.form.js')?>"></script>
      <script src="<?=base_url('vendor/bootstrap/js/bootstrap.min.js')?>"></script>
      <script src="<?=base_url('vendor/bootstrap-table-master/dist/bootstrap-table.min.js')?>"></script>
      <script src="<?=base_url('vendor/bootstrap-table-master/dist/locale/bootstrap-table-es-MX.min.js')?>"></script>
      <script src="<?=base_url('vendor/tableExport.jquery.plugin-master/libs/FileSaver/FileSaver.min.js')?>"></script>
      <script src="<?=base_url('vendor/tableExport.jquery.plugin-master/tableExport.min.js')?>"></script>
      <script src="<?=base_url('assets/scripts/bootstrap-table-export.js')?>"></script>
<?php
    if( isset($js) && !empty($js) )
      foreach ($js as $key => $value)
        echo "      <script src='".base_url('assets/scripts/'.$value).".js'></script>\n";
?>

    </div> <!-- BODY CONTAINER -->
  </body>
</html>
